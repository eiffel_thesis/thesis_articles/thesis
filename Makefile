CC=pdflatex
BIB=bibtex
SPELLCHECK=aspell

PAPER=thesis.tex
FLATTENED=flatten.tex

TEX=$(wildcard content/*.tex)
AUX=$(wildcard *.aux)
FIGS_SVG=$(wildcard figs/SVG/*.svg)
FIGS_PDF=$(subst SVG,PDF,$(FIGS_SVG:.svg=.pdf))

# Get first PDF figure of FIGS_PDF with firstword then get its dirname with dir.
FIGS_PDF_DIR=$(dir $(firstword $(FIGS_PDF)))

all: $(FIGS_PDF)
	$(CC) $(PAPER)

# For each figure in SVG convert it to PDF.
figs/PDF/%.pdf: figs/SVG/%.svg
	@if [ ! -d $(FIGS_PDF_DIR) ]; then mkdir $(FIGS_PDF_DIR); fi
	inkscape $< --export-area-drawing --without-gui --export-pdf=$@

bib: $(AUX)
	for i in $^; do $(BIB) $$i; done

check: $(PAPER) $(TEX)
	# '-d fr' tells to use english dictionnary, '-t' is to use LaTeX mode
	# and '-c' is to check.
	for i in $^; do $(SPELLCHECK) -d fr -t -c $$i; done

archive: $(FIGS_PDF)
	$(CC) --jobname these_archivage_3500038 "\def\archive{1} \input{$(PAPER)}"

flatten:
	# This command permits replacing \input by the associated content.
	# The job is done recursively.
	latexpand $(PAPER) -o $(FLATTENED)

clean:
	rm *.pdf *.aux *.bbl *.blg *.log *.out *.bcf *.lof *.lol *.lot *.toc *.xml

mr_proper: clean
	rm -r $(FIGS_PDF_DIR) _minted-thesis