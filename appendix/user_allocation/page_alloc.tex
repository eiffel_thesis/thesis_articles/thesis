\section{Allocation d'une \texttt{page}}
\label{sec:page_alloc}
Dans la section précédente, nous avons vu qu'un appel à \texttt{malloc} peut engendrer un appel à \texttt{\_\_brk} ou \texttt{\_\_mmap}.
Ces appels systèmes correspondent à des fonctions noyaux qui vont soit agrandir une \texttt{vm\_area\_struct} ou en créer une nouvelle.
Dans ces deux cas, aucune page n'est allouée pour satisfaire la requête de l'utilisateur.

En effet, Linux pratique l'allocation paresseuse, c'est-à-dire qu'il attend le dernier moment pour allouer une page.
Ce dernier moment correspond à l'accès d'une adresse d'une page virtuelle n'étant associée à aucune page physique.

Ceci provoquera un défaut de page, nous étudierons donc, dans cette section, ce mécanisme.

Un défaut de page est une exception, au même titre qu'une division par 0, elle fera donc passer la tâche courante en mode système.
Pour l'architecture x86\_64, un défaut de page se produit, pour une adresse linéaire donnée, dans deux cas différents \cite[Section 4.7]{intel_sys_prog_2019}:
\begin{enumerate}
	\item Il n'existe pas de traduction vers une adresse physique pour l'adresse linéaire donnée.
	\item Ou, il existe une traduction, mais les droits d'accès de la page physique ne permettent pas de répondre à l'accès demandé.
	Par exemple, la page physique est accessible en lecture seule et la MMU a reçu une demande d'écriture sur cette page.
\end{enumerate}
Dans Linux, c'est la fonction \texttt{do\_page\_fault} qui se chargera de gérer un défaut de page  \citelinux{arch/x86/entry/entry\_64.S}{1161}\citelinux{arch/x86/mm/fault.c}{1461}.
Pour ce faire, elle s'appuiera sur le code d'erreur qui lui est donné en paramètre, ce code d'erreur est décrit dans la Figure \ref{fig:error_code_page_fault} \cite[Figure 4.12]{intel_sys_prog_2019}.

\begin{figure}
	\centering

	\begin{bytefield}[endianness = big]{32}
		\bitheader{0, 1, 2, 3, 4, 5, 6, 7, 14, 15, 16, 31}\\
		\bitbox{16}{\color{black}\rule{\width}{\height}} & \bitbox{1}{\rotatebox{90}{\tiny SGX}} & \bitbox{8}{\color{black}\rule{\width}{\height}} & \bitbox{1}{\tiny SS} & \bitbox{1}{\tiny PK} & \bitbox{1}{\rotatebox{90}{\tiny I/D}} & \bitbox{1}{\rotatebox{90}{\tiny RSVD}} & \bitbox{1}{\rotatebox{90}{\tiny U/S}} & \bitbox{1}{\rotatebox{90}{\tiny W/R}} & \bitbox{1}{\tiny P}\\
	\end{bytefield}

	\caption[Code d'erreur fourni par le processeur lors d'un défaut de page]{Code d'erreur fourni par le processeur lors d'un défaut de page \cite[Figure 4.12]{intel_sys_prog_2019}}
	\label{fig:error_code_page_fault}
\end{figure}

Les différents bits de ce code d'erreur sont décrits dans le Tableau \ref{table:error_code_page_fault} \cite[Figure 4.12]{intel_sys_prog_2019}.
Dans le noyau, les drapeaux correspondant sont définis dans l'\texttt{enum x86\_pf\_error\_code} \citelinux{arch/x86/include/asm/traps.h}{158}.

\begin{table}
	\centering

	\caption[Descriptions des différents bits du code d'erreur fournis par le processeur lors d'un défaut de page]{Descriptions des différents bits du code d'erreur fournis par le processeur lors d'un défaut de page \cite[Figure 4.12]{intel_sys_prog_2019}}
	\label{table:error_code_page_fault}

	\begin{tabular}{p{.2\textwidth}ccp{.7\textwidth}}
		\hline
		Nom & Abréviation & Bit & Description\\
		\hline
		\textit{Present} & \texttt{P} & 0 & Si ce drapeau a pour valeur 0, ceci signifie que l'adresse linéaire ne possède pas de page physique associée.\\
		\textit{Read/Write} & \texttt{W/R} & 1 & Si ce drapeau vaut 1, alors le défaut de page a été causé par une écriture, sinon par une lecture.\\
		\textit{User/Supervisor} & \texttt{U/S} & 2 & Le défaut de page fait suite à un accès effectué en mode utilisateur si ce drapeau a pour valeur 1, sinon l'accès s'est fait en mode superviseur.\\
		\textit{Reserved} & \texttt{RSVD} & 3 & Ce drapeau vaut 1 s'il n'existe pas de traduction pour une adresse linéaire parce qu'un bit réservé a été utilisé dans les entrées de la table de pages ayant mené à cette tentative de traduction.\\
		\textit{Instruction/Data} & \texttt{I/D} & 4 & Le défaut de page a été causé par le chargement d'une instruction si ce drapeau est vrai, sinon il a été causé par celui d'une donnée.\\
		\textit{Protections Keys} & \texttt{PKEYS} & 5 & Quand ce drapeau est vrai, l'exception fait suite à une violation des \textit{protection keys}, sinon, elle ne fait pas suite à un tel événement.\\
		\textit{Shadow Stack} & \texttt{SS} & 6 & Le défaut de page se produit après un accès à une \textit{shadow stack} quand ce drapeau est à 1, sinon il n'est pas produit par un tel accès.
		Une \textit{shadow stack} est une seconde pile utilisée pour sécuriser le retour d'une fonction \cite[Section 18.1.1]{intel_basic_2019}.
		Lors d'un appel de fonction, via l'instruction \texttt{call}, l'adresse de retour de celle-ci sera empilée sur la pile classique et sur la \textit{shadow stack}.
		Quand la fonction se termine, via l'instruction \texttt{ret}, les adresses de retour sont dépilées et comparées, si elles ne correspondent pas une exception de sécurité est levée.\\
		\textit{Software Guard eXtensions} & \texttt{SGX} & 7 & Le défaut de page est lié à une enclave SGX quand ce drapeau vaut 1, sinon il n'est pas relié à une enclave.
		Les \texttt{SGX} permettent de créer des zones mémoires sécurisées, appelées enclaves \cite[Section 36.1]{intel_sys_prog_2019}.
		Une enclave est confidentielle et son intégrité est garantie par le matériel.\\
		\hline
	\end{tabular}
\end{table}

Cette fonction récupère d'abord l'adresse linéaire ayant causé le défaut de page depuis le registre \texttt{cr2} \citelinux{arch/x86/mm/fault.c}{1463}\cite[Section 2.5]{intel_sys_prog_2019}.
Elle délègue ensuite la gestion du défaut de page à la fonction \texttt{\_\_do\_page\_fault} \citelinux{arch/x86/mm/fault.c}{1470}\citelinux{arch/x86/mm/fault.c}{1211}.
Cette fonction rend, dans un premier temps, un diagnostic sur l'origine du défaut de page en se basant sur le code d'erreur.
Ainsi, plusieurs cas sont possibles, nous étudierons certains d'entre eux dans les sous-sections suivantes.

\subsection{Adresse orpheline}
\label{subsec:without_vm_area_struct}
En se basant sur l'adresse ayant généré le défaut de page, \texttt{\_\_do\_page\_fault} va essayer de trouver la \texttt{vm\_area\_struct} correspondante \citelinux{arch/x86/mm/fault.c}{1341}.
Si une telle structure n'existe pas, la fonction \texttt{bad\_area} sera invoquée \citelinux{arch/x86/mm/fault.c}{1343}\citelinux{arch/x86/mm/fault.c}{932}.
Celle-ci appelle \texttt{\_\_bad\_area} qui, à son tour, fait appel à \texttt{\_\_bad\_area\_nosemaphore} \citelinux{arch/x86/mm/fault.c}{934}\citelinux{arch/x86/mm/fault.c}{912}\citelinux{arch/x86/mm/fault.c}{927}\citelinux{arch/x86/mm/fault.c}{844}.

Si le défaut de page a été généré par une tâche en espace utilisateur, la fonction va d'abord tester s'il est lié à un chargement d'instruction \citelinux{arch/x86/mm/fault.c}{860}.
Si c'est le cas, la gestion du défaut de page s'arrêtera là.
Néanmoins, un chargement d'instruction peut se révéler malicieux, par exemple, s'il est effectué sur une page marquée comme ne pouvant être exécutée \citelinux{arch/x86/mm/fault.c}{133}.
La fonction commandera l'envoi du signal \texttt{SIGSEGV}, correspondant à une erreur de segmentation, à la tâche courante, via la fonction \texttt{force\_sig\_info\_fault} \citelinux{arch/x86/mm/fault.c}{893}.
\texttt{\_\_bad\_area\_nosemaphore} aura, auparavant, renseigné, pour la tâche courante, l'adresse ayant généré le défaut de page, le code d'erreur et le numéro d'exception.
Pour une architecture X86\_64, ce numéro vaut 14 \cite[Section 4.7]{intel_sys_prog_2019}.

\subsection{Adresse non orpheline}
\label{subsec:with_vm_area_struct}
Dans le cas où une \texttt{vm\_area\_struct} aurait été trouvée dans la fonction \texttt{\_\_do\_page\_fault}, l'adresse ayant causé le défaut de page sera comparée à la plus basse adresse de l'espace d'adressage virtuel de la tâche.
Ici aussi, trois cas, que nous allons étudier dans les paragraphes suivants, sont possibles.

\subsubsection{Défaut de page dans la pile}
\label{subsubsec:stack}
Si l'adresse ayant généré le défaut de page est inférieure à l'adresse la plus basse de sa \texttt{vm\_area\_struct} ceci signifie que cette adresse appartient à la pile.
En effet, la pile croît vers le bas, il faudra donc l'étendre.
Ainsi, \texttt{\_\_do\_page\_fault} appellera \texttt{expand\_stack} qui fera appel à \texttt{expand\_downwards} \citelinux{arch/x86/mm/fault.c}{1364}\citelinux{arch/x86/mm/fault.c}{2502}\citelinux{arch/x86/mm/fault.c}{2504}\citelinux{mm/mmap.c}{2389}.
Celle-ci invoquera \texttt{acct\_stack\_growth} pour vérifier que l'accroissement de la pile est possible \citelinux{mm/mmap.c}{2430}\citelinux{mm/mmap.c}{2252}.
Elle vérifiera, entre autres, que la taille de celle-ci est inférieure à \texttt{RLIMIT\_STACK} \citelinux{mm/mmap.c}{2263}.
Si l'agrandissement de la \texttt{vm\_area\_struct} est possible, son adresse de début sera changée par \texttt{expand\_upwards} \citelinux{mm/mmap.c}{2448}.

La fonction \texttt{expand\_downwards} terminera ainsi son exécution.
La gestion du défaut de page continuera dans \texttt{\_\_do\_page\_fault}, en effet, pour le moment aucun page physique n'a été allouée pour répondre au défaut.

\subsubsection{Défauts de page d'une adresse non associée à une entrée de table des pages}
\label{subsubsec:good_area}
Dans le cas où l'adresse ayant généré le défaut de page est supérieure à l'adresse de début de sa \texttt{vm\_area\_struct} la fonction continuera son exécution à l'étiquette \texttt{good\_area} \citelinux{arch/x86/mm/fault.c}{1346}\citelinux{arch/x86/mm/fault.c}{1373}.
Notez toutefois que dans le cas où ce serait la pile, l'exécution de la fonction \texttt{\_\_do\_page\_fault} continuera aussi son exécution à ce point.

L'exécution peut néanmoins se continuer avec \texttt{bad\_area\_access\_error} si l'accès est erroné \citelinux{arch/x86/mm/fault.c}{1374}\citelinux{arch/x86/mm/fault.c}{1143}\citelinux{arch/x86/mm/fault.c}{1375}.
La fonction \texttt{access\_error} considère, entre autres, un accès erroné lorsque le code d'erreur du défaut de page à son bit \texttt{W/R} valant 1 alors que la \texttt{vm\_area\_struct} ne peut être écrite \citelinux{arch/x86/mm/fault.c}{1167}.
\texttt{bad\_area\_access\_error} conduit à l'appel à \texttt{\_\_bad\_area} que nous avons déjà étudiée \citelinux{arch/x86/mm/fault.c}{964}\citelinux{arch/x86/mm/fault.c}{966}.

Dans le cas d'un accès licite, \texttt{handle\_mm\_fault} sera appelée \citelinux{arch/x86/mm/fault.c}{1395}\citelinux{mm/memory.c}{4116}.
Cette fonction testera si l'accès est légal pour l'architecture en appelant \texttt{arch\_vma\_access\_permitted} \citelinux{mm/memory.c}{4129}\citelinux{arch/x86/include/asm/mmu\_context.h}{310}.
Pour l'architecture x86\_64, cette fonction teste si l'accès est permis par les \textit{protections keys}.
Si cet accès est autorisé par celles-ci, la fonction invoquera \texttt{\_\_handle\_mm\_fault} si le défaut de page n'appartient pas à une \textit{huge page} \citelinux{mm/memory.c}{4144}\citelinux{mm/memory.c}{4129}.
Celle-ci parcoure les niveaux intermédiaires de la table des pages ayant mené à cette adresse pour vérifier s'il ne faut pas allouer une \textit{transparent huge page}.
Dans le cas contraire, elle délègue la gestion du défaut de page "simple" à \texttt{
handle\_pte\_fault} \citelinux{mm/memory.c}{4107}\citelinux{mm/memory.c}{3939}.
Si l'adresse ayant généré le défaut de page n'est pas associée à une entrée de la table des pages, la fonction distinguera deux cas que nous allons étudier dans les sous-paragraphes suivant.
Premièrement, nous étudions le cas où l'adresse est liée à de la mémoire anonyme.
Puis, celui où elle est utilisée pour projeter un fichier en mémoire.
Enfin, nous regarderons les traitements exécutés pour le mécanisme de \textit{Copy on Write} (CoW).
Dans le cas second cas, nous ferons la différence quand le défaut de page est causé par une lecture ou une écriture.

\paragraph{Adresse non associée à une entrée de la table des pages et utilisée pour de la mémoire anonyme}
\label{para:anonymous}
Dans ce cas, \texttt{handle\_pte\_fault} appelle \texttt{do\_anonymous\_page} \citelinux{mm/memory.c}{3981}\citelinux{mm/memory.c}{3120}.

% lru_cache_add_active_or_unevictable dans do_anonymous_page.

Cette fonction commence par allouer une entrée de table des pages pour l'adresse fautive en appelant \texttt{pte\_alloc} \citelinux{mm/memory.c}{3142}.
Une page est ensuite allouée depuis le \textit{buddy allocator}, dont nous détaillerons le fonctionnement dans une section ultérieure, via \texttt{alloc\_zeroed\_user\_highpage\_movable} \citelinux{mm/memory.c}{3172}.
Les droits d'accès de l'entrée de la table des pages, correspondant à la page allouée, sont initialisés à partir de ceux de la \texttt{vm\_area\_struct} englobant l'adresse fautive \citelinux{mm/memory.c}{3187}\citelinux{mm/memory.c}{3189}.
L'entrée en question est ensuite inscrite dans la table des pages de la tâche courante suite à l'appel à \texttt{set\_pte\_at} \citelinux{mm/memory.c}{3213}.

\paragraph{Adresse accédée en lecture, non associée à une entrée de la table des pages et utilisée pour de la mémoire fichier}
\label{para:file_read}
\texttt{handle\_pte\_fault} invoque \texttt{do\_fault} \citelinux{mm/memory.c}{3983}\citelinux{mm/memory.c}{3743}.
Si le défaut de page est causé par une lecture, la fonction \texttt{do\_read\_fault} est appelée \citelinux{mm/memory.c}{3752}\citelinux{mm/memory.c}{3636}.
Cette fonction appelle à son tour \texttt{\_\_do\_fault} \citelinux{mm/memory.c}{3652}\citelinux{mm/memory.c}{3235}.
Elle fait appel à la fonction pointée par \texttt{vm\_ops->fault} qui, pour le système de fichier \texttt{btrfs}, revient à appeler \texttt{filemap\_fault} \citelinux{mm/memory.c}{3240}\citelinux{fs/btrfs/file.c}{2176}\citelinux{mm/filemap.c}{2492}.
Si l'adresse fautive n'a pas de page associée dans le \textit{page cache}, une page sera lue depuis le disque via \texttt{do\_sync\_mmap\_readahead} \citelinux{mm/filemap.c}{2520}\citelinux{mm/filemap.c}{2408}.
Dans celle-ci, si la \texttt{vm\_area\_struct} correspondant à l'adresse fautive n'a jamais été modifiée par \texttt{madvise}, \texttt{\_\_do\_page\_cache\_readahead} est appelée par \texttt{ra\_submit} \citelinux{mm/filemap.c}{2444}\citelinux{mm/internal.h}{63}\citelinux{mm/internal.h}{66}\citelinux{mm/readahead.c}{152}.
Cette fonction va allouer des pages en appelant la fonction \texttt{\_\_page\_cache\_alloc} du \textit{buddy allocator} \citelinux{mm/readahead.c}{195}.
Les pages allouées seront remplies, groupe par groupe, en lisant les données depuis le disque via \texttt{read\_pages} \citelinux{mm/readahead.c}{189}.

% add_to_page_cache_lru dans read_pages.

Elle appelle des fonctions propres à chaque système de fichier, nous ne détaillons donc pas son fonctionnement.
Une fois les pages lues, l'exécution peut continuer son chemin dans \texttt{do\_read\_fault} avec \texttt{finish\_fault} \citelinux{mm/memory.c}{3656}\citelinux{mm/memory.c}{3491}.
Cette fonction, en appelant \texttt{alloc\_set\_pte}, ajoute une entrée de la table des pages pour l'adresse fautive et l'associe à la page précédemment allouée \citelinux{mm/memory.c}{3510}\citelinux{mm/memory.c}{3426}.

\paragraph{Adresse accédée en écriture, non associée à une entrée de la table des pages et utilisée pour de la mémoire fichier}
\label{para:file_read}
Dans le cas contraire, c'est \texttt{do\_cow\_fault}\sidenote{Cette fonction n'a rien à voir avec le mécanisme de \textit{Copy on Write} (CoW) que nous détaillerons dans la Section \ref{subsubsec:cow}} qui est invoquée par \texttt{do\_fault} \citelinux{mm/memory.c}{3756}\citelinux{mm/memory.c}{3663}.
Elle alloue tout de suite une page depuis le \textit{buddy allocator} en appelant \texttt{alloc\_page\_vma} \citelinux{mm/memory.c}{3671}.
Comme \texttt{do\_read\_fault}, elle appelle \texttt{\_\_do\_fault} et donc \texttt{filemap\_fault} \citelinux{mm/memory.c}{3681}.
La page allouée sera associée à une entrée de table des pages dans \texttt{finish\_fault} \citelinux{mm/memory.c}{3690}.

\subsubsection{Défaut de page lié au mécanisme de \textit{Copy on Write} (CoW)}
\label{subsubsec:cow}
Si l'adresse fautive est associée à une entrée de table des pages mais n'appartient pas à la pile de la tâche, il peut s'agir d'un défaut de page dû au mécanisme de CoW.
Lors de la création d'une nouvelle tâche, suite à un appel à \texttt{fork}, la tâche enfant hérite de la table des pages de son parent.
Les pages sont aussi toutes marquées comme étant accessibles en lecture seule.
De ce fait, lorsque l'enfant ou le parent accède en écriture à une page, une nouvelle page sera allouée.
Celle-ci sera une copie parfaite de la page d'origine et elle pourra être écrite.
Ainsi, des données qui étaient communes aux parent et à son enfant pourront diverger sans problème puisque ces divergences sont propres à chaque tâches.

Ainsi, \texttt{handle\_pte\_fault} teste si le défaut de page est du à une écriture et s'il existe une entrée de table des pages pour l'adresse fautive \citelinux{mm/memory.c}{3997}\citelinux{mm/memory.c}{3998}.
Si c'est le cas, la fonction \texttt{do\_wp\_page} est appelée \citelinux{mm/memory.c}{3999}\citelinux{mm/memory.c}{2724}.
Celle-ci appelle alors \texttt{wp\_page\_copy} \citelinux{mm/memory.c}{2793}\citelinux{mm/memory.c}{2486}.
Cette fonction alloue, grâce au \textit{buddy allocator}, une nouvelle page \citelinux{mm/memory.c}{2507}.
Elle appelle ensuite \texttt{cow\_user\_page}, à la fin de pile d'appel de cette fonction, le contenu de l'ancienne page sera copiée dans la page fraîchement allouée \citelinux{mm/memory.c}{2511}\citelinux{mm/memory.c}{2332}\citelinux{include/asm/page\_32.h}{44}.
Enfin, \texttt{wp\_page\_copy} mettra à jour l'entrée de la table des pages de l'adresse fautive pour qu'elle pointe sur la nouvelle page en appelant \texttt{set\_pte\_at} \citelinux{mm/memory.c}{2553}.

Dans cette section, nous avons vu les différents défauts de pages que peut rencontrer l'utilisateur.
Ceux-ci sont résumés dans la Figure \ref{fig:page_fault}.

\begin{figure}
	\centering



	\caption[Les différents défauts de page]{Les différents défauts de page \cite{}}
	\label{fig:page_fault}
\end{figure}