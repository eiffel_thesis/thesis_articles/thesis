\section{Allocation d'une \texttt{vm\_area\_struct}}
\label{sec:vm_area_struct_alloc}
Une \texttt{vm\_area\_struct} peut être allouée suite à deux événements distincts.
Nous verrons, dans un premier temps, l'allocation de cette structure suite à la création d'une nouvelle tâche.
Dans un second temps, nous étudierons cette allocation après l'appel aux fonctions utilisateurs permettant de manipuler la mémoire.

\subsection{Suite à la création d'une tâche}
\label{subsec:vm_area_struct_alloc_new_task}
Suite à l'appel système \texttt{clone}, initié par l'appel à la fonction \texttt{fork} de la \texttt{glibc}, les \texttt{vm\_area\_struct} du parent seront copiées à l'enfant.
Nous nous plaçons donc dans la fonction \texttt{dup\_mm}, juste après l'initialisation de la \texttt{mm\_struct} de l'enfant \citelinux{kernel/fork.c}{1274}.
La fonction \texttt{dup\_mmap} sera appelée afin de copier, une à une, les \texttt{vm\_area\_struct} de la tâche parent dans la liste de son enfant, pointée par \texttt{mmap} \citelinux{kernel/fork.c}{421}.

De plus, et ce pour chaque \texttt{vm\_area\_struct} du parent, les entrées de la table des pages de celle-ci seront copiées à l'enfant via l'appel à la fonction \texttt{copy\_page\_range} \citelinux{kernel/fork.c}{544}.
La copie de ces entrées s'effectue en parcourant la table des pages du parent, cette copie est donc un procédé récursif.
Ainsi, \texttt{copy\_page\_range} copiera chaque niveau 4 de la table des pages du parent à son enfant grâce à la fonction \texttt{copy\_p4d\_range} \citelinux{mm/memory.c}{1216}\citelinux{mm/memory.c}{1271}.
Après s'être occupé du niveau 4 qui lui aura été donné en argument, \texttt{copy\_p4d\_range} appellera \texttt{copy\_pud\_range} pour copier les niveaux 3 de la table de pages du parent dans son enfant \citelinux{mm/memory.c}{1194}\citelinux{mm/memory.c}{1209}.
Puis, celle-ci déléguera la copie des niveaux 2 du parent à la fonction \texttt{copy\_pmd\_range} \citelinux{mm/memory.c}{1160}\citelinux{mm/memory.c}{1186}.
À son tour, et après avoir copié les niveaux 2, elle invoquera \texttt{copy\_pte\_range} pour copier les niveaux 1 \citelinux{mm/memory.c}{1126}\citelinux{mm/memory.c}{1153}.
Enfin, la fonction \texttt{copy\_one\_pte} sera appelée par \texttt{copy\_pte\_range} pour copier, une par une, les entrées terminales \citelinux{mm/memory.c}{1063}\citelinux{mm/memory.c}{1102}.
Les entrées ainsi copiées sont aussi marquées comme accessible, pour le parent et son enfant, en lecture seule \citelinux{mm/memory.c}{1025}.
Chaque page copiée verra son nombre de référence, \texttt{\_refcount}, incrémenté puisqu'elle est désormais utilisée dans deux tables des pages : celle du parent et celle de l'enfant \citelinux{mm/memory.c}{1040}\citelinux{include/linux/mm.h}{918}.
Aucune page physique terminale n'est donc allouée.
Après copie de la table des pages, l'enfant pourra accéder aux pages physiques, héritées de son parent, sans causer d'accès illégal.

% \citelinux{arch/x86/include/asm/unistd.h}{53}.
% Le COW consiste juste à allouer une nouvelle page physique que la page du parent pour l'adresse virtuelle ayant causé le défaut de page.

\subsection{Appels système côté utilisateur}
\label{subsec:vm_area_struct_alloc_user}
Dans cette section, nous aborderons les différentes fonctions exécutées en espace utilisateur qui mène à la création d'une nouvelle \texttt{vm\_area\_struct}.
Nous commencerons par étudier les événements se produisant lors d'une allocation mémoire depuis le tas.
Nous nous intéresserons ensuite à la projection en mémoire pour terminer par la fonction permettant d'augmenter la taille du tas.

\subsubsection{Allocation mémoire depuis le tas}
\label{subsubsec:vm_area_struct_alloc_mmap_heap}
La fonction permettant d'allouer de la mémoire depuis le tas est la bien connue \texttt{malloc} \cite[fichier \texttt{sysdeps/unix/sysv/linux/x86\_64/vfork.S}, ligne 93]{gnu_community_debian_nodate}.
Dans la \texttt{glibc}, \texttt{malloc} n'est pas un appel système et appelle \texttt{brk}\footnote{
	Comme pour les fonctions précédentes, ce résultat peut être confirmé par l'utilisation de \texttt{strace} ou du couple \texttt{callgrind}/\texttt{kcachegrind}.

	Ainsi, \texttt{mmap} correspond en réalité à \texttt{\_\_libc\_malloc} \cite[fichier \texttt{malloc/malloc.c}, ligne 5570]{gnu_community_debian_nodate}.
	Elle appelle à son tour \texttt{\_int\_malloc} qui consiste en l'allocateur mémoire de la \texttt{glibc} \cite[fichier \texttt{malloc/malloc.c}, ligne 3019]{gnu_community_debian_nodate}\cite[fichier \texttt{malloc/malloc.c}, ligne 3057]{gnu_community_debian_nodate}.
	S'il n'y a plus de mémoire disponible, cet allocateur se résoudra à appeler \texttt{sysmalloc} pour en obtenir \cite[fichier \texttt{malloc/malloc.c}, ligne 3512]{gnu_community_debian_nodate}\cite[fichier \texttt{malloc/malloc.c}, ligne 3551]{gnu_community_debian_nodate}.
	Cette fonction demande de la mémoire au système d'exploitation comme l'indique le commentaire suivant \cite[fichier \texttt{malloc/malloc.c}, ligne 2250]{gnu_community_debian_nodate}\cite[fichier \texttt{malloc/malloc.c}, ligne 2257]{gnu_community_debian_nodate}:
	\begin{quote}
		sysmalloc handles malloc cases requiring more memory from the system.
	\end{quote}
	\texttt{sysmalloc} pourra appeler la macro \texttt{MORECORE} qui cache un appel à \texttt{sbrk} \cite[fichier \texttt{malloc/malloc.c}, ligne 2473]{gnu_community_debian_nodate}\cite[fichier \texttt{malloc/malloc.c}, ligne 381]{gnu_community_debian_nodate}.
	\texttt{sbrk} correspond à \texttt{\_\_sbrk} \cite[fichier \texttt{misc/sbrk.c}, ligne 62]{gnu_community_debian_nodate}\cite[fichier \texttt{misc/sbrk.c}, ligne 31]{gnu_community_debian_nodate}.
	Cette dernière invoque \texttt{\_\_brk} \cite[fichier \texttt{misc/sbrk.c}, ligne 41]{gnu_community_debian_nodate}\cite[fichier \texttt{sysdeps/unix/sysv/linux/x86\_64/brk.c}, ligne 27]{gnu_community_debian_nodate}.
	Cette fonction fera passer le programme en mode système \cite[fichier \texttt{sysdeps/unix/sysv/linux/x86\_64/brk.c}, ligne 31]{gnu_community_debian_nodate}.
} ou \texttt{mmap}\footnote{
	Ce résultat peut aussi être trouvé grâce aux outils précédemment utilisés.

	Dans le code la \texttt{glibc} \texttt{sysmalloc} pourra appeler la macro \texttt{MMAP} qui cache un appel à \texttt{\_\_mmap} \cite[fichier \texttt{malloc/malloc.c}, ligne 2313]{gnu_community_debian_nodate}\cite[fichier \texttt{malloc/malloc.c}, ligne 1029]{gnu_community_debian_nodate}.
	\texttt{\_\_mmap} correspond, sur un système 64 bits, à \texttt{\_\_mmap64} \cite[fichier \texttt{sysdeps/unix/sysv/linux/mmap64.c}, ligne 60]{gnu_community_debian_nodate}\cite[fichier \texttt{sysdeps/unix/sysv/linux/mmap64.c}, ligne 40]{gnu_community_debian_nodate}.
	Cette fonction fera passer le processus en mode système via un appel système à \texttt{mmap} ou \texttt{mmap2} \cite[fichier \texttt{sysdeps/unix/sysv/linux/mmap64.c}, ligne 42]{gnu_community_debian_nodate}\cite[fichier \texttt{sysdeps/unix/sysv/linux/mmap64.c}, ligne 45]{gnu_community_debian_nodate}.
} lorsque l'allocateur mémoire de la \texttt{glibc} ne dispose plus d'assez de mémoire \cite{sploitfun_syscalls_2015}.
Le distinguo entre \texttt{brk} ou \texttt{mmap} s'effectue selon la quantité de mémoire que l'utilisateur souhaite allouer.
Si celle-ci est supérieure à un seuil donné, \texttt{mmap} sera chargé de répondre à cette requête, sinon, ce sera \texttt{brk} qui sera appelé \cite{delorie_overview_2019, }.

\subsubsection{Projection en mémoire}
\label{subsubsec:vm_area_struct_alloc_mmap}
L'appel système \texttt{mmap} permet de projeter un fichier en mémoire ou d'allouer de la mémoire anonyme.
Cet appel système, dans la \texttt{glibc} fait passer le processus appelant en mode système \cite[fichier \texttt{sysdeps/unix/sysv/linux/mmap64.c}, ligne 59]{gnu_community_debian_nodate}\cite[fichier \texttt{sysdeps/unix/sysv/linux/mmap64.c}, ligne 60]{gnu_community_debian_nodate}\cite[fichier \texttt{sysdeps/unix/sysv/linux/mmap64.c}, ligne 40]{gnu_community_debian_nodate}.

\subsubsection{Augmentation de la taille du tas}
\label{subsubsec:vm_area_struct_alloc_mmap}
Pour augmenter la taille du tas, les fonctions \texttt{brk} et \texttt{sbrk} peuvent être utilisées.
Dans la \texttt{glibc}, la fonction \texttt{brk} correspond à \texttt{\_\_brk} \cite[fichier \texttt{sysdeps/unix/sysv/linux/x86\_64/brk.c}, ligne 41]{gnu_community_debian_nodate}.
Celle-ci fait passer le processus appelant en mode système.
Quant à la fonction \texttt{sbrk}, elle se rapporte à \texttt{\_\_sbrk} qui appelle \texttt{\_\_brk} \cite[fichier \texttt{misc/sbrk.c}, ligne 62]{gnu_community_debian_nodate}\cite[fichier \texttt{misc/sbrk.c}, ligne 31]{gnu_community_debian_nodate}.

Les différentes fonctions utilisateurs permettant d'allouer de la mémoire, ainsi que les appels systèmes correspondant, sont résumées dans le Tableau \ref{table:memory_allocation_functions_sumup}.

\begin{table}
	\centering

	\caption{Tableau récapitulatif des différentes fonction utilisateurs pour allouer la mémoire}
	\label{table:memory_allocation_functions_sumup}

	\begin{tabular}{ccc}
		\hline
		Fonction utilisateur & Appel système correspondant\\
		\hline
		\texttt{malloc} & \texttt{\_\_mmap}/\texttt{\_\_brk}\\
		\texttt{mmap} & \texttt{\_\_mmap}/\\
		\texttt{sbrk} & \texttt{\_\_brk}\\
		\texttt{brk} & \texttt{\_\_brk}\\
		\hline
	\end{tabular}
\end{table}

\subsection{Appels système côté noyau}
\label{subsec:vm_area_struct_alloc_kernel}
Nous allons désormais nous intéresser à ce qui se passe lorsque le processus est entré en mode noyau suite à un appel à \texttt{\_\_mmap} ou \texttt{\_\_brk}.

\subsubsection{Suite à un appel à \texttt{\_\_mmap}}
\label{subsubsec:vm_area_struct_alloc___mmap}
Pour un noyau compilé en vue de s'exécuter sur un processeur d'architecture x86\_64, \texttt{\_\_mmap} correspond à \texttt{mmap} \citelinux{arch/x86/kernel/sys\_x86\_64.c}{91}.
Celle-ci appelera \texttt{ksys\_mmap\_pgoff} \citelinux{arch/x86/kernel/sys\_x86\_64.c}{100}\citelinux{mm/mmap.c}{1543}.
Cette fonction vérifie notamment que le descripteur de fichier donné en argument correspond bien à un fichier, elle appelle ensuite \texttt{vm\_mmap\_pgoff} \citelinux{mm/mmap.c}{1585}\citelinux{mm/util.c}{344}.
Celle-ci appelle à son tour \texttt{do\_mmap\_pgoff} qui consiste en un appel à \texttt{do\_mmap}\citelinux{mm/util.c}{357}\citelinux{include/linux/mm.h}{2293}\citelinux{include/linux/mm.h}{2298}\citelinux{mm/mmap.c}{1359}.

C'est à \texttt{do\_mmap} que revient la gestion de la \texttt{vm\_area\_struct}.
Elle va, dans un premier temps, essayer de trouver une adresse convenant à l'association via la fonction \texttt{get\_unmapped\_area} \citelinux{mm/mmap.c}{1406}\citelinux{mm/mmap.c}{2147}.
Cette fonction va appeler la fonction \texttt{get\_area} qui est définie soit par le système de fichier\footnote{
	Certains systèmes de fichiers définissent une fonction \texttt{get\_area} mais ce n'est pas une généralité.
	Par exemple, le système de fichier \texttt{ext4} la définie mais ce n'est pas le cas de \texttt{btrfs} \citelinux{fs/ext4/file.c}{510}\citelinux{fs/btrfs/file.c}{3258}.
} auquel le fichier à projeter appartient soit par l'architecture.
Dans notre étude, nous nous intéresserons au second cas.
Ainsi, pour l'architecture x86\_64, le pointeur de fonction \texttt{get\_area} cache la fonction \texttt{arch\_get\_unmapped\_area\_topdown}\footnote{
	C'est cette fonction qui est stockée dans le pointeur de fonction et non \texttt{arch\_get\_unmapped\_area} car la fonction \texttt{mmap\_is\_legacy} renvoie la valeur de \texttt{vm.legacy\_va\_layout} qui vaut 0 pour le système exécutant l'éditeur de texte ayant permis d'écrire cette thèse \citelinux{arch/x86/mm/mmap.c}{143}\citelinux{arch/x86/kernel/sys\_x86\_64.c}{132}.
	La valeur \texttt{vm.legacy\_va\_layout} est accessible via \texttt{sysctl ---all}.
} \citelinux{arch/x86/mm/mmap.c}{145}
\citelinux{arch/x86/kernel/sys\_x86\_64.c}{174}.
Cette fonction appelle ensuite la fonction \texttt{vm\_unmapped\_area} qui cherchera une plage d'adresse qui n'est pas déjà projetée en mémoire \citelinux{arch/x86/kernel/sys\_x86\_64.c}{231}\citelinux{include/linux/mm.h}{2344}.
La plage d'adresse recherchée devra répondre aux 4 critères suivants \citelinux{include/linux/mm.h}{2335}:
\begin{enumerate}
	\item Avoir une intersection vide avec les autres \texttt{vm\_area\_struct} de la tâche.
	\item Appartenir à l'intervalle d'adresse virtuelle délimité par la plus basse et la plus haute de ces adresses.
	\item Avoir la taille demandée par l'utilisateur.
	\item Si la fonction est appelée pour projeter un fichier en mémoire alors la projection devra répondre à un certain alignement mémoire.
\end{enumerate}
Pour cela, \texttt{vm\_unmapped\_area} fera appel à \texttt{unmapped\_area\_topdown} \citelinux{include/linux/mm.h}{2347}
\citelinux{mm/mmap.c}{1946}.
Cette fonction va explorer l'arbre rouge noir des \texttt{vm\_area\_struct} afin de trouver un tel intervalle.

Une fois celui-ci trouvé, son adresse du début est renvoyée jusqu'à \texttt{do\_mmap}.
Les drapeaux des différents types de projection sont ensuite gérés avant l'appel à la fonction \texttt{mmap\_region} \citelinux{mm/mmap.c}{1535}\citelinux{mm/mmap.c}{1677}.

Cette fonction teste si l'espace d'adressage linéaire de la tâche courante peut croître via la fonction \texttt{may\_expand\_vm} \citelinux{mm/mmap.c}{1535}\citelinux{mm/mmap.c}{3229}.
Celle-ci teste principalement si l'espace d'adressage courant ne dépasse pas les limites \texttt{RLIMIT\_AS} et \texttt{RLIMIT\_DATA}\footnote{
	Ces limites sont consultables via l'appel système \texttt{getrlimit} \cite{}.
} \citelinux{mm/mmap.c}{3229}\citelinux{mm/mmap.c}{3231}\citelinux{mm/mmap.c}{3234}.

Si l'espace d'adressage courant peut croître, les \texttt{vm\_area\_struct} qui recouvrent l'adresse renvoyée par \texttt{get\_unmapped\_area} seront supprimées car elles seront recouvertes par celle qui sera créée \citelinux{mm/mmap.c}{1703}.
Pour être honnête, même en regardant le code, je n'ai pas compris cette partie, l'explication livrée est donc tirée de cette citation \cite[page 266]{gorman_understanding_2004} :
\begin{quote}
	If a VMA was found and it is part of the new mmaping, this removes the old mapping because the new one will cover both.
\end{quote}

Ensuite, \texttt{mmap\_region} va essayer d'agrandir une \texttt{vm\_area\_struct} existante grâce \texttt{vma\_merge} \citelinux{mm/mmap.c}{1722}\citelinux{mm/mmap.c}{1108}.
Bien entendu, pour agrandir une \texttt{vm\_area\_struct} existante, il faut que les drapeaux de celle-ci et ceux renseignés par l'utilisateur concordent.
Ainsi, une \texttt{vm\_area\_struct} en lecture seule ne pourra être étendue si l'utilisateur a appelé \texttt{mmap} avec le drapeau \texttt{PROT\_WRITE}.
Cette vérification sera faite au moyen des fonctions \texttt{can\_vma\_merge\_after} et \texttt{can\_vma\_merge\_before} \citelinux{mm/mmap.c}{1144}\citelinux{mm/mmap.c}{1152}\citelinux{mm/mmap.c}{1176}\citelinux{mm/mmap.c}{1053}\citelinux{mm/mmap.c}{1032}.
Ces deux fonctions s'appuient sur les fonctions \texttt{is\_mergeable\_vma} et \texttt{is\_mergeable\_anon\_vma} \citelinux{mm/mmap.c}{1058}\citelinux{mm/mmap.c}{1059}\citelinux{mm/mmap.c}{1037}\citelinux{mm/mmap.c}{1038}.
La première vérifiera, entre autres, si les drapeaux correspondent \citelinux{mm/mmap.c}{983}.
Si ces fonctions indiquent qu'un agrandissement est possible, la fonction \texttt{\_\_vma\_adjust} s'en chargera \citelinux{mm/mmap.c}{1159}\citelinux{mm/mmap.c}{1163}\citelinux{mm/mmap.c}{1180}\citelinux{mm/mmap.c}{1183}\citelinux{mm/mmap.c}{690}.
Les différents cas d'agrandissement sont représentés dans la Figure \ref{fig:vm_area_struct_alloc_merge}.

\begin{figure}
	\centering



	\caption{Différents cas d'agrandissement d'une \texttt{vm\_area\_struct} existante lors de la création d'une nouvelle}
	\label{fig:vm_area_struct_alloc_merge}
\end{figure}

Si l'agrandissement n'est pas possible, la fonction \texttt{vma\_merge} renverra \texttt{NULL}.
La fonction \texttt{mmap\_region} va donc allouer une nouvelle \texttt{vm\_area\_struct} \citelinux{mm/mmap.c}{1732}\citelinux{kernel/fork.c}{311}.
Les champs de celle-ci seront initialisés en fonction de son utilisation.
Ainsi, quand elle est destinée à projeter un fichier, la fonction \texttt{mmap} du système de fichier\footnote{
	Pour \texttt{btrfs}, ce pointeur de fonction stocke l'adresse de la fonction \texttt{btrfs\_file\_mmap} \citelinux{fs/btrfs/file.c}{3263}.
} sera appelée via \texttt{call\_mmap} \citelinux{mm/mmap.c}{1762}\citelinux{include/linux/fs.h}{1811}.
Si elle est employée pour un segment de mémoire partagée, la fonction \texttt{shmem\_zero\_setup} sera chargée de son initialisation \citelinux{mm/mmap.c}{1778}\citelinux{mm/shmem.c}{3981}.
Dans le dernier cas, la \texttt{vm\_area\_struct} est utilisée pour de la mémoire anonyme, \texttt{vma\_set\_anonymous} mettra à \texttt{NULL} son champ \texttt{vm\_ops} pour signifier cela \citelinux{mm/mmap.c}{1782}\citelinux{include/linux/mm.h}{465}.

La \texttt{vm\_area\_struct} nouvellement créée est ensuite ajoutée à la liste et l'arbre rouge noir des \texttt{vm\_area\_struct} de la tâche par la fonction \texttt{vma\_link} \citelinux{mm/mmap.c}{1785}\citelinux{mm/mmap.c}{613}.
Cette fonction appelle \texttt{\_\_vma\_link} qui rendra effectif l'ajout de la \texttt{vm\_area\_struct} dans les structures de données mentionnées en appelant à son tour \texttt{\_\_vma\_link\_list} et \texttt{\_\_vma\_link\_rb} \citelinux{mm/mmap.c}{1762}\citelinux{mm/mmap.c}{605}\citelinux{mm/mmap.c}{609}\citelinux{mm/mmap.c}{610}.

Ensuite, les statistiques mémoire de la tâche sont mises à jour pour prendre en compte la nouvelle \texttt{vm\_area\_struct}.
Cette tâche revient à la fonction \texttt{vm\_stat\_account} \citelinux{mm/mmap.c}{1797}\citelinux{mm/mmap.c}{3254}.

Enfin, la fonction \texttt{vma\_set\_page\_prot} sera appelée pour faire concorder les drapeaux de la \texttt{vm\_area\_struct} aux drapeaux des pages qu'elle contient \citelinux{mm/mmap.c}{1819}\citelinux{mm/mmap.c}{126}.
Pour ce faire, cette fonction fera appel à la fonction \texttt{vm\_pgprot\_modify} \citelinux{mm/mmap.c}{131}\citelinux{mm/mmap.c}{120}.

La Figure \ref{fig:call_stack_vm_area_struct} résume les fonctions prenant part à la création ou l'agrandissement d'une \texttt{vm\_area\_struct} suite à un appel à \texttt{\_\_mmap}.

\begin{figure}
	\centering



	\caption{Appels de fonctions allouant ou agrandissant une \texttt{vm\_area\_struct} suite à un appel à \texttt{\_\_mmap}}
	\label{fig:call_stack_vm_area_struct}
\end{figure}

\subsubsection{Suite à un appel à \texttt{\_\_brk}}
\label{subsubsec:vm_area_struct_alloc___brk}
Le point d'entrée, dans le noyau, de cet appel système est la fonction \texttt{brk} \citelinux{mm/mmap.c}{191}.
Dans le cas où \texttt{\_\_brk} est appelé pour diminuer la taille du tas, la fonction \texttt{do\_munmap} sera appelée \citelinux{mm/mmap.c}{237}\citelinux{mm/mmap.c}{2690}.
Si cette fonction a réussi à abaisser la taille du tas à celle demandée par l'utilisateur, \texttt{brk} continuera en mettant à jour la limite du tas pour l'espace d'adressage virtuel de la tâche courante.

Dans le cas où l'appel système aurait été invoqué pour augmenter la taille du tas suite, par exemple, à un \texttt{malloc} conséquent.
La \texttt{vm\_area\_struct} qui suit l'adresse du tas sera recherchée via la fonction \texttt{find\_vma} \citelinux{mm/mmap.c}{243}.
Si une telle structure existe et que la valeur souhaitée pour le tas empiète sur celle-ci, la fonction noyau renverra la taille actuelle du tas ce qui est synonyme d'échec \citelinux{mm/mmap.c}{245}\cite[Section "Linux notes"]{noauthor_brk2_nodate}.

Si cette structure n'existe pas, ou que la valeur demandée est raisonnable, l'augmentation de la taille du tas continuera dans la fonction \texttt{do\_brk\_flags} \citelinux{mm/mmap.c}{248}\citelinux{mm/mmap.c}{2928}.
Comme l'indique le commentaire suivant, cette fonction peut être vue comme une version simplifiée de \texttt{do\_mmap} gérant seulement le tas \citelinux{mm/mmap.c}{2924} :
\begin{quote}
	this is really a simplified "do\_mmap". it only handles anonymous maps.
\end{quote}
Comme \texttt{do\_mmap}, cette fonction appelle \texttt{get\_unmapped\_area} mais uniquement pour vérifier que celle-ci ne renvoie pas d'erreur \citelinux{mm/mmap.c}{2941}.

Par la suite, elle supprime les \texttt{vm\_area\_struct} qui recouvrent potentiellement l'adresse de fin du tas \citelinux{mm/mmap.c}{2958}.

Similairement à \texttt{do\_mmap}, \texttt{may\_expand\_vm} sera appelée \citelinux{mm/mmap.c}{2965}.
Si elle échoue, la taille du tas ne sera pas modifiée.

Puis, \texttt{do\_brk\_flags} essayera d'agrandir une \texttt{vm\_area\_struct} existante en appelant \texttt{vma\_merge} \citelinux{mm/mmap.c}{2975}.
En cas d'échec, une nouvelle \texttt{vm\_area\_struct} sera allouée et initialisée comme étant destinée à stocker de la mémoire anonyme \citelinux{mm/mmap.c}{2983}\citelinux{mm/mmap.c}{2989}.
La nouvelle structure sera ajoutée à la liste et à l'arbre des \texttt{vm\_area\_struct} \citelinux{mm/mmap.c}{2995}.

Si l'allocation de la nouvelle \texttt{vm\_area\_struct} s'est bien déroulée, la fonction \texttt{do\_brk\_flags} se terminera.
La fonction \texttt{brk} pourra donc continuer en mettant à jour la nouvelle taille du tas avant de la renvoyer à l'utilisateur \citelinux{mm/mmap.c}{252}\citelinux{mm/mmap.c}{258}.

% https://elixir.bootlin.com/linux/v4.19/source/mm/mmap.c#L2690 do_munmap diminue un espace d'adressage virtuel voir même désaoulle des vm_area_struct.