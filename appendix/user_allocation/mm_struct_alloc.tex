\section{Allocation d'une \texttt{mm\_struct}}
\label{sec:mm_struct_alloc}
Puisqu'une \texttt{mm\_struct} décrit l'espace d'adressage virtuel associé à une \texttt{task\_struct}, une \texttt{mm\_struct} peut être créée lors de la création d'une \texttt{task\_struct}.
Dans cette section, nous verrons tout d'abord, côté utilisateur, les appels système qui mènent à la création d'une \texttt{task\_struct} puis nous étudierons les fonctions appelées, dans le noyau, en conséquence.

\subsection{Appels système côté utilisateur}
\label{subsec:user_viewpoint}
Dans les sous-sections suivantes, nous aborderons la création d'un processus ou d'un thread en se plaçant du côté utilisateur\footnote{
	La bibliothèque étudiée est la \texttt{glibc}, la bibliothèque standard du projet GNU, dans sa version 2.28-10 qui, lors de l'écriture de ses lignes, est utilisée dans debian \cite{gnu_community_debian_nodate}.
}.
Nous verrons donc les appels systèmes permettant de créer l'un et l'autre.
Rappelons avant tout, que du point de vue UNIX, un processus est une application associée à un espace d'adressage virtuel tandis qu'un thread est un processus léger appartenant à une application.
Un thread partage donc l'espace d'adressage virtuel de l'application avec les autres threads de cette application.
Un processus comporte toujours un thread : le thread main \footnote{
	Lors du débogage d'un programme avec l'outil \texttt{gdb}, la commande \texttt{info threads} permet d'obtenir des informations sur les threads du programme.
	Ainsi, au démarrage du programme, la liste affichée par cette commande ne contient toujours qu'un thread.
}.

\subsubsection{Création de processus UNIX}
La création d'un nouveau processus, dans un système UNIX, s'opère via l'appel système \texttt{fork} ou sa variante \texttt{vfork} \cite{noauthor_fork2_nodate, noauthor_vfork2_nodate}.
L'appel système créé, à la manière d'une division cellulaire, un nouveau processus à partir du processus courant.
Le nouveau processus, dit processus enfant, héritera donc des caractéristiques du processus ayant mené à sa création, dit processus parent.
La spécialisation du processus enfant en un processus différent se fera via l'invocation d'une fonction de la famille \texttt{exec}\footnote{
	Ces fonctions mènent à l'appel système \texttt{execve} \cite{noauthor_execve2_nodate}.
} \cite{noauthor_exec3_nodate}.

Néanmoins, la fonction \texttt{fork} de la \texttt{glibc} ne peut être qualifiée d'appel système.
En effet, celle-ci n'appelle pas l'appel système correspondant, c'est-à-dire \texttt{fork}, mais \texttt{clone}\footnote{
	Le logiciel \texttt{strace}, qui peut être attaché à un programme, intercepte les appels systèmes générés par celui-ci.
	Ainsi, lorsque \texttt{strace} est attaché à un programme appelant \texttt{fork}, la liste des appels systèmes ne contient pas d'appel à \texttt{fork} mais un appel à \texttt{clone} \cite{noauthor_strace1_nodate}.

	Exécuter ce même programme dans l'environnement d'exécution \texttt{valgrind}, avec l'outil \texttt{callgrind} qui permet d'obtenir le graphe des appels de fonction du programme, montre aussi, après visualisation des résultats avec l'outil \texttt{kcachegrind}, la présence de l'appel système \texttt{clone} aux dépens de \texttt{fork} \cite{valgrind_developers_callgrind_nodate, weidendorfer_kcachegrind_nodate}.

	Ce résultat expérimental peut être confirmé en étudiant le code, qui détient la vérité, de la \texttt{glibc}.
	La fonction \texttt{fork} de la \texttt{glibc} appelle la fonction \texttt{arch\_fork} \cite[fichier \texttt{sysdeps/nptl/fork.c}, ligne 48]{gnu_community_debian_nodate}\cite[fichier \texttt{sysdeps/nptl/fork.c}, ligne 76]{gnu_community_debian_nodate}.
	La fonction \texttt{arch\_fork}, invoque l'appel système \texttt{clone} \cite[fichier \texttt{sysdeps/unix/sysv/linux/arch-fork.h}, ligne 32]{gnu_community_debian_nodate}.
	Cette invocation amène sûrement à l'exécution de l'appel système clone en assembleur \cite[fichier \texttt{sysdeps/unix/sysv/linux/x86\_64/clone.S}, ligne 50]{gnu_community_debian_nodate}.
}.

L'appel système \texttt{vfork} diffère de \texttt{fork} dans le fait que certaines parties de l'espace d'adressage virtuel du processus parent ne seront pas copiées pour créer celui du processus enfant\footnote{
	Contrairement à \texttt{fork}, \texttt{vfork} est un vrai appel système puisqu'il invoque bien \texttt{vfork}.
	Ce résultat peut être trouvé expérimentalement grâce aux outils \texttt{strace}, \texttt{valgrind} et \texttt{kcachegrind} présentés précédemment.

	La \texttt{glibc} semble coder cet appel système directement en assembleur \cite[fichier \texttt{sysdeps/unix/sysv/linux/x86\_64/vfork.S}, ligne 93]{gnu_community_debian_nodate}
}.
Les parties incriminées seront étudiées dans la Section \ref{subsec:kernel_viewpoint}.
Puisque certaines structures ne sont pas copiées, \texttt{vfork} est plus rapide que \texttt{fork} \cite{zahariev_fork_2009}.
Cet appel système devrait donc être préféré à \texttt{fork} lorsque la création du processus enfant précède sa spécialisation via une fonction de la famille \texttt{exec}.
Cet appel a aussi la particularité de bloquer le processus parent jusqu'à ce que le processus enfant ait appelé \texttt{\_exit} ou... une fonction de la famille \texttt{exec} !

\subsubsection{Création de thread POSIX}
La fonction permettant de créer un thread POSIX est \texttt{pthread\_create} \cite{}.
Cette fonction n'est pas un appel système, mais elle invoque, comme \texttt{fork}, l'appel système \texttt{clone}\footnote{
	Comme pour les fonctions menant à la création d'un processus UNIX, le résultat de l'appel de \texttt{clone} par \texttt{pthread\_create} peut être trouvé expérimentalement en utilisant \texttt{strace}, \texttt{valgrind} et \texttt{kcachegrind}.

	La fonction \texttt{pthread\_create} correspond à la fonction \texttt{\_\_pthread\_create\_2\_1} de la \texttt{glibc} \cite[fichier \texttt{nptl/pthread\_create.c}, ligne 632]{gnu_community_debian_nodate}.
	Cette fonction appelle la fonction \texttt{create\_thread} \cite[fichier \texttt{nptl/pthread\_create.c}, ligne 794]{gnu_community_debian_nodate}\cite[fichier \texttt{nptl/pthread\_create.c}, ligne 826]{gnu_community_debian_nodate}.
	La fonction \texttt{create\_thread} appelle à son tour la macro \texttt{ARCH\_CLONE} qui est définie comme \texttt{\_\_clone} \cite[fichier \texttt{sysdeps/unix/sysv/linux/createthread.c}, ligne 49]{gnu_community_debian_nodate}\cite[fichier \texttt{sysdeps/unix/sysv/linux/createthread.c}, ligne 93]{gnu_community_debian_nodate}\cite[fichier \texttt{sysdeps/unix/sysv/linux/createthread.c}, ligne 34]{gnu_community_debian_nodate}.
	\texttt{\_\_clone} correspond à du code assembleur \cite[fichier \texttt{sysdeps/unix/sysv/linux/x86\_64/clone.S}, ligne 50]{gnu_community_debian_nodate}.
}.

Les différentes fonctions utilisateurs permettant de créer une tâche, ainsi que leurs appels systèmes correspondant, sont résumées dans le Tableau \ref{table:create_task_functions_sumup}.

\begin{table}
	\centering

	\caption{Tableau récapitulatif des différentes fonctions utilisateurs permettant de créer une tâche}
	\label{table:create_task_functions_sumup}

	\begin{tabular}{ccc}
		\hline
		Fonction utilisateur & Appel système correspondant\\
		\hline
		\texttt{fork} & \texttt{\_\_clone}\\
		\texttt{vfork} & code assembleur\\
		\texttt{pthread\_create} & \texttt{\_\_clone}\\
		\hline
	\end{tabular}
\end{table}

\subsection{Appels système côté noyau}
\label{subsec:kernel_viewpoint}
Comme nous l'avons vu dans la Section \ref{subsec:user_viewpoint}, deux appels systèmes permettent de créer une nouvelle tâche : \texttt{vfork} et \texttt{clone}.
Dans Linux, ces appels système appellent la fonction noyau \texttt{\_do\_fork} avec des drapeaux différents \citelinux{kernel/fork.c}{2233}\citelinux{kernel/fork.c}{2135}.
Cette fonction va, à son tour, appeler la fonction \texttt{copy\_process} \citelinux{kernel/fork.c}{2166}\citelinux{kernel/fork.c}{1628}.
La fonction \texttt{copy\_process} va, comme son nom l'indique, initialiser les données de la tâche enfant à partir de la tâche parent notamment l'espace d'adressage virtuel en appelant la fonction \texttt{copy\_mm} \cite[\texttt{kernel/fork.c}, ligne 1869]{noauthor_linux_2018}\citelinux{kernel/fork.c}{1295}.

La fonction \texttt{copy\_mm} distingue deux cas qu'il est intéressant d'étudier :

\begin{itemize}
	\item Si le drapeau \texttt{CLONE\_VM} est donné en argument de cette fonction alors la tâche enfant aura le même espace d'adressage virtuel que la tâche parent (le champ \texttt{mm\_users} de la \texttt{task\_struct} parent sera incrémenté).
	Ceci marquera la fin de la gestion mémoire lors d'un \texttt{fork}.
	Ce drapeau est mis à 1 lorsque l'appel à \texttt{copy\_mm} fait suite aux appels système \texttt{vfork} ou \texttt{clone}, \texttt{clone} étant appelé par la fonction \texttt{pthread\_create}\footnote{
		Ce drapeau est donné en argument de la macro \texttt{ARCH\_CLONE} \cite[fichier \texttt{sysdeps/unix/sysv/linux/createthread.c}, ligne 93]{gnu_community_debian_nodate}.
	} \citelinux{kernel/fork.c}{2244}.
	\item Sinon, la \texttt{mm\_struct} de la tâche parent sera copiée et allouée à la tâche enfant dans la suite des fonctions appelées.
	C'est le cas lors d'un appel à \texttt{clone} faisant suite à la fonction \texttt{fork} de la \texttt{glibc}\footnote{
		Le drapeau \texttt{CLONE\_VM} n'est en effet pas mis à 1 avant l'invocation de l'appel système \texttt{clone} par la fonction \texttt{arch\_fork} de la \texttt{glibc} \cite[fichier \texttt{sysdeps/unix/sysv/linux/arch-fork.h}, ligne 32]{gnu_community_debian_nodate}.
	}.
\end{itemize}

Dans le second cas, la gestion mémoire lors d'un \texttt{fork} continuera par l'appel à la fonction \texttt{dup\_mm} par la fonction \texttt{copy\_mm} \citelinux{kernel/fork.c}{1329}\citelinux{kernel/fork.c}{1260}.
La fonction \texttt{dup\_mm} allouera une nouvelle \texttt{mm\_struct} pour la tâche enfant, le contenu de la \texttt{mm\_struct} de la tâche parent sera copié dans celle de son enfant.
Elle appellera ensuite la fonction \texttt{mm\_init} \citelinux{kernel/fork.c}{1271}\citelinux{kernel/fork.c}{924}.
Celle-ci appelle la fonction \texttt{mm\_pgd\_alloc} \citelinux{kernel/fork.c}{962}\citelinux{kernel/fork.c}{572}.
La fonction \texttt{mm\_pgd\_alloc} lancera l'allocation de la table des pages de la tâche enfant via l'appel à \texttt{pgd\_alloc}, \citelinux{kernel/fork.c}{574}\citelinux{arch/x86/mm/pgtable.c}{434}.
Puisque la table des pages est propre au matériel, la définition de cette fonction varie d'une architecture à l'autre.

Les différents appels de fonction menant à l'allocation et l'initialisation d'une \texttt{mm\_struct} sont résumés dans la Figure \ref{fig:call_stack_mm_struct}.

\begin{figure}
	\centering



	\caption{Appels de fonction menant à l'allocation et l'initialisation d'une \texttt{mm\_struct}}
	\label{fig:call_stack_mm_struct}
\end{figure}