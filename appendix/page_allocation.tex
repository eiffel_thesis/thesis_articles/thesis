\chapter{L'allocation de pages dans le noyau Linux}
\label{chapter:page_allocation}
\section{Introduction}
\label{sec:page_allocation_intro}
Dans le chapitre précédent, nous avons vu à quel moment sont allouées les structures \texttt{mm\_struct}, \texttt{vm\_area\_struct} et \texttt{struct page} ainsi que les entrées des tables des pages.
Dans ce chapitre, nous allons nous intéresser à l'allocation des pages en elle-même.
Comme nous l'avons indiqué dans la Section \ref{sec:zone}, Linux s'appuie sur deux mécanismes pour allouer des pages : le \textit{buddy allocator} et le \textit{per cpu pages allocator}.

Nous allons étudier ces mécanismes dans l'action suite aux appels des fonctions permettant d'allouer des pages que nous avons étudiées dans le chapitre précédent.
Pour rappel, ces fonctions sont les suivantes :
\begin{enumerate}
	\item La première est \texttt{alloc\_zeroed\_user\_highpage\_movable}, elle est appelée lors d'un défaut de page causé par une adresse utilisée pour de la mémoire anonyme \citelinux{mm/memory.c}{3172}.
	\item La deuxième est \texttt{\_\_page\_cache\_alloc}, invoquée suite à un défaut de page d'une adresse associée à de la mémoire fichier lors d'une lecture \citelinux{mm/readahead.c}{195}.
	\item La dernière est \texttt{alloc\_page\_vma} qui est appelée après un défaut de page du à une écriture d'une page fichier ou au mécanisme de \textit{Copy on Write} \citelinux{mm/memory.c}{3671}.
\end{enumerate}
Pour chacune de ces fonctions, nous allons détailler leurs piles d'appels jusqu'au point d'entrée principal du \textit{buddy allocator} : \texttt{\_\_alloc\_pages\_nodemask}.
Puis, nous détaillerons les initialisations réalisées par cette fonction.
Nous verrons ensuite le fonctionnement des deux allocateurs dans différents cas tout en faisant l'hypothèse qu'il n'y a pas de pression mémoire.
Nous nous concentrerons ainsi sur les mécanismes d'allocations mémoire et non de réclamation.

\input{appendix/page_allocation/anonymous_allocation.tex}
\input{appendix/page_allocation/file_allocation.tex}
\input{appendix/page_allocation/cow_allocation.tex}
\input{appendix/page_allocation/per_cpu_pages_allocator.tex}
\input{appendix/page_allocation/budy_allocator.tex}

\section{Conclusion}
\label{sec:page_allocation_conclusion}
Dans ce chapitre, nous avons vu comment étaient allouées les pages suites aux différents défauts de page étudiés dans la Section \ref{sec:page_alloc}.
Les défauts de page ne concernant qu'une page, une requête d'allocation de ne portera donc que sur une page.
Ainsi, dans la majorité des cas, ça sera le \textit{per cpu pages allocator} qui sera en charge de la requête.
Il fera appel au \textit{buddy allocator} lorsqu'il ne dispose plus de pages.
Bien entendu, le \textit{buddy allocator} peut être appelé directement, notamment par le noyau lors d'un appel à \texttt{kmalloc}.

Le Tableau \ref{table:page_allocation_sum_up} résume les différentes fonctions appelées suite à un défaut de page ainsi que certains de leurs arguments.

\begin{table}
	\centering

	\caption{Table récapitulant la \texttt{zone}, le \texttt{gfp\_mask}, le \texttt{migratetype} et l'\texttt{order} d'allocation pour chaque fonction}
	\label{table:page_allocation_sum_up}

	% Get Free Pages Flags
	% alloc_zeroed_user_highpage_movable:
	% 	__GFP_MOVABLE | GFP_HIGHUSER | __GFP_ZERO
	% 	https://elixir.bootlin.com/linux/v4.19/source/include/linux/highmem.h#L184
	% 	https://elixir.bootlin.com/linux/v4.19/source/arch/x86/include/asm/page.h#L38
	% 		Où GFP_HIGHUSER = GFP_USER | __GFP_HIGHMEM
	% 		https://elixir.bootlin.com/linux/v4.19/source/include/linux/gfp.h#L298
	% 			Où GFP_USER = __GFP_RECLAIM | __GFP_IO | __GFP_FS | __GFP_HARDWALL
	% 			https://elixir.bootlin.com/linux/v4.19/source/include/linux/gfp.h#L295
	% 				Où __GFP_RECLAIM = ___GFP_DIRECT_RECLAIM|___GFP_KSWAPD_RECLAIM
	% 				https://elixir.bootlin.com/linux/v4.19/source/include/linux/gfp.h#L195
	% 	Donc :
	% 		__GFP_MOVABLE | ___GFP_DIRECT_RECLAIM|___GFP_KSWAPD_RECLAIM | __GFP_IO | __GFP_FS | __GFP_HARDWALL | __GFP_HIGHMEM | __GFP_ZERO
	%
	% __page_cache_alloc :
	% 	GFP_ATOMIC | __GFP_ACCOUNT | __GFP_NORETRY | __GFP_NOWARN
	% 	Héritage inode, etc.
	% 		Où GFP_ATOMIC = __GFP_HIGH|__GFP_ATOMIC|__GFP_KSWAPD_RECLAIM
	% 		https://elixir.bootlin.com/linux/v4.19/source/include/linux/gfp.h#L289
	% 	Donc :
	% 		__GFP_HIGH|__GFP_ATOMIC|__GFP_KSWAPD_RECLAIM | __GFP_ACCOUNT | __GFP_NORETRY | __GFP_NOWARN
	%
	% alloc_page_vma :
	% 	GFP_HIGHUSER_MOVABLE
	% 	https://elixir.bootlin.com/linux/v4.19/source/mm/memory.c#L2507
	% 		Où GFP_HIGHUSER_MOVABLE = GFP_HIGHUSER | __GFP_MOVABLE
	% 		https://elixir.bootlin.com/linux/v4.19/source/include/linux/gfp.h#L299
	% 			Où GFP_HIGHUSER = GFP_USER | __GFP_HIGHMEM
	% 			https://elixir.bootlin.com/linux/v4.19/source/include/linux/gfp.h#L298
	% 				Où GFP_USER = ___GFP_DIRECT_RECLAIM|___GFP_KSWAPD_RECLAIM | __GFP_IO | __GFP_FS | __GFP_HARDWALL
	% 				https://elixir.bootlin.com/linux/v4.19/source/include/linux/gfp.h#L295
	% 				https://elixir.bootlin.com/linux/v4.19/source/include/linux/gfp.h#L195
	% 	Donc :
	% 		___GFP_DIRECT_RECLAIM|___GFP_KSWAPD_RECLAIM | __GFP_IO | __GFP_FS | __GFP_HARDWALL | __GFP_HIGHMEM | __GFP_MOVABLE

	\begin{tabular}{cp{.33\textwidth}|p{.33\textwidth}|p{.33\textwidth}}
		\hline
		Fonctions & \texttt{alloc\_z\_u\_h\_m} & \texttt{\_\_page\_cache\_alloc} & \texttt{alloc\_page\_vma}\\
		\hline
		\texttt{node} & \texttt{numa\_node\_id()} & \texttt{numa\_node\_id()} & \texttt{numa\_node\_id()}\\
		\texttt{nodemask} & \texttt{NULL} & \texttt{NULL} & \texttt{NULL}\\
		\texttt{gfp\_mask} & \texttt{\_\_GFP\_MOVABLE | \_\_GFP\_DIRECT\_RECLAIM | \_\_GFP\_KSWAPD\_RECLAIM | \_\_GFP\_IO | \_\_GFP\_FS | \_\_GFP\_HARDWALL | \_\_GFP\_HIGHMEM | \_\_GFP\_ZERO} & \texttt{\_\_GFP\_HIGH | \_\_GFP\_ATOMIC | \_\_GFP\_KSWAPD\_RECLAIM | \_\_GFP\_ACCOUNT | \_\_GFP\_NORETRY | \_\_GFP\_NOWARN} & \texttt{\_\_GFP\_DIRECT\_RECLAIM | \_\_GFP\_KSWAPD\_RECLAIM | \_\_GFP\_IO | \_\_GFP\_FS | \_\_GFP\_HARDWALL | \_\_GFP\_HIGHMEM | \_\_GFP\_MOVABLE}\\
		\texttt{migratetype} & \texttt{MIGRATE\_MOVABLE} & \texttt{MIGRATE\_UNMOVABLE} & \texttt{MIGRATE\_MOVABLE}\\
		\texttt{order} & 0 & 0 & 0\\
		\hline
	\end{tabular}
\end{table}