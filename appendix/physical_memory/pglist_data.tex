\section{La \texttt{struct pglist\_data}}
\label{sec:pglist_data}
Le noyau Linux peut s'exécuter sur des machines dites "\textit{Non Uniform Memory Access}" (NUMA).
Ces machines ont la particularité de posséder plusieurs réserves de mémoire physique dont les temps d'accès ne sont pas les mêmes \cite{intel_qp_2008, d_purley_2017}.
Ainsi, un ensemble composé d'un processeur et de sa mémoire constitue un nœud NUMA.
Par exemple, pour une machine \textit{bi-socket}, chaque nœud possédera son processeur avec contrôleur mémoire intégré ainsi que des barrettes mémoires associées.
L'accès d'une donnée dans une barrette mémoire locale sera plus rapide puisque géré par le contrôleur mémoire local.
Pour accéder aux données du nœud voisin, il faut utiliser l'\textit{interconnect}, l'accès sera donc plus long.

Linux gère la mémoire des différents nœuds NUMA au moyen de la structure \texttt{pglist\_data}.
Il existe une structure de ce type pour chaque nœud du système.
Si le système en question n'est pas NUMA, il n'y aura qu'une seule \texttt{pglist\_data} \citelinux{include/linux/mmzone.h}{617}.
Le code de cette structure est donnée dans le code \ref{lst:pglist_data}.

\begin{code}
	\centering

	\begin{minted}[autogobble, tabsize = 2, breaklines, linenos, firstnumber = 624, bgcolor = nativebg]{C}
		typedef struct pglist_data {
			struct zone node_zones[MAX_NR_ZONES];
			struct zonelist node_zonelists[MAX_ZONELISTS];
			int nr_zones;
		#ifdef CONFIG_FLAT_NODE_MEM_MAP	/* means !SPARSEMEM */
			struct page *node_mem_map;
		#ifdef CONFIG_PAGE_EXTENSION
			struct page_ext *node_page_ext;
		#endif
		#endif
		#ifndef CONFIG_NO_BOOTMEM
			struct bootmem_data *bdata;
		#endif
		#if defined(CONFIG_MEMORY_HOTPLUG) || defined(CONFIG_DEFERRED_STRUCT_PAGE_INIT)
			/*
			 * Must be held any time you expect node_start_pfn, node_present_pages
			 * or node_spanned_pages stay constant.  Holding this will also
			 * guarantee that any pfn_valid() stays that way.
			 *
			 * pgdat_resize_lock() and pgdat_resize_unlock() are provided to
			 * manipulate node_size_lock without checking for CONFIG_MEMORY_HOTPLUG
			 * or CONFIG_DEFERRED_STRUCT_PAGE_INIT.
			 *
			 * Nests above zone->lock and zone->span_seqlock
			 */
			spinlock_t node_size_lock;
		#endif
			unsigned long node_start_pfn;
			unsigned long node_present_pages; /* total number of physical pages */
			unsigned long node_spanned_pages; /* total size of physical page
									range, including holes */
			int node_id;
			wait_queue_head_t kswapd_wait;
			wait_queue_head_t pfmemalloc_wait;
			struct task_struct *kswapd;	/* Protected by
								mem_hotplug_begin/end() */
			int kswapd_order;
			enum zone_type kswapd_classzone_idx;

			int kswapd_failures;		/* Number of 'reclaimed == 0' runs */

		#ifdef CONFIG_COMPACTION
			int kcompactd_max_order;
			enum zone_type kcompactd_classzone_idx;
			wait_queue_head_t kcompactd_wait;
			struct task_struct *kcompactd;
		#endif
			/*
			* This is a per-node reserve of pages that are not available
			* to userspace allocations.
			*/
			unsigned long		totalreserve_pages;

		#ifdef CONFIG_NUMA
			/*
			 * zone reclaim becomes active if more unmapped pages exist.
			 */
			unsigned long		min_unmapped_pages;
			unsigned long		min_slab_pages;
		#endif /* CONFIG_NUMA */

			/* Write-intensive fields used by page reclaim */
			ZONE_PADDING(_pad1_)
			spinlock_t		lru_lock;

		#ifdef CONFIG_DEFERRED_STRUCT_PAGE_INIT
			/*
			 * If memory initialisation on large machines is deferred then this
			 * is the first PFN that needs to be initialised.
			 */
			unsigned long first_deferred_pfn;
			/* Number of non-deferred pages */
			unsigned long static_init_pgcnt;
		#endif /* CONFIG_DEFERRED_STRUCT_PAGE_INIT */

		#ifdef CONFIG_TRANSPARENT_HUGEPAGE
			spinlock_t split_queue_lock;
			struct list_head split_queue;
			unsigned long split_queue_len;
		#endif

			/* Fields commonly accessed by the page reclaim scanner */
			struct lruvec		lruvec;

			unsigned long		flags;

			ZONE_PADDING(_pad2_)

			/* Per-node vmstats */
			struct per_cpu_nodestat __percpu *per_cpu_nodestats;
			atomic_long_t		vm_stat[NR_VM_NODE_STAT_ITEMS];
		} pg_data_t;
	\end{minted}

	\caption[La \texttt{struct pglist\_data}]{La \texttt{struct pglist\_data} \cite[fichier \texttt{include/linux/mmzone.h}]{noauthor_linux_2018}}
	\label{lst:pglist_data}
\end{code}

Les différentes \texttt{pglist\_data} sont regroupées, pour l'architecture X86\_64, dans le tableau \texttt{node\_data} \citelinux{include/asm/mmzone\_64.h}{13}
Dans le reste de cette section, nous allons commenter certains des champs de la \texttt{struct pglist\_data}.

\subsection{Les zones mémoires du nœud}
\label{subsec:node_zone}
La mémoire d'un nœud est découpé en zones.
Celles-ci sont représentées via la \texttt{struct zone}.
Ainsi, le champ \texttt{node\_zones} est un tableau de \texttt{MAX\_NR\_ZONES} \texttt{struct zones} \citelinux{include/linux/mmzone.h}{625}.
Ces différentes zones sont utilisées pour satisfaire différentes demandes d'allocation de pages.
Le noyau, compilé pour l'architecture x86\_64, compte 5 zones \citelinux{include/linux/mmzone.h}{302} :
\begin{description}
	\item[ZONE\_DMA :] Cette zone est utilisée pour les périphériques \textit{Direct Memory Access} (DMA) \citelinux{include/linux/mmzone.h}{305}.
	Ces périphériques peuvent accéder directement à la mémoire sans demander au processeur de lire ou d'écrire une donnée pour eux.
	Plus spécifiquement, cette zone est réservée aux périphériques DMA ne pouvant pas adresser plus de \SI{16}{\mega\byte}.
	Cette limitation vient du bus ISA utilisé par ces périphériques, d'un temps éloigné, qui
	ne peuvent pas adresser toute la mémoire \cite{corbet_is_2018}.
	\item[ZONE\_DMA32 :] Elle est aussi utilisée pour satisfaire les allocations mémoire de périphériques DMA.
	Contrairement à la première zone, celle-ci s'étend jusqu'à \SI{4}{\giga\byte} et correspond donc aux périphériques 32 bits \citelinux{include/linux/mmzone.h}{326}.
	\item[ZONE\_MOVABLE :] Linux offre la possibilité d'ajouter ou de retirer, à chaud, de la mémoire.
	La \texttt{ZONE\_MOVABLE} est utilisée à cette fin \cite{noauthor_memory_2007}.
	\item[ZONE\_DEVICE :] Cette zone semble être utilisée pour la \textit{persistent memory} \cite{williams_mm_2015}.
	\item[ZONE\_NORMAL :] Les autres allocations de page seront gérées par cette zone.
\end{description}
Le champ \texttt{nr\_zones} contient le nombre de zones actuellement reliées à ce nœud \citelinux{include/linux/mmzone.h}{627}.
Nous détaillerons le code de la \texttt{struct zone} dans la Section \ref{TODO}.

Le champ \texttt{node\_zonelists}, de type \texttt{struct zonelist}, semble regrouper les différentes zones du noeud \citelinux{include/linux/mmzone.h}{626}.
Une \texttt{zonelist} est utilisée lors d'une allocation de pages, en effet, la première zone de cette liste est la zone cible de l'allocation.
Les autres seront utilisées uniquement si l'allocation n'a pas pu se faire dans la zone cible.
Le commentaire suivant donne du crédit à cette assertion \citelinux{include/linux/mmzone.h}{593} :
\begin{quote}
	One allocation request operates on a zonelist.
	A zonelist is a list of zones, the first one is the 'goal' of the allocation, the other zones are fallback zones, in decreasing priority.
\end{quote}
Cette structure contient un unique champ \texttt{\_zonerefs} qui est un tableau de \texttt{struct zoneref} \citelinux{include/linux/mmzone.h}{607}.
Une \texttt{zoneref} contient un pointeur vers une zone \citelinux{include/linux/mmzone.h}{587}.
Cette structure contient aussi l'indice de la \texttt{zone} en terme de type de zone, par exemple, si la zone a pour type \texttt{ZONE\_DMA}, son indice vaudra 0.
Comme l'indique ce commentaire, Les \texttt{zonelist} et \texttt{zoneref} semblent être utilisées pour accélérer l'allocation sans avoir à déréférencer la \texttt{struct zone} \citelinux{include/linux/mmzone.h}{584} :
\begin{quote}
	It is stored here to avoid dereferences into large structures and lookups of tables
\end{quote}
% Nous verrons, dans la pratique, en quoi ce commentaire se révèle vrai.

\subsection{Le \textit{page cache}}
\label{subsec:pglist_data_page_cache}
Le \textit{page cache} est un mécanisme de cache offert par Linux.
Plutôt que d'accéder au disque à chaque fois qu'une donnée d'un fichier est manipulée, Linux ramène une partie du fichier dans la mémoire.
Cette partie du fichier peut ensuite être accédée via la page dans laquelle les données ont été stockées.

Pour le \textit{page cache}, Linux utilise la mémoire non utilisée\footnote{La commande \texttt{free} indique la taille du \textit{page cache} dans la colonne \textit{buff/cache}.}.
Ainsi, à un instant donné, la taille maximale du \textit{page cache} est définie par la formule suivante :
\[cache = total - used\]

Le champ \texttt{lruvec} correspond à l'implémentation du \textit{page cache} \citelinux{include/linux/mmzone.h}{706}.
Il y a donc un \textit{page cache} par nœud NUMA.
Celui-ci repose sur plusieurs listes \citelinux{include/linux/mmzone.h}{237}\cite{https://linux-mm.org/PageReplacementDesign} :
\begin{description}
	\item[LRU\_INACTIVE\_ANON :] Cette liste contient les pages anonymes qui n'ont pas été accédées pendant une durée déterminée.
	Si une page de cette liste est à nouveau accédée elle sera transférée dans la \texttt{LRU\_ACTIVE\_ANON}.
	\item[LRU\_ACTIVE\_ANON :] Quand une page anonyme est accédée pour la première fois elle rejoint cette liste.
	\item[LRU\_INACTIVE\_FILE :] Cette liste contient les pages fichiers qui n'ont été accédées qu'une fois.
	\item[LRU\_ACTIVE\_FILE :] Quand une page fichier est réaccédée, elle est migrée de la liste précédente à celle-ci.
	\item[LRU\_UNEVICTABLE :] Les pages qui ne peuvent être évincées sont stockées dans cette liste.
	Plus de détails sont donnés sur les pages verrouillées dans le Tableau \ref{table:vm_flags_mmap}.
\end{description}
La description donnée ici est volontairement incomplète, en effet, nous étudierons en détails le \textit{page cache} dans le Chapitre \ref{TODO}.

\subsection{\texttt{kswapd}}
\label{subsec:pglist_data_kswapd}
En plus de la structure représentant le \textit{page cache}, une \texttt{pglist\_data} contient la \texttt{task\_struct} associée à \texttt{kswapd} \citelinux{include/linux/mmzone.h}{658}.
Une \texttt{task\_struct} est une structure de donnée utilisée pour représenter une tâche.
Quant à \texttt{kswapd}, il est le démon responsable de la réclamation mémoire.
Tout comme il y a un \textit{page cache} par nœud NUMA, il y a aussi un démon récupérateur de mémoire par nœud.

En plus de la \texttt{task\_struct} associée à \texttt{kswapd}, une \texttt{pglist\_data} contient l'ordre de \texttt{kswapd} dans son champ \texttt{kswapd\_order} \citelinux{include/linux/mmzone.h}{660}.
Cette structure contient aussi une \texttt{wait\_queue\_head\_t kswapd\_wait} qui est utilisée par le démon quand il s'endort pour pouvoir le réveiller \citelinux{include/linux/mmzone.h}{656}.
Enfin, le champ \texttt{kswapd\_failures} contient le nombre de fois où \texttt{kswapd} a échoué à réclamer de la mémoire \citelinux{include/linux/mmzone.h}{663}.

\subsection{Identifiant du nœud}
\label{subsec:pglist_data_node_id}
L'identifiant d'un nœud est stocké dans le champ \texttt{node\_id} \citelinux{include/linux/mmzone.h}{655}.

\subsection{Caractéristiques du nœud}
\label{subsec:pglist_data_characteristics}
Certaines caractéristiques, propres au nœud, sont stockées dans la \texttt{pglist\_data}.
Ainsi, la mémoire physique totale, exprimée en page, dont dispose ce nœud est accessible via le champ \texttt{node\_present\_pages} \citelinux{include/linux/mmzone.h}{652}.
\texttt{node\_start\_pfn} contient le numéro du premier numéro de page physique appartenant à ce nœud \citelinux{mm/memory\_hotplug.c}{459}.