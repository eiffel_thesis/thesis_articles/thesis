\section{La \texttt{struct page}}
\label{sec:page}
La brique de base de la mémoire virtuelle est la page.
Le code \ref{lst:page} contient la définition de la \texttt{struct page} de Linux \cite[\texttt{include/linux/mm\_types.h}]{noauthor_linux_2018}.

Une \texttt{page} contient donc des drapeaux ainsi que des structures qui dépendront de différentes caractéristiques propres à la page représentée.

Dans les sous-sections suivantes, nous nous intéresserons aux différents types de page qui peuvent être trouvés dans le premier \texttt{union}.

\begin{code}
	\centering

	\begin{minted}[autogobble, tabsize = 2, breaklines, linenos, firstnumber = 70, bgcolor = nativebg]{C}
		struct page {
			unsigned long flags;		/* Atomic flags, some possibly
							* updated asynchronously */
			/*
			 * Five words (20/40 bytes) are available in this union.
			 * WARNING: bit 0 of the first word is used for PageTail(). That
			 * means the other users of this union MUST NOT use the bit to
			 * avoid collision and false-positive PageTail().
			 */
			union {
				struct {	/* Page cache and anonymous pages */
					/**
					 * @lru: Pageout list, eg. active_list protected by
					 * zone_lru_lock.  Sometimes used as a generic list
					 * by the page owner.
					 */
					struct list_head lru;
					/* See page-flags.h for PAGE_MAPPING_FLAGS */
					struct address_space *mapping;
					pgoff_t index;		/* Our offset within mapping. */
					/**
					 * @private: Mapping-private opaque data.
					 * Usually used for buffer_heads if PagePrivate.
					 * Used for swp_entry_t if PageSwapCache.
					 * Indicates order in the buddy system if PageBuddy.
					 */
					unsigned long private;
				};
				struct {	/* slab, slob and slub */
					union {
						struct list_head slab_list;	/* uses lru */
						struct {	/* Partial pages */
							struct page *next;
		#ifdef CONFIG_64BIT
							int pages;	/* Nr of pages left */
							int pobjects;	/* Approximate count */
		#else
							short int pages;
							short int pobjects;
		#endif
						};
					};
					struct kmem_cache *slab_cache; /* not slob */
					/* Double-word boundary */
					void *freelist;		/* first free object */
					union {
						void *s_mem;	/* slab: first object */
						unsigned long counters;		/* SLUB */
						struct {			/* SLUB */
							unsigned inuse:16;
							unsigned objects:15;
							unsigned frozen:1;
						};
					};
				};
				struct {	/* Tail pages of compound page */
					unsigned long compound_head;	/* Bit zero is set */

					/* First tail page only */
					unsigned char compound_dtor;
					unsigned char compound_order;
					atomic_t compound_mapcount;
				};
				struct {	/* Second tail page of compound page */
					unsigned long _compound_pad_1;	/* compound_head */
					unsigned long _compound_pad_2;
					struct list_head deferred_list;
				};
				struct {	/* Page table pages */
					unsigned long _pt_pad_1;	/* compound_head */
					pgtable_t pmd_huge_pte; /* protected by page->ptl */
					unsigned long _pt_pad_2;	/* mapping */
					union {
						struct mm_struct *pt_mm; /* x86 pgds only */
						atomic_t pt_frag_refcount; /* powerpc */
					};
		#if ALLOC_SPLIT_PTLOCKS
					spinlock_t *ptl;
		#else
					spinlock_t ptl;
		#endif
				};
				struct {	/* ZONE_DEVICE pages */
					/** @pgmap: Points to the hosting device page map. */
					struct dev_pagemap *pgmap;
					unsigned long hmm_data;
					unsigned long _zd_pad_1;	/* uses mapping */
				};

				/** @rcu_head: You can use this to free a page by RCU. */
				struct rcu_head rcu_head;
			};

			union {		/* This union is 4 bytes in size. */
				/*
				* If the page can be mapped to userspace, encodes the number
				* of times this page is referenced by a page table.
				*/
				atomic_t _mapcount;

				/*
				 * If the page is neither PageSlab nor mappable to userspace,
				 * the value stored here may help determine what this page
				 * is used for.  See page-flags.h for a list of page types
				 * which are currently stored here.
				 */
				unsigned int page_type;

				unsigned int active;		/* SLAB */
				int units;			/* SLOB */
			};

			/* Usage count. *DO NOT USE DIRECTLY*. See page_ref.h */
			atomic_t _refcount;

		#ifdef CONFIG_MEMCG
			struct mem_cgroup *mem_cgroup;
		#endif

			/*
			 * On machines where all RAM is mapped into kernel address space,
			 * we can simply calculate the virtual address. On machines with
			 * highmem some memory is mapped into kernel virtual memory
			 * dynamically, so we need a place to store that address.
			 * Note that this field could be 16 bits on x86 ... ;)
			 *
			 * Architectures with slow multiplication can define
			 * WANT_PAGE_VIRTUAL in asm/page.h
			 */
		#if defined(WANT_PAGE_VIRTUAL)
			void *virtual;			/* Kernel virtual address (NULL if
								not kmapped, ie. highmem) */
		#endif /* WANT_PAGE_VIRTUAL */

		#ifdef LAST_CPUPID_NOT_IN_PAGE_FLAGS
			int _last_cpupid;
		#endif
		} _struct_page_alignment;
	\end{minted}

	\caption[La \texttt{struct page}]{La \texttt{struct page} \cite[fichier \texttt{include/linux/mm\_types.h}]{noauthor_linux_2018}}
	\label{lst:page}
\end{code}

\subsection{Page anonyme et page fichier}
Ces pages sont celles qui seront utilisées par l'espace utilisateur.
Elles sont de deux types.

Les premières, dites anonymes (\textit{anonymous pages}), correspondent aux pages utilisées pour le tas ou la pile d'un processus.
Elles sont considérées comme "anonymes" car elles ne peuvent pas être lues depuis le disque.

À la différences des premières, les pages fichiers peuvent être récupérées depuis le disque.
En effet, ces pages sont utilisées pour mettre en cache des blocs du disque afin d'accélérer la lecture des données.

Les pages sont liées entres elles via le champ \texttt{lru}.
Elles le sont notamment dans le cadre du \textit{page cache}.
Nous détaillerons ce mécanisme dans le Chapitre \ref{TODO}.

\subsection{Slab, slob et slub}
Un \textit{slab} est un cache d'objets pouvant être réutilisés \cite{usenix_association_proceedings_1994}.

En effet, après l'allocation d'un objet, celui-ci est souvent initialisé.
Un \textit{slab} est donc une collection d'objets déjà initialisés, mais plus utilisés et prêts à être réutilisés.

Ainsi, l'étape d'initialisation ne sera pas ré-exécutée pour cet objet puisqu'elle aura été exécutée lors de sa première allocation.
Le noyau Linux offre trois types de \textit{slab} \cite{love_what_2013, lameter_slab_2014} :
\begin{itemize}
	\item \texttt{SLAB} : Le \texttt{SLAB} de Linux est une implémentation du \textit{slab} décrit par Jeff Bonwick \cite{usenix_association_proceedings_1994, love_what_2013}.
	\item \texttt{SLUB} : Le \texttt{SLUB} est une adaptation du \texttt{SLAB} pour les machines équipées de beaucoup de cœurs \cite{love_what_2013}.
	\item \texttt{SLOB} : Le \texttt{SLOB} est une version optimisée du \texttt{SLAB} pour des systèmes embarqués disposant de peu de mémoire \cite{love_what_2013}.
\end{itemize}

\subsection{Page composée}
Une page composée (\textit{compound page}) est une page plus large que la normale\footnote{Une page composée aura donc une taille supérieure à 4096 octets. L'appel \texttt{sysconf(PAGESIZE)} permet d'obtenir la taille d'une page. Celle-ci est de 4096 octets sur une architecture x86\_64.}.
Elle est, pour cela, composée de plusieurs pages physiques contiguës en mémoire \cite{corbet_introduction_2014}.
Ces pages sont notamment utilisées pour créer des \textit{huge pages}.

La première page classique d'une page composée est la tête de celle-ci, tandis que les autres pages la composant sont des "pages queues".
La page tête peut être accédée depuis n'importe laquelle des pages d'une page composée via le champ \texttt{compound\_head}\footnote{Le lecteur assidu aura remarqué que la deuxième "page queue" d'une page composée a une structure un peu particulière comme décrit ligne 139 du code \ref{lst:page}. Néanmoins, grâce au fait que les différentes structures sont anonymes et au champ \texttt{\_compound\_pad\_1} il est possible d'accéder au champ \texttt{compound\_head}.}.

La première page queue contient d'autres informations sur la page composée.
Ainsi, le champ \texttt{compound\_dtor} sera utilisé comme un pointeur de fonction permettant de désaouller la page composée.
Le champ \texttt{compound\_order} indique l'ordre d'une page composée, une page composée d'ordre $n$ sera composée de $2^n$ pages.

\subsection{Les pages utilisées pour la table des pages}
La traduction d'une adresse linéaire en adresse physique est un procédé complexe s'appuyant à la fois sur un matériel, l'unité de gestion mémoire (\textit{Memory Management Unit}, MMU), et une structure de données noyau, une table des pages.

Dans un système d'exploitation, une table des pages est une structure de données permettant d'associer une adresse linéaire\footnote{Intel, dans ses documents techniques, utilise le terme d'adresse linéaire (\textit{linear address}) pour parler d'adresse virtuelle \cite[Section 3.3.1
]{intel_basic_2019}. Cette thèse s'efforcera de coller au dialecte d'Intel.} à une adresse physique.
Cette table est elle-même composée de pages.
Chaque processus dispose d'une table des pages afin que la MMU traduise ses adresses linéaires en adresses physiques.

À chaque accès mémoire\footnote{Le jeu d'instruction des processeurs x86 possède la particularité d'être un jeu d'instruction CISC (\textit{Complex Instruction Set Computer}).
Ainsi, la plupart des instructions peuvent prendre comme opérande un registre ou une adresse mémoire via l'octet ModR/M du code d'instruction \cite[Section 2.1.5]{intel_instr_set_2019}.}, la MMU va parcourir la table des pages pour traduire l'adresse linéaire en adresse physique.

Une fois la traduction faite, l'association entre adresse linéaire et adresse physique est stockée dans un cache processeur appelé le \textit{Translation Lookaside Buffer} (TLB)\footnote{Les processeurs Intel peuvent posséder, en plus du TLB, des caches contenant des informations sur les niveaux intermédiaires de la table des pages \cite[Section 4.10.3.1]{intel_sys_prog_2019}}.
Ce cache permet donc d'accélérer une traduction déjà réalisée en évitant d'accéder à la table des pages dans la mémoire physique.

\subsubsection{La table des pages}
L'organisation logicielle de la table des pages est imposée par le matériel puisque la MMU doit être capable de parcourir celle-ci.
Les architectures modernes possèdent des tables des pages multi-niveaux \cite{intel_5_level_2017}.

Pour un processeur x86\_64, l'adresse du plus haut niveau de la table des pages du processus courant est stockée dans le registre \texttt{cr3}.
La Figure \ref{fig:bit_cr3} montre le découpage des bits contenus dans ce registre.
Les bits 12 à 51 contiennent l'adresse du PML5, c'est-à-dire le plus haut niveau de la table des pages pour une table des pages à 5 niveaux pour une architecture x86\_64 \cite[Section 2.4]{intel_5_level_2017}.
Les bits 0 à 11 sont utilisés pour le \textit{Process-context identifier} (PCID).
Un PCID permet d'identifier l'espace d'adressage virtuel d'un processus\footnote{Il peut y avoir au maximum 4096 PCID différents ($2^{12} = 4096$), or Linux peut gérer beaucoup plus que 4096 processus (la commande \texttt{cat /proc/sys/kernel/pid\_max} renvoie 32768). Linux dispose donc d'un mécanisme de gestion des PCID \cite[fichier \texttt{/arch/x86/mm/tlb.c}, ligne 67]{noauthor_linux_2018}.}.
En plus de l'association (adresse linéaire, adresse physique), une entrée du TLB stocke aussi le PCID de l'espace d'adressage virtuel pour lequel la traduction a été effectuée, ainsi que les droits d'accès de la page concernée \cite[Section 4.10.2.2]{intel_sys_prog_2019}.

\begin{figure}
	\centering

	\begin{bytefield}[endianness = big]{32}
		\bitheader[lsb = 32]{32, 51, 52, 63} \\
		\bitbox{12}{\color{black}\rule{\width}{\height}} & \bitbox{20}{PML5 Address (continued)} \\
		\bitbox{20}{PML5 Address} & \bitbox{12}{PCID} \\
		\bitheader{0, 11, 12, 31} \\
	\end{bytefield}

	\caption[Découpage des bits du \texttt{cr3}]{Découpage des bits du \texttt{cr3} \cite[Table 4-19]{intel_sys_prog_2019}}
	\label{fig:bit_cr3}
\end{figure}

La Figure \ref{fig:page_table} montre la table des pages de Linux pour une architecture x86\_64 à 5 niveaux \cite{intel_5_level_2017}.
Il faut donc effectuer 5 "bonds" pour traduire une adresse linéaire en adresse physique.

Un niveau intermédiaire de la table des pages est une page dont les entrées mènent à d'autres niveaux intermédiaires et non à des pages terminales.
Une entrée d'un niveau intermédiaire est une entrée de la table de pages (\textit{page table entry}), de type \texttt{pte\_t} dans Linux \citelinux{arch/x86/include/asm/pgtable\_64\_types.h}{14}.
De manière générale, le nombre d'entrées d'un niveau intermédiaire est donné par la formule suivante :
\[entries = \frac{PAGE\_SIZE}{sizeof(pte\_t)}\]
Dans le cas d'une architecture x86\_64, il y a donc 512 entrées par niveaux intermédiaires\footnote{Le type \texttt{pte\_t} est défini comme une structure comportant un champ de type \texttt{pteval\_t} \citelinux{arch/x86/include/asm/pgtable\_64\_types.h}{21}.
Le type \texttt{pteval\_t} est à son tour défini comme un \texttt{unsigned long} \citelinux{arch/x86/include/asm/pgtable\_64\_types.h}{14}.
La taille d'un \texttt{long} équivaut à la taille de l'architecture en octet, ainsi \texttt{sizeof(unsigned long)} renvoie 8 pour une architecture 64 bits.
Puisque les pages font 4096 octets, on a donc $\frac{4096}{8} = 512$.}.

La Figure \ref{fig:page_table} montre aussi comment la MMU utilise l'adresse linéaire pour parcourir la table des pages et trouver l'adresse physique correspondante.
L'adresse linéaire sera donc découpée en autant de parties qu'il y a de niveaux dans la table des pages (dans notre cas 5).
Les bits contenus dans chaque partie, par exemple PML4 sur la Figure \ref{fig:page_table}, seront utilisés pour accéder à l'entrée du niveau correspondant, 4 dans notre exemple.
Ainsi, si les bits de la partie $n$ ont pour valeur 16, l'entrée 16 du niveau $n$ sera accédée.
Les bits de poids faible de l'adresse linéaire ne sont pas utilisés pour la traduction et correspondent au déplacement à effectuer dans la page physique pour obtenir la donnée demandée.
Les bits de poids faible d'une adresse linéaire et d'une adresse physique sont donc communs.

La Figure \ref{fig:page_table_huge_page} présente une traduction d'adresse menant à une \textit{huge page}.
Les \textit{huge} pages sont des pages dont la taille est supérieure à \SI{4}{\kilo\byte}.
Les processeurs x86\_64 offrent la possibilité d'allouer des \textit{huge pages} de \SI{1}{\giga\byte} et \SI{2}{\mega\byte} \cite[Table 4-15]{intel_sys_prog_2019}\cite[Table 4-17]{intel_sys_prog_2019}.
Les \textit{huge pages} présentent l'intérêt de réduire le nombre d'étapes à effectuer pour traduire une adresse linéaire en adresse physique.
En effet, comme le montre la Figure \ref{fig:page_table_huge_page}, une \textit{huge page} supprime un ou plusieurs niveaux intermédiaires.

\begin{figure}
	\centering

	\includegraphics[scale = .42]{5_level_page_table.pdf}

	\caption[Table des pages à 5 niveaux]{Table des pages à 5 niveaux \cite[Figure 2-1]{intel_5_level_2017}}
	\label{fig:page_table}
\end{figure}

\begin{figure}
	\centering

	\includegraphics[scale = .42]{5_level_page_table_huge_page.pdf}

	\caption[Table des pages à 5 niveaux menant à une \textit{huge page} de \SI{1}{\giga\byte} ]{Table des pages à 5 niveaux menant à une \textit{huge page} de \SI{1}{\giga\byte} \cite[Figure 2-1]{intel_5_level_2017}\cite[Figure 4-10]{intel_sys_prog_2019}}
	\label{fig:page_table_huge_page}
\end{figure}

\subsubsection{Anatomie d'une entrée terminale de la table de pages}
\label{subsubsec:pte}
La Figure \ref{fig:bit_pte} montre le découpage des bits d'une \texttt{pte\_t} pour l'architecture x86\_64 \cite[Table 4-19]{intel_sys_prog_2019}\cite[Chapter 3]{gorman_understanding_2004}.
Certains bits sont utilisés pour des drapeaux, le code définissant les drapeaux est donné dans le code \ref{lst:flags}.

Les différents drapeaux d'une entrée ciblant une page de \SI{4}{\kilo\byte} sont détaillés dans le Tableau \ref{table:pte_flags}\footnote{L'architecture Intel est une architecture complexe et elle ne manque pas de le prouver. Ainsi, selon à quel niveau se trouve l'entrée de la table des pages considérée, ces drapeaux peuvent avoir différentes significations \cite[Figure 4-11]{intel_sys_prog_2019}}.
Les bits 9 à 11 inclus et 52 à 58 inclus sont ignorés par Intel.
Les développeurs du noyau Linux ont donc utilisé certains de ces bits comme bon leur semblent.

\begin{longtable}{p{.2\textwidth}p{.2\textwidth}p{.05\textwidth}p{.65\textwidth}}
	\caption{Les différents drapeaux et leurs significations pour une entrée ciblant une page de \SI{4}{\kilo\byte}\label{table:pte_flags}}\\

	\hline
	Nom & Abréviation & Bit & Description\\
	\hline
	\textit{Present} & \texttt{P} & 0 & Ce drapeau indique que l'entrée est présente.
	Ceci signifie donc que l'adresse contenue dans les bits 57 à 12 pointe sur un niveau inférieur de la table des pages ou une page terminale.\\
	\textit{Read/Write} & \texttt{R/W} & 1 & Si ce drapeau vaut 1 alors la page pointée est accessible en lecture et en écriture, sinon elle n'est accessible qu'en lecture.\\
	\textit{User/Supervisor} & \texttt{U/S} & 2 & Si ce drapeau vaut 1 alors la page pointée est accessible en mode utilisateur et superviseur\footnote{Dans la dénomination Intel, le mode superviseur correspond à ce qui est plus communément appelé le mode système ou le mode kernel \cite[Section 4.6.1]{intel_sys_prog_2019}.}, sinon elle n'est accessible qu'en mode superviseur.\\
	\textit{Page Write Through} & \texttt{PWT} & 3 & Si ce drapeau est à 1 alors les écritures sur cette page traverseront le cache processeur, sinon les écritures seront propagées à la page lorsque la ligne écrite sera évincée du cache\footnote{\textit{Write Through} est l'un des nombreux types de mémoire que compte la \textit{Page Attribute Table} (PAT) \cite[Table 11-10]{intel_sys_prog_2019}\cite[fichier \texttt{Documentation/x86/pat.txt}]{noauthor_linux_2018}.}.\\
	\textit{Page Cache Disable} & \texttt {PCD} & 4 & Si ce drapeau est à 1 alors aucune donnée de la page ne pourra résider dans le cache processeur, sinon les données pourront être mises en cache processeur\footnote{Le drapeau \textit{Cache Disable} correspond au type mémoire \textit{Uncacheable} de la PAT.}.\\
	\textit{Accessed} & \texttt{A} & 5 & Ce drapeau est mis à 1 par le processeur quand il manipule une adresse linéaire appartenant à cette page.\\
	\textit{Dirty} & \texttt{D} & 6 & Ce drapeau est similaire au drapeau \textit{Accessed}, à part qu'il est mis à 1 quand le processeur \textbf{écrit} une adresse linéaire appartenant à cette page.\\
	\textit{Page Attribute Table } & \texttt{PSE} ou \texttt{PAT} & 7 & Si l'entrée de la table des pages est une entrée terminale (c'est-à-dire qu'elle pointe sur une page de \SI{4}{\kilo\byte}) alors ce drapeau servira à choisir le type mémoire dans la PAT.
	Sinon, si ce drapeau est à 1, il indiquera que la page ciblée est une \textit{huge} page.
	Pour le moment et pour les processeurs x86\_64, il existe des \textit{huge} pages de \SI{1}{\giga\byte} et \SI{2}{\mega\byte} \cite[Section 4.1.4]{intel_sys_prog_2019}.\\
	\textit{Global} & \texttt{G} & 8 & Si ce drapeau est à 1 alors la page est dite globale, ceci signifie qu'une entrée du TLB correspondant à cette page ne sera pas invalidée lors du changement de tâche active \cite[Section 4.10.2.4]{intel_sys_prog_2019}\cite[Section 4.10.4.1]{intel_sys_prog_2019}.\\
	\textit{Special} & & 9 & D'après le commentaire suivant, une page ayant le drapeau \texttt{\_PAGE\_BIT\_SPECIAL} à 1 n'est pas associée avec une \texttt{struct page} \citelinux{mm/memory.c}{780} :
	\begin{quote}
		"Special" mappings do not wish to be associated with a "struct page"
	\end{quote}\\
	\textit{CPA Test} & & 10 & Le drapeau \texttt{\_PAGE\_BIT\_CPA\_TEST} ne semble être utilisé qu'à des fins de test \cite[fichier \texttt{mm/pageattr-test.c}]{noauthor_linux_2018}.\\
	\textit{Soft Dirty} & & 11 & Le drapeau \texttt{\_PAGE\_BIT\_SOFT\_DIRTY} sert à traquer les pages accédées par une tâche donnée \cite{emelyanov_soft_dirty_2013}.\\
	\textit{Device mapped} & & 58 & Si le drapeau \texttt{\_PAGE\_BIT\_DEVMAP} est mis à 1 alors la page référence un périphérique et est utilisée pour de l'accès direct \cite{williams_patch_2015}\cite[Sections 10.4.2 et 10.4.3]{snia_nvm_2017}.\\
	\textit{Protection Keys} & \texttt{PKEYS} & 59 à 62 & Les \textit{protection keys} sont un mécanisme de protection additionnelle aux drapeaux \textit{read/write} et \textit{no execution}.
	Elles permettent de diviser l'espace d'adressage d'un processus en jusqu'à 16 zones ayant des propriétés différentes.
	Par exemple, la première zone ne pourra être accédée qu'en lecture tandis que la deuxième ne pourra pas être accédée du tout.
	Contrairement aux drapeaux \textit{read/write} et \textit{no execution}, les modifications des drapeaux de protection ne nécessitent pas d'invalider l'entrée du TLB correspondante à la page modifiée \cite{corbet_memory_2015, noauthor_pkeys_2019}.
	Ces \textit{protection keys} ne s'appliquent qu'à des pages utilisateurs \cite[Section 4.6.2]{intel_sys_prog_2019}.\\
	\textit{No Execution} & \texttt{NX} & 63 & Si ce drapeau est à 1 alors les données contenues dans la page ne peuvent pas être du code exécutable. Cette fonctionnalité a été introduite par l'architecture K8 d'AMD \cite{amd_amd_2006}. \\
	\hline
\end{longtable}

\begin{figure}[b]
	\centering

	\begin{bytefield}[endianness = big]{32}
		\bitheader[lsb = 32]{32, 51, 52, 58, 59, 62, 63} \\
		\bitbox{1}{\tiny NX} & \bitbox{4}{\tiny PKEYS} & \bitbox{7}{\color{lightgray}\rule{\width}{\height}} & \bitbox{20}{\tiny Physical Address [51 : 32]} \\
		\bitbox{20}{\tiny Page Address [31 : 12]} & \bitbox{3}{\color{lightgray}\rule{\width}{\height}} & \bitbox{1}{\tiny G} & \bitbox{1}{\rotatebox{90}{\tiny PAT}} & \bitbox{1}{\tiny D} & \bitbox{1}{\tiny A} & \bitbox{1}{\rotatebox{90}{\tiny PCD}} & \bitbox{1}{\rotatebox{90}{\tiny PWT}} & \bitbox{1}{\rotatebox{90}{\tiny U/S}} & \bitbox{1}{\rotatebox{90}{\tiny R/W}} & \bitbox{1}{\tiny P} \\
		\bitheader{0 - 9, 11, 12, 31} \\
	\end{bytefield}

	\caption[Découpage des bits d'une \texttt{pte\_t} pointant sur une page de \SI{4}{\kilo\byte}]{Découpage des bits d'une \texttt{pte\_t} pointant sur une page de \SI{4}{\kilo\byte} \cite[Table 4-19]{intel_sys_prog_2019}}
	\label{fig:bit_pte}
\end{figure}

\begin{code}
	\centering

	\begin{minted}[autogobble, tabsize = 2, breaklines, linenos, firstnumber = 12, bgcolor = nativebg]{C}
		#define _PAGE_BIT_PRESENT	0	/* is present */
		#define _PAGE_BIT_RW		1	/* writeable */
		#define _PAGE_BIT_USER		2	/* userspace addressable */
		#define _PAGE_BIT_PWT		3	/* page write through */
		#define _PAGE_BIT_PCD		4	/* page cache disabled */
		#define _PAGE_BIT_ACCESSED	5	/* was accessed (raised by CPU) */
		#define _PAGE_BIT_DIRTY		6	/* was written to (raised by CPU) */
		#define _PAGE_BIT_PSE		7	/* 4 MB (or 2MB) page */
		#define _PAGE_BIT_PAT		7	/* on 4KB pages */
		#define _PAGE_BIT_GLOBAL	8	/* Global TLB entry PPro+ */
		#define _PAGE_BIT_SOFTW1	9	/* available for programmer */
		#define _PAGE_BIT_SOFTW2	10	/* " */
		#define _PAGE_BIT_SOFTW3	11	/* " */
		#define _PAGE_BIT_PAT_LARGE	12	/* On 2MB or 1GB pages */
		#define _PAGE_BIT_SOFTW4	58	/* available for programmer */
		#define _PAGE_BIT_PKEY_BIT0	59	/* Protection Keys, bit 1/4 */
		#define _PAGE_BIT_PKEY_BIT1	60	/* Protection Keys, bit 2/4 */
		#define _PAGE_BIT_PKEY_BIT2	61	/* Protection Keys, bit 3/4 */
		#define _PAGE_BIT_PKEY_BIT3	62	/* Protection Keys, bit 4/4 */
		#define _PAGE_BIT_NX		63	/* No execute: only valid after cpuid check */

		#define _PAGE_BIT_SPECIAL	_PAGE_BIT_SOFTW1
		#define _PAGE_BIT_CPA_TEST	_PAGE_BIT_SOFTW1
		#define _PAGE_BIT_SOFT_DIRTY	_PAGE_BIT_SOFTW3 /* software dirty tracking */
		#define _PAGE_BIT_DEVMAP	_PAGE_BIT_SOFTW4
	\end{minted}

	\caption[Définition des drapeaux d'une entrée de page des tables pour l'architecture x86\_64]{Définition des drapeaux d'une entrée de page des tables pour l'architecture x86\_64 \cite[fichier \texttt{arch/x86/include/asm/pgtable\_types.h}]{noauthor_linux_2018}}
	\label{lst:flags}
\end{code}

\subsubsection{Mise à jour de la table des pages courante lors du changement de processus actif}
Lors d'un changement de processus actif, le contenu du registre \texttt{cr3} sera modifié pour contenir l'adresse du plus haut niveau de la table des pages du nouveau processus courant.

La Figure \ref{fig:write_cr3_call_stack}, montre la pile d'appel d'un changement de processus menant à la modification de la valeur contenue dans le registre \texttt{cr3}.
Un changement de processus est opéré par la fonction noyau \texttt{\_\_schedule()} \citelinux{kernel/sched/core.c}{3383}.
Cette fonction fait partie de l'ordonnanceur du noyau Linux.
Dans un système d'exploitation, l'ordonnanceur est le mécanisme en charge du choix des processus s'exécutant \cite[Chapter 10]{bovet_understanding_2006}.

La valeur du \texttt{cr3} est modifiée dans la fonction noyau \texttt{native\_write\_cr3()}, cette fonction modifie effectivement la valeur contenue dans ce registre en embarquant du code assembleur modifiant ce registre \cite[fichier \texttt{arch/x86/include/asm/special\_insns.h, ligne 50}]{noauthor_linux_2018}.
Le lecteur averti aura remarqué que cette pile d'appel présente une alternative après l'appel à la fonction noyau \texttt{load\_new\_mm\_cr3()}.
En effet, selon si l'invalidation les entrées du TLB est nécessaire ou non, la fonction noyau  \texttt{build\_cr3()} ou \texttt{build\_cr3\_noflush()} sera appelée.
Ces deux fonctions différent par la valeur qu'elles renverront et qui sera écrite dans le \texttt{cr3}.
La valeur renvoyée par la fonction \texttt{build\_cr3()} contient l'adresse du plus haut niveau de la table des pages du nouveau processus courant ainsi que son PCID.
Celle renvoyée par la fonction \texttt{build\_cr3\_noflush()} aura en plus son bit de poids fort à 1.
La documentation d'Intel liste plusieurs manières d'invalider les entrées du TLB dont l'écriture dans le \texttt{cr3} d'une valeur dont le bit de poids fort vaut 0 \cite[4.10.4.1]{intel_sys_prog_2019}.

\begin{figure}
	\centering

	\includegraphics[scale = .42]{write_cr3_call_stack.pdf}

	\caption[Pile d'appel d'un changement de contexte menant à un changement de table des pages courante via l'écriture du \texttt{cr3}]{Pile d'appel d'un changement de contexte menant à un changement de table des pages courante via l'écriture du \texttt{cr3} \cite{noauthor_linux_2018}}
	\label{fig:write_cr3_call_stack}
\end{figure}

Depuis l'apparition de la faille \textit{meltdown}, le changement du contenu du \textit{cr3} est aussi effectué lors du passage d'un processeur du mode utilisateur au mode superviseur \cite{lipp_meltdown_2018, noauthor_page_nodate}.
Dans le noyau Linux, la contre-mesure à cette faille porte le nom de \textit{Page Table Isolation} (PTI).

Avec la PTI, la table des pages d'un processus s'exécutant en mode utilisateur ne contient pas la totalité des adresses noyaux mais uniquement celles qui sont, entre autres, nécessaires au passage du processus en mode superviseur.
Suite à un appel système, la valeur du \texttt{cr3} sera modifiée pour contenir une version de la table des pages avec toutes les adresses noyaux \citelinux{arch/x86/entry/entry\_64.S}{174}.
Quand le processus retournera en mode utilisateur, la table des pages "allégée" sera chargée dans son \texttt{cr3} \citelinux{arch/x86/entry/entry\_64.S}{332}.

Avant cette faille, la table des pages d'un processus s'exécutant en mode utilisateur contenait la totalité des adresses noyaux\footnote{Le patch \textit{kaiser} permettait déjà d'isoler les adresses noyaux des adresses utilisateurs \cite{bodden_kaslr_2017}.}.

\subsection{Page utilisée pour la zone périphérique}
Les pages appartenant à la plage d'adresse de la \texttt{ZONE\_DEVICE} ne sont jamais marquées en ligne \cite{noauthor_physical_nodate}.
la \texttt{ZONE\_DEVICE} est, d'après la documentation de Linux, utilisé par les types de mémoire suivants :
\begin{description}
	\item[pmem (\textit{persistent memory}) :] Permet d'accéder à la mémoire non volatile de la machine via des entrées/sorties directes \cite{corbet_zone_device_2017}.
	\item[hmm (\textit{heterogeneous memory management}) :] Le \texttt{hmm} est une fonctionnalité du noyau Linux permettant la gestion de la mémoire attachée à un périphérique, comme, par exemple, la mémoire d'une carte graphique \cite{noauthor_heterogeneous_nodate}.
	\item[p2pdma (\textit{peer 2 peer direct memory access}) :] Permet à deux périphériques reliés au bus PCI-E de s'échanger des informations via des accès DMA direct sans passer par la mémoire centrale.
\end{description}