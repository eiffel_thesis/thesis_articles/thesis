\section{La \texttt{struct zone}}
\label{sec:zone}
Comme nous l'avons étudié dans la section précédente, la mémoire d'un nœud est découpée en plusieurs zones.
Une zone est représentée par la \texttt{struct zone} \citelinux{include/linux/mmzone.h}{359}.
Le code de cette structure est donnée dans le code \ref{lst:zone}.

\begin{code}
	\centering

	\begin{minted}[autogobble, tabsize = 2, breaklines, linenos, firstnumber = 359, bgcolor = nativebg]{C}
		struct zone {
			/* Read-mostly fields */

			/* zone watermarks, access with *_wmark_pages(zone) macros */
			unsigned long watermark[NR_WMARK];

			unsigned long nr_reserved_highatomic;

			/*
			 * We don't know if the memory that we're going to allocate will be
			 * freeable or/and it will be released eventually, so to avoid totally
			 * wasting several GB of ram we must reserve some of the lower zone
			 * memory (otherwise we risk to run OOM on the lower zones despite
			 * there being tons of freeable ram on the higher zones).  This array is
			 * recalculated at runtime if the sysctl_lowmem_reserve_ratio sysctl
			 * changes.
			 */
			long lowmem_reserve[MAX_NR_ZONES];

		#ifdef CONFIG_NUMA
			int node;
		#endif
			struct pglist_data	*zone_pgdat;
			struct per_cpu_pageset __percpu *pageset;

		#ifndef CONFIG_SPARSEMEM
			/*
			 * Flags for a pageblock_nr_pages block. See pageblock-flags.h.
			 * In SPARSEMEM, this map is stored in struct mem_section
			 */
			unsigned long		*pageblock_flags;
		#endif /* CONFIG_SPARSEMEM */

			/* zone_start_pfn == zone_start_paddr >> PAGE_SHIFT */
			unsigned long		zone_start_pfn;

			/*
			 * spanned_pages is the total pages spanned by the zone, including
			 * holes, which is calculated as:
			 * 	spanned_pages = zone_end_pfn - zone_start_pfn;
			 *
			 * present_pages is physical pages existing within the zone, which
			 * is calculated as:
			 *	present_pages = spanned_pages - absent_pages(pages in holes);
			 *
			 * managed_pages is present pages managed by the buddy system, which
			 * is calculated as (reserved_pages includes pages allocated by the
			 * bootmem allocator):
			 *	managed_pages = present_pages - reserved_pages;
			 *
			 * So present_pages may be used by memory hotplug or memory power
			 * management logic to figure out unmanaged pages by checking
			 * (present_pages - managed_pages). And managed_pages should be used
			 * by page allocator and vm scanner to calculate all kinds of watermarks
			 * and thresholds.
			 *
			 * Locking rules:
			 *
			 * zone_start_pfn and spanned_pages are protected by span_seqlock.
			 * It is a seqlock because it has to be read outside of zone->lock,
			 * and it is done in the main allocator path.  But, it is written
			 * quite infrequently.
			 *
			 * The span_seq lock is declared along with zone->lock because it is
			 * frequently read in proximity to zone->lock.  It's good to
			 * give them a chance of being in the same cacheline.
			 *
			 * Write access to present_pages at runtime should be protected by
			 * mem_hotplug_begin/end(). Any reader who can't tolerant drift of
			 * present_pages should get_online_mems() to get a stable value.
			 *
			 * Read access to managed_pages should be safe because it's unsigned
			 * long. Write access to zone->managed_pages and totalram_pages are
			 * protected by managed_page_count_lock at runtime. Idealy only
			 * adjust_managed_page_count() should be used instead of directly
			 * touching zone->managed_pages and totalram_pages.
			 */
			unsigned long		managed_pages;
			unsigned long		spanned_pages;
			unsigned long		present_pages;

			const char		*name;

		#ifdef CONFIG_MEMORY_ISOLATION
			/*
			 * Number of isolated pageblock. It is used to solve incorrect
			 * freepage counting problem due to racy retrieving migratetype
			 * of pageblock. Protected by zone->lock.
			 */
			unsigned long		nr_isolate_pageblock;
		#endif

		#ifdef CONFIG_MEMORY_HOTPLUG
			/* see spanned/present_pages for more description */
			seqlock_t		span_seqlock;
		#endif

			int initialized;

			/* Write-intensive fields used from the page allocator */
			ZONE_PADDING(_pad1_)

			/* free areas of different sizes */
			struct free_area	free_area[MAX_ORDER];

			/* zone flags, see below */
			unsigned long		flags;

			/* Primarily protects free_area */
			spinlock_t		lock;

			/* Write-intensive fields used by compaction and vmstats. */
			ZONE_PADDING(_pad2_)

			/*
			 * When free pages are below this point, additional steps are taken
			 * when reading the number of free pages to avoid per-cpu counter
			 * drift allowing watermarks to be breached
			 */
			unsigned long percpu_drift_mark;

		#if defined CONFIG_COMPACTION || defined CONFIG_CMA
			/* pfn where compaction free scanner should start */
			unsigned long		compact_cached_free_pfn;
			/* pfn where async and sync compaction migration scanner should start */
			unsigned long		compact_cached_migrate_pfn[2];
		#endif

		#ifdef CONFIG_COMPACTION
			/*
			 * On compaction failure, 1<<compact_defer_shift compactions
			 * are skipped before trying again. The number attempted since
			 * last failure is tracked with compact_considered.
			 */
			unsigned int		compact_considered;
			unsigned int		compact_defer_shift;
			int			compact_order_failed;
		#endif

		#if defined CONFIG_COMPACTION || defined CONFIG_CMA
			/* Set to true when the PG_migrate_skip bits should be cleared */
			bool			compact_blockskip_flush;
		#endif

			bool			contiguous;

			ZONE_PADDING(_pad3_)
			/* Zone statistics */
			atomic_long_t		vm_stat[NR_VM_ZONE_STAT_ITEMS];
			atomic_long_t		vm_numa_stat[NR_VM_NUMA_STAT_ITEMS];
		} ____cacheline_internodealigned_in_smp;
	\end{minted}

	\caption[La \texttt{struct zone}]{La \texttt{struct zone} \cite[fichier \texttt{include/linux/mmzone.h}]{noauthor_linux_2018}}
	\label{lst:zone}
\end{code}

Comme pour la \texttt{struct pglist\_data}, nous allons commenter certains champs de la \texttt{struct zone}.

\subsection{Noeud d'appartenance}
\label{subsec:pglist_data_link}
Le champ \texttt{zone\_pgdat} pointe sur la \texttt{pglist\_data} à laquelle cette zone appartient \citelinux{include/linux/mmzone.h}{381}.

\subsection{Seuils d'allocation}
\label{subsec:watermarks}
Lorsqu'une page est allouée, celle-ci l'est depuis une zone.
Le nombre de pages gérées par le \textit{buddy allocator} d'une zone est accessible via le champ \texttt{managed\_pages} \citelinux{include/linux/mmzone.h}{436}.
Afin de mesurer la pression mémoire, une zone possède 3 marqueurs qui sont compilés dans le Tableau \ref{table:watermarks}.

\begin{table}
	\centering

	\caption{Les différents marqueurs utilisés lors de l'allocation de pages}
	\label{table:watermarks}

	\begin{tabular}{cc}
		\hline
		Marqueur & Macro associée\\
		\hline
		\texttt{WMARK\_MIN} & \texttt{min\_wmark\_pages}\\
		\texttt{WMARK\_LOW} & \texttt{low\_wmark\_pages}\\
		\texttt{WMARK\_HIGH} & \texttt{high\_wmark\_pages}\\
		\hline
	\end{tabular}
\end{table}

Grossièrement, lorsque la mémoire libre de la zone descend en-dessous de \texttt{WMARK\_MIN}, le mécanisme de réclamation mémoire est activé \cite[Section 2.2.1]{gorman_understanding_2004}.
Nous reviendrons sur ces marqueurs quand nous étudierons le mécanisme de réclamation mémoire dans le Chapitre \ref{TODO}.

\subsection{Le \textit{buddy allocator}}
\label{subsec:buddy_allocator}
Le \textit{buddy allocator} est l'allocateur de page du noyau Linux \cite[Chapitre 6]{gorman_understanding_2004}.
Il est accessible via le champ \texttt{free\_area} \citelinux{include/linux/mmzone.h}{462}.
Ce champ est un tableau de \texttt{MAX\_ORDER} structures \texttt{free\_area} \citelinux{include/linux/mmzone.h}{96}.
Ces structures stockent un tableau de \texttt{MIGRATE\_TYPES} listes qui seont utilisées pour lier des blocs de pages libres entre elles.
Nous laissons, pour le moment, de côté les différents types définis par l'\texttt{enum migratetype} \citelinux{include/linux/mmzone.h}{39} \cite[Slide 38]{nazarewicz_continuous_2012}.
Nous y reviendrons néanmoins dans le Chapitre \ref{TODO}.

Revenons donc au tableau \texttt{free\_area} de la \texttt{struct zone}.
Plutôt que de parler de case ou d'indices pour ce tableau, il est plus commun d'utiliser la dénomination d'ordre.
Ainsi, la \texttt{free\_area} d'ordre $n$ correspond à la case $n$ du tableau.
Plus particulièrement, la \texttt{free\_area} d'ordre $n$ est une liste de blocs de $n$ pages physiques libres contiguës.
Par exemple, des blocs de 4 pages contiguës, soit \SI{16}{\kilo\byte}, sont chaînés dans la cinquième\sidenote{La première \texttt{free\_area} a pour ordre 0, ainsi la cinquième est d'odre 4.} \texttt{free\_area}.

L'utilisation du \textit{buddy allocator} pour obtenir une ou plusieurs pages sera détaillée dans le Chapitre \ref{TODO}.

\begin{figure}
	\centering

	TODO

	\caption[Organisation interne du \textit{buddy allocator}]{Organisation interne du \textit{buddy allocator} \cite[]{gorman_understanding_2004}}
	\label{fig:label}
\end{figure}

\subsection{Le \textit{per cpu pages allocator}}
\label{subsec:per_cpu_pages_allocator}
Cet allocateur est utilisé pour allouer une seule page, c'est-à-dire pour des allocations d'odre 0 \citelinux{mm/page\_alloc.c}{3271}.
Il y a un tel allocateur par CPU et par zone.
Il est accessible via le champ \texttt{pageset} \citelinux{include/linux/mmzone.h}{432}.
Ce champ a pour type \texttt{struct per\_cpu\_pageset}, cette structure contient un champ \texttt{pcp} de type \texttt{struct per\_cpu\_pages} \citelinux{include/linux/mmzone.h}{349}\citelinux{include/linux/mmzone.h}{350}.
C'est la structure \texttt{per\_cpu\_pages} qui représente le \textit{per cpu pages allocator} \citelinux{include/linux/mmzone.h}{340}.
Elle contient un tableau de \texttt{MIGRATE\_PCPTYPES} listes, ces listes servent à chaîner entre elles des pages \citelinux{include/linux/mmzone.h}{346}.

Le nombre de pages gérées par cet allocateur est stocké dans le champ \texttt{count} \citelinux{include/linux/mmzone.h}{341}.
Le champ \texttt{batch} est utilisé pour indiquer la quantité de pages qui sera ajoutée ou prise au \textit{buddy allocator} \citelinux{include/linux/mmzone.h}{343}.

Contrairement au \textit{buddy allocator}, le \textit{per cpu pages allocator} n'a qu'un marqueur de pression mémoire : \texttt{high} \citelinux{include/linux/mmzone.h}{342}.

Nous détaillerons le fonctionnement de cet allocateur, ainsi que ses interactions avec le \textit{buddy allocator} dans le Chapitre \ref{TODO}.