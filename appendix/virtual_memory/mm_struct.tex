\section{La \texttt{mm\_struct}}
\label{sec:mm_struct}
La dernière des structures de données utilisée par le noyau pour gérer la mémoire virtuelle est la \texttt{mm\_struct}.
Chaque tâche \footnote{
	Dans le monde UNIX, il y a une distinction entre un processus et un thread, un processus comportant en général un ou plusieurs threads.
	Linux, ne fait pas cette différence, les processus et threads sont tout deux représentés par la \texttt{task\_struct} \citelinux{include/linux/sched.h}{593}.
	Ainsi, pour Linux et comme indiqué par Robert Love, un thread n'est qu'un processus qui partage certaines ressources avec d'autres processus \cite[Section "The Linux Implementation of Threads"]{love_linux_2010} :
	\begin{quote}
		Linux has a unique implementation of threads. To the Linux kernel, there is no concept of a thread. Linux implements all threads as standard processes. [...]. Instead, a thread is merely a process that shares certain resources with other processes.
	\end{quote}
}, représentée par une \texttt{task\_struct} possède une \texttt{mm\_struct} accessible via le pointeur \texttt{mm} \citelinux{include/linux/sched.h}{688}.
Dans les sections suivantes, nous allons décrire certains des champs de cette structure dont le code est donné dans le Code \ref{lst:mm_struct}.

\begin{code}
	\centering

	\begin{minted}[autogobble, tabsize = 2, breaklines, linenos, firstnumber = 258, bgcolor = nativebg]{C}
		struct mm_struct {
			struct {
				struct vm_area_struct *mmap;		/* list of VMAs */
				struct rb_root mm_rb;
				u64 vmacache_seqnum;                   /* per-thread vmacache */
		#ifdef CONFIG_MMU
				unsigned long (*get_unmapped_area) (struct file *filp,
						unsigned long addr, unsigned long len,
						unsigned long pgoff, unsigned long flags);
		#endif
				unsigned long mmap_base;	/* base of mmap area */
				unsigned long mmap_legacy_base;	/* base of mmap area in bottom-up allocations */
		#ifdef CONFIG_HAVE_ARCH_COMPAT_MMAP_BASES
				/* Base adresses for compatible mmap() */
				unsigned long mmap_compat_base;
				unsigned long mmap_compat_legacy_base;
		#endif
				unsigned long task_size;	/* size of task vm space */
				unsigned long highest_vm_end;	/* highest vma end address */
				pgd_t * pgd;

				/**
				 * @mm_users: The number of users including userspace.
				 *
				 * Use mmget()/mmget_not_zero()/mmput() to modify. When this
				 * drops to 0 (i.e. when the task exits and there are no other
				 * temporary reference holders), we also release a reference on
				 * @mm_count (which may then free the &struct mm_struct if
				 * @mm_count also drops to 0).
				 */
				atomic_t mm_users;

				/**
				 * @mm_count: The number of references to &struct mm_struct
				 * (@mm_users count as 1).
				 *
				 * Use mmgrab()/mmdrop() to modify. When this drops to 0, the
				 * &struct mm_struct is freed.
				 */
				atomic_t mm_count;

		#ifdef CONFIG_MMU
				atomic_long_t pgtables_bytes;	/* PTE page table pages */
		#endif
				int map_count;			/* number of VMAs */

				spinlock_t page_table_lock; /* Protects page tables and some
									* counters
									*/
				struct rw_semaphore mmap_sem;

				struct list_head mmlist; /* List of maybe swapped mm's.	These
								* are globally strung together off
								* init_mm.mmlist, and are protected
								* by mmlist_lock
								*/


				unsigned long hiwater_rss; /* High-watermark of RSS usage */
				unsigned long hiwater_vm;  /* High-water virtual memory usage */

				unsigned long total_vm;	   /* Total pages mapped */
				unsigned long locked_vm;   /* Pages that have PG_mlocked set */
				unsigned long pinned_vm;   /* Refcount permanently increased */
				unsigned long data_vm;	   /* VM_WRITE & ~VM_SHARED & ~VM_STACK */
				unsigned long exec_vm;	   /* VM_EXEC & ~VM_WRITE & ~VM_STACK */
				unsigned long stack_vm;	   /* VM_STACK */
				unsigned long def_flags;

				spinlock_t arg_lock; /* protect the below fields */
				unsigned long start_code, end_code, start_data, end_data;
				unsigned long start_brk, brk, start_stack;
				unsigned long arg_start, arg_end, env_start, env_end;

				unsigned long saved_auxv[AT_VECTOR_SIZE]; /* for /proc/PID/auxv */

				/*
				 * Special counters, in some configurations protected by the
				 * page_table_lock, in other configurations by being atomic.
				 */
				struct mm_rss_stat rss_stat;

				struct linux_binfmt *binfmt;

				/* Architecture-specific MM context */
				mm_context_t context;

				unsigned long flags; /* Must use atomic bitops to access */

				struct core_state *core_state; /* coredumping support */
		#ifdef CONFIG_MEMBARRIER
				atomic_t membarrier_state;
		#endif
		#ifdef CONFIG_AIO
				spinlock_t			ioctx_lock;
				struct kioctx_table __rcu	*ioctx_table;
		#endif
		#ifdef CONFIG_MEMCG
				/*
				 * "owner" points to a task that is regarded as the canonical
				 * user/owner of this mm. All of the following must be true in
				 * order for it to be changed:
				 *
				 * current == mm->owner
				 * current->mm != mm
				 * new_owner->mm == mm
				 * new_owner->alloc_lock is held
				 */
				struct task_struct __rcu *owner;
		#endif
				struct user_namespace *user_ns;

				/* store ref to file /proc/<pid>/exe symlink points to */
				struct file __rcu *exe_file;
		#ifdef CONFIG_MMU_NOTIFIER
				struct mmu_notifier_mm *mmu_notifier_mm;
		#endif
		#if defined(CONFIG_TRANSPARENT_HUGEPAGE) && !USE_SPLIT_PMD_PTLOCKS
				pgtable_t pmd_huge_pte; /* protected by page_table_lock */
		#endif
		#ifdef CONFIG_NUMA_BALANCING
				/*
				 * numa_next_scan is the next time that the PTEs will be marked
				 * pte_numa. NUMA hinting faults will gather statistics and
				 * migrate pages to new nodes if necessary.
				 */
				unsigned long numa_next_scan;

				/* Restart point for scanning and setting pte_numa */
				unsigned long numa_scan_offset;

				/* numa_scan_seq prevents two threads setting pte_numa */
				int numa_scan_seq;
		#endif
				/*
				 * An operation with batched TLB flushing is going on. Anything
				 * that can move process memory needs to flush the TLB when
				 * moving a PROT_NONE or PROT_NUMA mapped page.
				 */
				atomic_t tlb_flush_pending;
		#ifdef CONFIG_ARCH_WANT_BATCHED_UNMAP_TLB_FLUSH
				/* See flush_tlb_batched_pending() */
				bool tlb_flush_batched;
		#endif
				struct uprobes_state uprobes_state;
		#ifdef CONFIG_HUGETLB_PAGE
				atomic_long_t hugetlb_usage;
		#endif
				struct work_struct async_put_work;

		#if IS_ENABLED(CONFIG_HMM)
				/* HMM needs to track a few things per mm */
				struct hmm *hmm;
		#endif
			} __randomize_layout;

			/*
			 * The mm_cpumask needs to be at the end of mm_struct, because it
			 * is dynamically sized based on nr_cpu_ids.
			 */
			unsigned long cpu_bitmap[];
		};
	\end{minted}

	\caption[La \texttt{mm\_struct page}]{La \texttt{mm\_struct page} \citelinux{include/linux/mm\_types.h}{340}}
	\label{lst:mm_struct}
\end{code}

\subsection{Chaînage des différentes \texttt{vm\_area\_struct}}
Une \texttt{mm\_struct} regroupe plusieurs \texttt{vm\_area\_struct}.
La liste contenant celles-ci est accessible via le champ \texttt{mmap}, en effet, les \texttt{vm\_area\_struct} sont chaînées via leurs champs \texttt{vm\_next} et \texttt{vm\_prev}.
Il est donc possible de parcourir toutes les \texttt{vm\_area\_struct} d'une \texttt{mm\_struct}.

Ces structures sont aussi rassemblées, via leurs champs \texttt{vm\_rb}, dans l'arbre rouge noir dont le champ \texttt{mm\_rb} est la racine\footnote{
Le codeur averti aura remarqué que ce champ et le champ \texttt{vm\_rb} n'ont pas le même type.
	En effet, ce champ a pour type \texttt{rb\_root} tandis que le champ \texttt{vm\_rb} est de type \texttt{rb\_node}.
	Dans le noyau Linux, un arbre rouge noir possède une racine de type \texttt{rb\_root} tandis que ses noeuds ont pour type \texttt{rb\_node}.
	Néanmoins, la structure \texttt{rb\_root} est définie comme ne contenant qu'un seul champ de type \texttt{rb\_node} \citelinux{include/linux/rbtree.h}{43}.
}.

Le nombre de \texttt{vm\_area\_struct} rattachée à une \texttt{mm\_struct} est contenue dans le champ \texttt{map\_count}.

\subsection{Trouver un intervalle d'adresse non alloué}
La fonction, stockée dans le pointeur de fonction \texttt{get\_unmapped\_area }, sera appelée lors d'un appel à \texttt{do\_mmap}.
Le but de cette fonction sera de trouver, dans cet espace d'adressage, un intervalle d'adresse linéaires qui n'a pas encore été alloué.

\subsection{Taille maximale de l'espace d'adressage virtuel}
Contrairement à ce qu'il est possible de penser de prime abord, le champ \texttt{task\_size} ne contient pas la somme des tailles des \texttt{vm\_area\_struct} mais la taille maximale de l'espace d'adressage virtuel décrit.

Il contient donc la valeur \texttt{TASK\_SIZE} qui, pour l'architecture x86\_64, est une macro remplacée par \texttt{((1UL << \_\_VIRTUAL\_MASK\_SHIFT) - PAGE\_SIZE)} \citelinux{fs/exec.c}{1377}\citelinux{arch/x86/include/asm/processor.h}{884}.
\texttt{\_\_VIRTUAL\_MASK\_SHIFT} est aussi une macro remplacée par 56 ou 47 selon si les tables des pages contiennent 5 ou 4 niveaux \citelinux{arch/x86/include/asm/page\_64\_types.h}{56}.
Les adresses supérieures à \texttt{TASK\_SIZE} seront uniquement utilisées par le noyau \cite{the_kernel_development_community_memory_nodate}.

\subsection{L'adresse linéaire la plus haute de l'espace d'adressage virtuel}
Le champ \texttt{highest\_vm\_end} contient l'adresse linéaire la plus haute.

\subsection{Table des pages}
Le champ \texttt{pgd} pointe sur la table des pages associée à cet espace d'adressage linéaire.

\subsection{Nombre d'utilisateurs de la structure}
Le champ \texttt{mm\_users} contient le nombre d'utilisateurs de cette \texttt{mm\_struct}.
En effet, une \texttt{mm\_struct} n'est pas propre à une \texttt{task\_struct}.
Plusieurs \texttt{task\_struct} peuvent pointer sur une même \texttt{mm\_struct}, c'est le cas notamment des threads POSIX qui sont, chacune, représentées par une \texttt{task\_struct} mais partagent un même espace d'adressage virtuel donc une même \texttt{mm\_struct}.
Comme indiqué par la page de manuel des threads POSIX, chaque thread possède sa propre pile \cite{noauthor_pthreads7_nodate}.
Néanmoins, cette pile n'est pas privée, et un thread peut accéder aux variables de la pile d'un autre thread comme le montre le Code \ref{lst:toy}.

\begin{code}
	\centering

	\begin{minted}[autogobble, tabsize = 2, breaklines, linenos, bgcolor = nativebg]{C}
		#include <stdlib.h>
		#include <stdio.h>
		#include <pthread.h>
		#include <assert.h>

		#define MAIN_VALUE 353
		#define THREAD_VALUE 748


		/**
			* Set given pointer to THREAD_VALUE before checking that its value is
			* MAIN_VALUE.
			*
			* @param arg This pointer points to a main stack variable. It is a pointer to
			* an int.
			* @return NULL always.
			*/
		void *thread(void *arg){
			int *shared;

			// Cast arg to int pointer.
			shared = (int *) arg;

			// Check that its value is MAIN_VALUE
			assert(*shared == MAIN_VALUE);

			// Set its value to THREAD_VALUE.
			*shared = THREAD_VALUE;

			return NULL;
		}

		/**
			* Set a variable in main stack, create a thread and change the value of the
			* variable from the thread.
			*
			* @return EXIT_SUCCESS if everything ran smoothly, EXIT_FAILURE otherwise (i.e.
			* a call to pthread_* failed).
			*/
		int main(void){
			int shared;
			pthread_t tid;

			// Set stack variable value.
			shared = MAIN_VALUE;

			// Create thread.
			if(pthread_create(&tid, NULL, thread, &shared)){
				perror("pthread_create:");

				return EXIT_FAILURE;
			}

			// Wait for it.
			if(pthread_join(tid, NULL)){
				perror("pthread_join");

				return EXIT_FAILURE;
			}

			// Check that variable value becomes THREAD_VALUE.
			assert(shared == THREAD_VALUE);

			return EXIT_SUCCESS;
		}
	\end{minted}

	\caption{Ce code montre le changement d'une variable de pile depuis la pile d'un autre thread.}
	\label{lst:toy}
\end{code}

\subsection{\texttt{mmap\_sem}}
Ce sémaphore est pris pour la plupart des opérations sur les \texttt{vm\_area\_struct} de cette \texttt{mm\_struct} \citelinux{mm/util.c}{344}.

\subsection{Champs de statistiques du nombre de page}
Les champs décrits dans le Tableau \ref{table:stats_page} contiennent le nombre de pages utilisées pour différents types de \texttt{vm\_area\_struct}.

\begin{table}
	\centering

	\caption{Les champs utilisés pour contenir le nombre de page de différents types de \texttt{vm\_area\_struct}}
	\label{table:stats_page}

	\begin{tabular}{cp{.2\textwidth}p{.8\textwidth}}
		\hline
		Champ & Drapeaux & Description\\
		\hline
		\texttt{total\_vm} & & Ce champ contient le nombre de pages utilisées par cet espace d'adressage.\\
		\texttt{locked\_vm} & \texttt{VM\_LOCKED} & Ce champ contient le nombre de pages verrouillées.\\
		\texttt{data\_vm} & \texttt{VM\_WRITE \& $\sim$VM\_SHARED \& $\sim$VM\_STACK} & Ce champ contient le nombre de pages utilisées pour stocker les données n'étant pas dans la pile.
		Ces \texttt{vm\_area\_struct} correspondent donc à la section \texttt{.data} d'un fichier \texttt{elf} \cite{}.\\
		\texttt{exec\_vm} & \texttt{VM\_EXEC \& $\sim$VM\_WRITE \& $\sim$VM\_STACK} & Ce champ contient le nombre de pages utilisées pour le code.
		La section \texttt{.text} d'un fichier \texttt{elf} correspond à ces pages \cite{}.\\
		\texttt{stack\_vm} & \texttt{VM\_STACK} & Ce champ contient le nombre de page utilisées pour la pile.
		Le drapeau \texttt{VM\_STACK} correspond au drapeau \texttt{VM\_GROWSDOWN} \citelinux{include/linux/mm.h}{281}.\\
		\hline
	\end{tabular}
\end{table}


\subsection{Champ de début et de fin de section}
Ces différents champs, regroupés dans le Tableau \ref{table:begin_end}, contiennent les adresses linéaires de début et de fin de différentes sections de l'espace d'adressage virtuel.

\begin{table}
	\centering

	\caption{Les champs utilisés pour contenir les adresses linéaires de début et de fin de différentes sections}
	\label{table:begin_end}

	\begin{tabular}{ccp{.9\textwidth}}
		\hline
		Début & Fin & Description\\
		\hline
		\texttt{start\_code} & \texttt{end\_code} & Ces champs contiennent, respectivement, la première et dernière adresses linéaires utilisées pour stocker du code.\\
		\texttt{start\_data} & \texttt{end\_data} & Ces champs contiennent, respectivement, la première et dernière adresses linéaires utilisées pour stocker des données.\\
		\texttt{start\_brk} & \texttt{brk} & Ces champs contiennent, respectivement, la première et la dernière adresses linénaires du tas.
		La valeur contenue dans \texttt{brk} pourra donc être modifiée par un appel à \texttt{malloc}.\\
		\texttt{start\_stack} & & Ce champ contient l'adresse linéaire du haut de la pile, en effet, celle-ci croît vers le bas.
		Il n'y a pas de champ \texttt{end\_stack} car la taille de la pile varie souvent, nous verrons plus loin comme sont gérés les défauts de page de la pile.
		De plus, certaines architectures disposent d'un registre stockant l'adresse courante du sommet de la pile.
		Pour un processeur x86\_64, le registre \texttt{rsp} stocke cette valeur \cite[Section 3.4.1]{intel_basic_2019}.\\
		\texttt{arg\_start} & \texttt{arg\_end} & Ces champs contiennent, respectivement, la première et dernière adresses linéaires utilisées pour stocker les arguments du programme.\\
		\texttt{env\_start} & \texttt{env\_end} & Ces champs contiennent, respectivement, la première et dernière adresses linéaires utilisées pour stocker les variables d'environnement correspondantes à l'environnement dans lequel le programme a été exécuté.\\
		\hline
	\end{tabular}
\end{table}

\subsection{Statistiques sur les différents types de pages résidant en émoire}
Le champ \texttt{rss\_stat} contient ces statistiques.
En effet, \texttt{rss} est un acronyme pour \textit{Resident Set Size} ce qui correspond aux pages qui sont en mémoire physique pour cette application \cite{gregg_working_2018}.
Ce champ contient donc des informations sur les nombres de pages fichiers actuellement en mémoire, de pages anonymes actuellement en mémoire, de pages actuellement stockées dans l'espace d'échange et de pages partagées \citelinux{include/linux/mm\_types\_task.h}{39}.

\subsection{Format binaire}
Le champ \texttt{binfmt} pointe sur une structure \texttt{linux\_binfmt} qui décrit un format binaire.
Puisque Linux supporte, entre autres, les formats binaires \texttt{a.out} et \texttt{elf}, il existe une structure de ce type pour chacun de ces deux formats \citelinux{fs/binfmt\_aout.c}{117}\citelinux{fs/binfmt\_elf.c}{93}.