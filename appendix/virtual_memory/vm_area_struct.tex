\section{La \texttt{struct vm\_area\_struct}}
\label{sec:vm_area_struct}
Comme nous l'avons vu précédemment, chaque processus dispose de son propre espace d'adressage virtuel qui s'appuie sur sa table des pages.
Du point de vue du processus, son espace d'adressage virtuel est un espace mémoire contigüe.
Or le noyau regroupe les différentes pages virtuelles d'un processus dans plusieurs \texttt{struct vm\_area\_struct} \citelinux{include/linux/mm\_types.h}{264}.
Une \texttt{vm\_area\_struct} est un ensemble de pages physiques ayant les mêmes droits.
Dans les sections suivantes, nous détaillerons certains champs de cette structure puis nous étudierons spécifiquement les drapeaux pouvant être stockés dans le champ \texttt{vm\_flags}.

\subsection{Les champs de la \texttt{struct vm\_area\_struct}}
Dans cette section, nous allons décrire certains champs de la \texttt{vm\_area\_struct} dont le code est exposé dans le code \ref{lst:vm_area_struct}.

\begin{code}
	\centering

	\begin{minted}[autogobble, tabsize = 2, breaklines, linenos, firstnumber = 264, bgcolor = nativebg]{C}
		struct vm_area_struct {
			/* The first cache line has the info for VMA tree walking. */

			unsigned long vm_start;		/* Our start address within vm_mm. */
			unsigned long vm_end;		/* The first byte after our end address
								within vm_mm. */

			/* linked list of VM areas per task, sorted by address */
			struct vm_area_struct *vm_next, *vm_prev;

			struct rb_node vm_rb;

			/*
			 * Largest free memory gap in bytes to the left of this VMA.
			 * Either between this VMA and vma->vm_prev, or between one of the
			 * VMAs below us in the VMA rbtree and its ->vm_prev. This helps
			 * get_unmapped_area find a free area of the right size.
			 */
			unsigned long rb_subtree_gap;

			/* Second cache line starts here. */

			struct mm_struct *vm_mm;	/* The address space we belong to. */
			pgprot_t vm_page_prot;		/* Access permissions of this VMA. */
			unsigned long vm_flags;		/* Flags, see mm.h. */

			/*
			 * For areas with an address space and backing store,
			 * linkage into the address_space->i_mmap interval tree.
			 */
			struct {
				struct rb_node rb;
				unsigned long rb_subtree_last;
			} shared;

			/*
			 * A file's MAP_PRIVATE vma can be in both i_mmap tree and anon_vma
			 * list, after a COW of one of the file pages.	A MAP_SHARED vma
			 * can only be in the i_mmap tree.  An anonymous MAP_PRIVATE, stack
			 * or brk vma (with NULL file) can only be in an anon_vma list.
			 */
			struct list_head anon_vma_chain; /* Serialized by mmap_sem &
								* page_table_lock */
			struct anon_vma *anon_vma;	/* Serialized by page_table_lock */

			/* Function pointers to deal with this struct. */
			const struct vm_operations_struct *vm_ops;

			/* Information about our backing store: */
			unsigned long vm_pgoff;		/* Offset (within vm_file) in PAGE_SIZE
								units */
			struct file * vm_file;		/* File we map to (can be NULL). */
			void * vm_private_data;		/* was vm_pte (shared mem) */

			atomic_long_t swap_readahead_info;
		#ifndef CONFIG_MMU
			struct vm_region *vm_region;	/* NOMMU mapping region */
		#endif
		#ifdef CONFIG_NUMA
			struct mempolicy *vm_policy;	/* NUMA policy for the VMA */
		#endif
			struct vm_userfaultfd_ctx vm_userfaultfd_ctx;
		} __randomize_layout;
	\end{minted}

	\caption[La \texttt{vm\_area\_struct}]{La \texttt{vm\_area\_struct} \citelinux{include/linux/mm\_types.h}{264}}
	\label{lst:vm_area_struct}
\end{code}

\subsubsection{Début et fin d'aire}
Le champ \texttt{vm\_start} indique l'adresse linéaire qui marque le début de cette aire tandis que le champ \texttt{vm\_end} est l'adresse du premier octet à la fin de l'aire.

Pour éviter toute ambiguïté avec les segments Intel utilisés pour adresser la mémoire, nous préférons parler d'aire \cite[Section 1.3.4]{intel_basic_2019}.
À ma connaissance, les segments Intel ne sont plus utilisés aujourd'hui à deux exceptions près :
\begin{enumerate}
	\item Lors de la phase de démarrage du noyau Linux \citelinux{arch/x86/boot/pm.c}{72}.
	Comme nous l'avons vu précédemment, Linux s'appuie sur des pages et n'utilise donc pas ces segments.
	\item Pour le \textit{Thread Local Storage} (TLS), comme son nom l'indique, le TLS est un espace de stockage propre à chaque thread \cite{drepper_elf_2013}.
	Par exemple, la variable \texttt{errno} est stockée dans le TLS.
	Si ce n'était pas le cas, un appel système dans un thread conduirait à la modification de cette variable dans tous les threads et pourraient avoir des conséquences désastreuses.
	Le TLS s'appuie notamment sur le segment \texttt{FS} \cite[Section 3.4.6]{drepper_elf_2013}.
\end{enumerate}

\subsubsection{Chaînage entre les différentes \texttt{vm\_area\_struct}}
Les \texttt{vm\_area\_struct} d'un processus sont chaînées entres elles via les champs \texttt{vm\_next} et \texttt{vm\_prev}.
Les \texttt{vm\_area\_struct} de cette liste sont triées par adresse croissante.

Ces \texttt{vm\_area\_struct} sont aussi groupées dans un arbre rouge noir accessible depuis le champ \texttt{vm\_rb} \cite{guibas_dichromatic_1978}.
L'arbre rouge noir permet d'accélérer la recherche, comparé à la liste chaînée ($O(\log{}n)$ contre $O(n)$).

\subsubsection{\texttt{vm\_mm}}
L'espace d'adressage virtuel auquel la \texttt{vm\_area\_struct} appartient.

\subsubsection{La \textit{reverse map} (\textit{rmap})}
Le champ \texttt{anon\_vma} fait partie de la \textit{rmap}.
La \textit{rmap} est utilisée par l'algorithme de réclamation des pages pour savoir à quelle \texttt{vm\_area\_struct} une page appartient.

Ainsi, une page appartient à une \texttt{struct anon\_vma}, celle-ci peut être accédée via le champ \texttt{mapping} de la \texttt{struct page} \cite[\texttt{mm/rmap.c}, ligne 1000]{noauthor_linux_2018}.

Le lien entre une \texttt{struct anon\_vma} et une \texttt{vm\_area\_struct} est assurée par la \texttt{struct anon\_vma\_chain} \citelinux{include/linux/rmap.h}{77}.
Ce lien est notamment effectué dans la fonction \texttt{rmap\_walk\_anon} grâce à la macro \texttt{anon\_vma\_interval\_tree\_foreach}\footnote{
	Cette macro est définie comme une boucle \texttt{for} \citelinux{include/linux/mm.h}{2213}.

	Elle s'appuie sur la fonction \texttt{anon\_vma\_interval\_tree\_iter\_first} qui appelle la fonction \texttt{\_\_anon\_vma\_interval\_tree\_iter\_first} pour obtenir une \texttt{struct anon\_vma\_chain} à partir du champ \texttt{rb\_root} d'une \texttt{struct anon\_vma} \cite[\texttt{mm/interval\_tree.c}, ligne 93]{noauthor_linux_2018}.
	Cette fonction est définie, comme les autres fonctions de la famille \texttt{\_\_anon\_vma\_interval\_tree\_iter}, par la macro \texttt{INTERVAL\_TREE\_DEFINE} \citelinux{mm/interval\_tree.c}{24}.
	En effet, l'arbre rouge noir d'une \texttt{struct anon\_vma} ne contient pas des \texttt{struct anon\_vma} mais des \texttt{struct anon\_vma\_chain}.
	La \texttt{struct anon\_vma\_chain} correspondante à \texttt{rb\_root} est obtenue grâce à la macro \texttt{rb\_entry} \citelinux{include/linux/interval\_tree\_generic.h}{170}.
	Il est ensuite possible de récupérer la \texttt{vm\_area\_struct} correspondante via le champ \texttt{vma} d'une \texttt{struct anon\_vma\_chain} \citelinux{include/linux/rmap.h}{78}.

	Pour passer à l'élément suivant de l'arbre \texttt{rb\_root} d'une \texttt{struct anon\_vma\_chain}, la macro s'appuie sur la fonction \texttt{anon\_vma\_interval\_tree\_iter\_next} qui appelle la fonction \texttt{\_\_anon\_vma\_interval\_tree\_iter\_next} \citelinux{include/linux/mm.h}{2213}\citelinux{mm/interval\_tree.c}{100}.
	Il est ensuite possible d'obtenir le noeud suivant dans l'arbre rouge noir des \texttt{struct anon\_vma\_chain} via la macro \texttt{rb\_entry} \citelinux{include/linux/interval\_tree\_generic.h}{195}.
} \citelinux{mm/rmap.c}{1822}.

La position d'une page dans une \texttt{vm\_area\_struct} est donnée par le champ \texttt{index} de la \texttt{struct page} \citelinux{include/linux/mm\_types.h}{89}.

La Figure \ref{fig:pages_links} montre les liens entre ces différentes structures \cite[Figure 17-1]{bovet_understanding_2006}.
La \textit{rmap} sera détaillée plus longuement lors de l'étude de l'algorithme de réclamation des pages de Linux.

\subsubsection{Opérations sur la \texttt{vm\_area\_struct}}
Le champ \texttt{vm\_ops} pointe sur une \texttt{struct vm\_operations\_struct} qui contient des pointeurs de fonctions \citelinux{include/linux/mm.h}{393}.
Ce champ peut prendre plusieurs valeurs selon l'utilisation de la \texttt{vm\_area\_struct} :
\begin{itemize}
	\item Si la \texttt{vm\_area\_struct} est utilisée par le code d'un pilote de périphérique, celui-ci lui donnera une valeur qui lui est propre dans la plupart des cas \citelinux{drivers/pci/mmap.c}{66}.
	\item Si elle est utilisée pour projeter en mémoire un fichier, sa valeur sera définie par le système de fichier auquel le fichier appartient\footnote{
		Quand une \texttt{vma\_area\_struct} est créée et qu'elle est utilisée pour projeter un fichier en mémoire, la fonction stockée dans le pointeur de fonction \texttt{mmap} associée au champ \texttt{f\_op} d'une \texttt{struct file} est appelée \cite[\texttt{include/linux/fs.h}, ligne 1811]{noauthor_linux_2018}\citelinux{mm/mmap.c}{1762}.

		Le champ \texttt{f\_op} est initialisé à partir du champ \texttt{i\_fop} d'une \texttt{struct inode} \citelinux{fs/open.c}{752}\citelinux{include/linux/fs.h}{2202}.

		La valeur de ce champ dépend du système de fichier auquel appartiennent l'\texttt{inode} et le fichier \citelinux{fs/btrfs/inode.c}{6545}.
		La fonction stockée dans le pointeur de fonction \texttt{mmap} sera donc dépendante du système de fichier, pour \texttt{btrfs}, par exemple, ce sera la fonction \texttt{btrfs\_file\_mmap} qui y sera stockée \citelinux{fs/btrfs/file.c}{3263}.
		Cette fonction changera la valeur du champ \texttt{vm\_ops} d'une \texttt{vma\_area\_struct} \citelinux{fs/btrfs/file.c}{2189}.
	}.
	\item Si elle est utilisée pour de la mémoire anonyme alors ce champ vaudra \texttt{NULL} \citelinux{mm/mmap.c}{1782}\citelinux{include/linux/mm.h}{465}.
\end{itemize}

\subsubsection{Déplacement dans un fichier}
Si la \texttt{vma\_area\_struct} est associée à un fichier, suite, par exemple, à un appel à \texttt{mmap} avec comme argument \texttt{fd} un descripteur de fichier ouvert; le champ \texttt{vm\_pgoff} indiquera le déplacement dans le fichier.
Comme indiqué par le commentaire suivant, ce champ contient, pour de la mémoire anonyme, l'adresse de la page de début de cette structure, \citelinux{mm/mmap.c}{3132} :
\begin{quote}
	By setting it to reflect the virtual start address of the vma, merges and splits can happen in a seamless way, just using the existing file pgoff checks and manipulations.
\end{quote}
Dans ce cas, sa valeur est calquée sur celle de \texttt{vm\_start}.

\subsubsection{Fichier projeté en mémoire}
Le champ \texttt{vm\_file} aura une valeur significative si et seulement si la \texttt{vma\_area\_struct} est associée à un fichier.
Dans ce cas, il pointera sur la \texttt{struct file} contenant les informations relatives au fichier projeté en mémoire comme le mode d'ouverture de celui-ci \citelinux{include/linux/fs.h}{899}.

\subsection{Les drapeaux associés aux \texttt{vm\_area\_struct}}
Nous décrirons dans cette section les différents drapeaux pouvant être associés à une \texttt{vm\_area\_struct}.
Ces drapeaux seront stockés dans le champ \texttt{vm\_page\_prot} ou \texttt{vm\_flags}, le code \ref{lst:vm_flags} liste ceux-ci.

\begin{code}
	\begin{minted}[autogobble, tabsize = 2, breaklines, linenos, firstnumber = 169, bgcolor = nativebg]{C}
		/*
			* vm_flags in vm_area_struct, see mm_types.h.
			* When changing, update also include/trace/events/mmflags.h
			*/
		#define VM_NONE		0x00000000

		#define VM_READ		0x00000001	/* currently active flags */
		#define VM_WRITE	0x00000002
		#define VM_EXEC		0x00000004
		#define VM_SHARED	0x00000008

		/* mprotect() hardcodes VM_MAYREAD >> 4 == VM_READ, and so for r/w/x bits. */
		#define VM_MAYREAD	0x00000010	/* limits for mprotect() etc */
		#define VM_MAYWRITE	0x00000020
		#define VM_MAYEXEC	0x00000040
		#define VM_MAYSHARE	0x00000080

		#define VM_GROWSDOWN	0x00000100	/* general info on the segment */
		#define VM_UFFD_MISSING	0x00000200	/* missing pages tracking */
		#define VM_PFNMAP	0x00000400	/* Page-ranges managed without "struct page", just pure PFN */
		#define VM_DENYWRITE	0x00000800	/* ETXTBSY on write attempts.. */
		#define VM_UFFD_WP	0x00001000	/* wrprotect pages tracking */

		#define VM_LOCKED	0x00002000
		#define VM_IO           0x00004000	/* Memory mapped I/O or similar */

		/* Used by sys_madvise() */
		#define VM_SEQ_READ	0x00008000	/* App will access data sequentially */
		#define VM_RAND_READ	0x00010000	/* App will not benefit from clustered reads */

		#define VM_DONTCOPY	0x00020000      /* Do not copy this vma on fork */
		#define VM_DONTEXPAND	0x00040000	/* Cannot expand with mremap() */
		#define VM_LOCKONFAULT	0x00080000	/* Lock the pages covered when they are faulted in */
		#define VM_ACCOUNT	0x00100000	/* Is a VM accounted object */
		#define VM_NORESERVE	0x00200000	/* should the VM suppress accounting */
		#define VM_HUGETLB	0x00400000	/* Huge TLB Page VM */
		#define VM_SYNC		0x00800000	/* Synchronous page faults */
		#define VM_ARCH_1	0x01000000	/* Architecture-specific flag */
		#define VM_WIPEONFORK	0x02000000	/* Wipe VMA contents in child. */
		#define VM_DONTDUMP	0x04000000	/* Do not include in the core dump */

		#ifdef CONFIG_MEM_SOFT_DIRTY
		# define VM_SOFTDIRTY	0x08000000	/* Not soft dirty clean area */
		#else
		# define VM_SOFTDIRTY	0
		#endif

		#define VM_MIXEDMAP	0x10000000	/* Can contain "struct page" and pure PFN pages */
		#define VM_HUGEPAGE	0x20000000	/* MADV_HUGEPAGE marked this vma */
		#define VM_NOHUGEPAGE	0x40000000	/* MADV_NOHUGEPAGE marked this vma */
		#define VM_MERGEABLE	0x80000000	/* KSM may merge identical pages */

		#ifdef CONFIG_ARCH_USES_HIGH_VMA_FLAGS
		#define VM_HIGH_ARCH_BIT_0	32	/* bit only usable on 64-bit architectures */
		#define VM_HIGH_ARCH_BIT_1	33	/* bit only usable on 64-bit architectures */
		#define VM_HIGH_ARCH_BIT_2	34	/* bit only usable on 64-bit architectures */
		#define VM_HIGH_ARCH_BIT_3	35	/* bit only usable on 64-bit architectures */
		#define VM_HIGH_ARCH_BIT_4	36	/* bit only usable on 64-bit architectures */
	\end{minted}

	\caption[Les différents drapeaux qui peuvent être stockés dans le champ \texttt{vm\_flags} d'une \texttt{vm\_area\_struct page} ]{Les différents drapeaux qui peuvent être stockés dans le champ \texttt{vm\_flags} d'une \texttt{vm\_area\_struct page} \citelinux{include/linux/mm.h}{173}}
	\label{lst:vm_flags}
\end{code}

\subsubsection{Les drapeaux de protection des pages}
Ces drapeaux sont stockés dans le champ \texttt{vm\_page\_prot}.
Ils sont mis à 1 pour les entrées de table des pages appartenant à cette aire\footnote{
	Puisque toutes les pages d'une \texttt{vm\_area\_struct} auront les mêmes drapeaux, une \texttt{vm\_area\_struct} ne peut, par exemple, pas contenir, à la fois, des pages en lecture seule et des pages en lecture/écriture.
}.
Nous avons décrit ces drapeaux dans le Tableau \ref{table:pte_flags}.

\subsubsection{Drapeaux de protection}
Ces drapeaux ont une correspondance quasi directe avec certains drapeaux d'une entrée de table de pages.
Les différents drapeaux de protection sont détaillés dans le Tableau \ref{table:vm_flags_protect}.
Une correspondance est donnée entre ces drapeaux et les valeurs possibles pour l'argument \texttt{prot} de l'appel système \texttt{mmap} \cite{noauthor_mmap2_nodate}.

La fonction noyau \texttt{calc\_vm\_prot\_bits} traduira les drapeaux de protection de \texttt{mmap} en ces drapeaux \citelinux{include/linux/mman.h}{117}.

\begin{table}
	\centering

	\caption{Les drapeaux de protection d'une \texttt{vm\_area\_struct}}
	\label{table:vm_flags_protect}

	\begin{tabular}{ccp{.8\textwidth}}
		\hline
		Nom & Correspondance & Description\\
		\hline
		\texttt{VM\_NONE} & \texttt{PROT\_NONE} & Les pages de cette zone ne peuvent être accédées.
		Néanmoins ce drapeau ne semble pas beaucoup utilisé dans les sources de Linux.\\
		\texttt{VM\_READ} & \texttt{PROT\_READ} & Ce drapeau indique que les pages appartenant à la \texttt{vm\_area\_struct} peuvent être lues.\\
		\texttt{VM\_WRITE} & \texttt{PROT\_WRITE} & Ce drapeau est le penchant permettant l'écriture du drapeau \texttt{VM\_READ}.\\
		\texttt{VM\_EXEC} & \texttt{PROT\_EXEC} & Si ce drapeau appartient aux champs \texttt{vm\_flags}, les pages de la \texttt{vm\_area\_struct} pourront être exécutées, c'est-à-dire qu'elles n'auront pas leur drapeau \textit{no execute} mis à 1.\\
		\hline
	\end{tabular}
\end{table}

\subsubsection{Drapeaux potentiels}
Les drapeaux \texttt{VM\_MAYREAD}, \texttt{VM\_MAYWRITE}, \texttt{VM\_MAYEXEC} et \texttt{VM\_MAYSHARE} indiquent respectivement que la \texttt{vma\_area\_struct} pourra être lue, écrite, exécutée ou partagée.
Ces drapeaux semblent être positionnés pour toute \texttt{vma\_area\_struct} nouvellement créée \citelinux{mm/mmap.c}{1427}.
Ces drapeaux seront ensuite utilisés dans la fonction noyau \texttt{do\_mprotect\_pkey} qui est appelée par l'appel système \texttt{mprotect} pour vérifier que les nouvelles protections à appliquer peuvent l'être pour cette \texttt{vm\_area\_struct} \cite[\texttt{mm/mprotect.c}, ligne 543]{noauthor_linux_2018}.

\subsubsection{Drapeaux utilisés par \texttt{mmap}}
Ces drapeaux correspondent à des options de l'appel système \texttt{mmap} \cite{noauthor_mmap2_nodate}.
Ils sont décrits dans le Tableau \ref{table:vm_flags_mmap}.

La fonction noyau \texttt{calc\_vm\_flag\_bits} assurera la correspondance pour les drapeaux \texttt{MAP\_GROWSDOWN}, \texttt{MAP\_DENYWRITE}, \texttt{MAP\_LOCKED} et \texttt{MAP\_SYNC} \citelinux{include/linux/mman.h}{129}.

\begin{longtable}{ccp{.8\textwidth}}
	\caption[Les drapeaux d'une \texttt{vm\_area\_struct} venant de l'appel système \texttt{mmap} ]{Les drapeaux d'une \texttt{vm\_area\_struct} venant de l'appel système \texttt{mmap} \cite{noauthor_mmap2_nodate}\label{table:vm_flags_mmap}}\\

	\hline
	Nom & Correspondance & Description\\
	\hline
	\texttt{VM\_SHARED} & \texttt{MAP\_SHARED} & Ce drapeau indique que les pages de la \texttt{vm\_area\_struct} sont accessibles depuis différents espaces d'adressage \cite[Chapter 12]{gorman_understanding_2004}.
	Un appel à l'appel système \texttt{mmap()} avec l'argument \texttt{flags} contenant le drapeau \texttt{MAP\_SHARED} aura cet effet \citelinux{mm/mmap.c}{1446}.
	Notez que le fait d'attacher un segment de mémoire partagée à un espace d'adressage via l'appel système \texttt{shmat} conduit à l'appel de la fonction noyau \texttt{do\_mmap}.\\
	\texttt{VM\_GROWSDOWN} & \texttt{MAP\_GROWSDOWN} & Ce drapeau est principalement utilisé pour représenter la pile d'un processus puisque celle-ci croît vers le bas \citelinux{mm/mmap.c}{2502}.\\
	\texttt{VM\_DENYWRITE} & \texttt{MAP\_DENYWRITE} & Ce drapeau ne semble utilisé que pour un appel à \texttt{mmap} visant à projeter en mémoire un fichier \citelinux{mm/mmap.c}{1744}.
	Un appel à \texttt{mmap} avec ce drapeau renverra le code d'erreur \texttt{ETXTBSY} si le compteur d'écriture du fichier tombe à 0 \citelinux{include/linux/fs.h}{2798}.\\
	\texttt{VM\_LOCKED} & \texttt{MAP\_LOCKED} & Ce drapeau peut être mis à 1 par l'appel système \texttt{mmap} et est toujours mis à 1 par l'appel système \texttt{mlock}.
	Comme l'indique le commentaire suivant présent dans le code du noyau Linux, les pages d'une \texttt{vm\_area\_struct} dont ce drapeau sera mis à 1 ne pourront pas être réclamées lors d'une pression mémoire \citelinux{mm/mlock.c}{44}\cite{noauthor_mlock2_nodate, noauthor_unevictable_nodate} :
	\begin{quote}
		An mlocked page [PageMlocked(page)] is unevictable.
	\end{quote}
	De plus, avec ce drapeau, les pages physiques de la \texttt{vm\_area\_struct} seront allouées immédiatement \citelinux{mm/mmap.c}{1539}\cite{noauthor_mmap2_nodate}.
	Ce comportement est contraire au comportement par défaut de Linux qui alloue les pages de manière paresseuse : les pages physiques ne sont allouées que lorsqu'elles sont touchées (l'allocation paresseuse sera détaillée ultérieurement).\\
	\texttt{VM\_HUGETLB} & \texttt{MAP\_HUGETLB} & Pour utiliser ce drapeau, il faut qu'un système de fichier virtuel de type \texttt{hugetlbfs} soit monté sur la machine \cite{the_kernel_development_community_hugetlb_nodate}.
	Avec ce drapeau, les pages de la \texttt{vm\_area\_struct} seront des \textit{huge pages} \cite{noauthor_mmap2_nodate}.
	Il est possible de spécifier la taille des \textit{huge pages} à utiliser en combinant ce drapeau avec les drapeaux \texttt{MAP\_HUGE\_2MB} et \texttt{MAP\_HUGE\_1GB}.\\
	\texttt{VM\_SYNC} & \texttt{MAP\_SYNC} & Ce drapeau garantit qu'en cas de plantage, une écriture vers un fichier qui aura été projété en mémoire via \texttt{mmap} sera visible au déplacement indiqué après le plantage \cite{corbet_two_2017, noauthor_mmap2_nodate}.\\
	\texttt{VM\_NORESERVE} & \texttt{MAP\_NORESERVE} & De base, lors d'un appel à \texttt{mmap}, Linux va vérifier qu'il a suffisamment de mémoire physique pour satisfaire la requête, même s'il n'allouera pas cette mémoire puisqu'il pratique l'allocation paresseuse \citelinux{mm/mmap.c}{1712}.
	Ainsi, par défaut, un appel à \texttt{mmap} avec pour argument \texttt{size} une valeur supérieure à la mémoire physique dont dispose la machine aboutira à l'erreur "\textit{Cannot allocate memory}".
	Ce drapeau aura donc l'effet inverse : Linux ne vérifiera pas la quantité de mémoire physique disponible avant de créer la projection.
	Dans ce cas, le précédent appel à \texttt{mmap} réussira.\\
	\hline
\end{longtable}

La fonction noyau \texttt{calc\_vm\_flag\_bits} assurera la correspondance pour les drapeaux \texttt{MAP\_GROWSDOWN}, \texttt{MAP\_DENYWRITE}, \texttt{MAP\_LOCKED} et \texttt{MAP\_SYNC} \citelinux{include/linux/mman.h}{129}.

\subsubsection{Drapeaux utilisés par \texttt{madvise}}
Ces drapeaux correspondent à des options de l'appel système \texttt{madvise} \cite{noauthor_madvise2_nodate}.
Ils sont décrits dans le Tableau \ref{table:vm_flags_madvise}.

\begin{longtable}{ccp{.8\textwidth}}
	\caption[Les drapeaux d'une \texttt{vm\_area\_struct} venant d'un appel à \texttt{madvise}]{Les drapeaux d'une \texttt{vm\_area\_struct} venant d'un appel à \texttt{madvise} \cite{noauthor_madvise2_nodate}\label{table:vm_flags_madvise}}\\

	\hline
	Nom & Correspondance & Description\\
	\hline
	\texttt{VM\_SEQ\_READ} & \texttt{MADV\_SEQUENTIAL} & Avec ce drapeau positionné, un défaut du cache fichier, pour un fichier projeté en mémoire et donc associé à une \texttt{vm\_area\_struct}, pourra ramener en mémoire plus de pages que la page fautive \citelinux{mm/filemap.c}{2421}.
	Le commentaire de la fonction \texttt{page\_cache\_sync\_readahead}, est sans équivoque :
	\begin{quote}
		page\_cache\_sync\_readahead() should be called when a cache miss happened: it will submit the read.  The readahead logic may decide to piggyback more pages onto the read request if access patterns suggest it will improve performance.
	\end{quote}\\
	\texttt{VM\_RAND\_READ} & \texttt{MADV\_RANDOM} & Ce drapeau a l'effet inverse du précédent \citelinux{mm/madvise.c}{72}.
	Lors d'un défaut de page d'une \texttt{vm\_area\_struct} avec ce drapeau, la fonction \texttt{page\_cache\_sync\_readahead} ne sera donc pas appelée \citelinux{mm/filemap.c}{2416}.
	En effet, la lecture en avance n'apporte aucun gain pour des accès aléatoires puisqu'aucune hypothèse ne peut être faite sur ces accès.\\
	\texttt{VM\_DONTCOPY} & \texttt{MADV\_DONTFORK} & Les pages d'une \texttt{vm\_area\_struct} marquée de ce drapeau ne seront pas transmises au fils lors d'un \texttt{fork} \citelinux{kernel/fork.c}{464}.\\
	\texttt{VM\_WIPEONFORK} & \texttt{MADV\_WIPEONFORK} & Contrairement au drapeau \texttt{VM\_DONTCOPY}, une \texttt{vm\_area\_struct} marquée de ce drapeau sera copiée au fils.
	Mais le champ \texttt{anon\_vma} d'une \texttt{vm\_area\_struct} sera réinitialisée \citelinux{kernel/fork.c}{494}.
	Ainsi, après un \texttt{fork}, si le fils accède aux pages d'une \texttt{vm\_area\_struct} avec ce drapeau il ne lira que des 0, l'accès est donc autorisé et ne génèrera pas d'erreur de segmentation, et non les données du père \cite{riel_mmforksecurity_2017}.\\
	\texttt{VM\_DONTDUMP} & \texttt{MADV\_DONTDUMP} & Les pages d'une \texttt{vm\_area\_struct} marquée de ce drapeau ne seront pas listées dans le \textit{core dump} du processus \citelinux{fs/binfmt\_elf.c}{1329}.
	Un \textit{core dump} est un instantané de la mémoire d'un processus suite à la réception d'une erreur \cite{noauthor_core5_nodate}.\\
	\texttt{VM\_HUGEPAGE} & \texttt{MADV\_HUGEPAGE} & Ce drapeau activera le mécanisme des \textit{transparent huge pages} pour cette \texttt{vm\_area\_struct}. \cite{the_kernel_development_community_transparent_nodate}.
	Ce mécanisme s'appuie sur le démon \texttt{khugepaged} qui scanne périodiquement la mémoire afin de regrouper des pages classiques dans une \textit{huge page}.
	Le démon peut scanner toute la mémoire ou seulement les régions marquées de ce drapeau.\\
	\texttt{VM\_NOHUGEPAGE} & \texttt{MADV\_NOHUGEPAGE} & Ce drapeau est l'inverse du drapeau \texttt{VM\_HUGEPAGE}.\\
	\texttt{VM\_MERGEABLE} & \texttt{MADV\_MERGEABLE} & Le noyau Linux propose la fonctionnalité \textit{kernel samepage merging} \cite{eidus_kernel_2009}.
	Ainsi, des pages aux contenus identiques pourront être fusionnées pour économiser des pages physiques.
	C'est le démon \texttt{ksmd} qui scanne la mémoire à la recherche de telles pages.
	Les pages d'une \texttt{vm\_area\_struct} marquée de ce drapeau pourront donc être fusionnées par celui-ci.
	Ce mécanisme a été, à la base, développé pour les machines virtuelles.
	En effet, il est possible que, sur une même machine physique, plusieurs machines virtuelles exécutent la même tâche, le code de celle-ci peut donc être partagé entre les différentes machines virtuelles \cite{corbet_devksm_2008}.\\
	\hline
\end{longtable}

\subsubsection{Drapeaux de protection étendue}
Le champ \texttt{vm\_flags} est de type \texttt{unsigned long}\footnote{
	La taille d'un \texttt{long} indique la taille de l'architecture en octet. Ainsi, dans un code C, le code \texttt{sizeof(unsigned long)} renverra \SI{8}{\byte} soit 64 bits.
}.
Or, les valeurs des drapeaux précédemment décrits sont sur 32 bits.
Sur une architecture 64 bits, les bits de poids fort de ce champ sont donc inutilisés.

Ainsi, les drapeaux \texttt{VM\_HIGH\_ARCH\_BIT\_0} à \texttt{VM\_HIGH\_ARCH\_BIT\_4}, correspondant aux bits de poids forts 32 à 36, peuvent être utilisés librement.
Pour un processeur Intel 64 bits, les bits \texttt{VM\_HIGH\_ARCH\_BIT\_0} à \texttt{VM\_HIGH\_ARCH\_BIT\_3}\footnote{
	Le bit \texttt{VM\_HIGH\_ARCH\_BIT\_4} est uniquement utilisé par l'architecture PowerPC 64 bits \citelinux{include/linux/mm.h}{240}.
} prendront les mêmes valeurs que les \textit{protection keys} d'une entrée de la table de page.

\subsubsection{Les autres drapeaux}
Ces drapeaux n'entrent dans aucune des catégories listées, ils sont décrits dans le Tableau \ref{table:vm_flags_others}.

\begin{longtable}{ccp{.5\textwidth}}
	\caption{Les autres drapeaux d'une \texttt{vm\_area\_struct}\label{table:vm_flags_others}}\\

	\hline
	Nom & Correspondance & Description\\
	\hline
	\texttt{VM\_UFFD\_MISSING} & \texttt{UFFDIO\_REGISTER\_MODE\_MISSING} & Ce drapeau est lié à l'appel système \texttt{userfaultfd} \cite{corbet_user_space_2015, noauthor_userfaultfd2_nodate}.
	Cet appel système permet la gestion de défaut de page en mode utilisateur.
	Ceci permet de migrer des machines virtuelles d'une machine hôte à une autre sans copier la mémoire.
	Après la migration, lorsqu'une application de la machine virtuelle essayera d'accéder à sa mémoire ceci provoquera un défaut de page.

	L'hyperviseur pourra, grâce à \texttt{userfaultfd}, aller récupérer la mémoire en question sur l'ancienne machine hôte.
	Concernant ce drapeau, il est mis à 1 pour une \texttt{vm\_area\_struct} quand un appel à \texttt{ioctl\_userfaultfd} a indiqué vouloir gérer en espace utilisateur les défauts de page de cette \texttt{vm\_area\_struct} grâce au drapeau \texttt{UFFDIO\_REGISTER\_MODE\_MISSING} correspondant \cite{noauthor_ioctl_userfaultfd2_nodate}.\\
	\texttt{VM\_UFFD\_WP} & \texttt{UFFDIO\_REGISTER\_MODE\_WP} & Ce drapeau est l'extension du drapeau précédent pour les pages protégées en écriture.
	Il n'est que très rarement mentionné seul dans les sources du noyau Linux.
	De plus, la page de manuel de l'appel système \texttt{ioctl\_userfaultfd} indique que le drapeau \texttt{UFFDIO\_REGISTER\_MODE\_WP} correspondant n'est pas supporté \cite{noauthor_ioctl_userfaultfd2_nodate}.\\
	\texttt{VM\_PFNMAP} & & Comme l'indique le commentaire suivant trouvé dans les sources du noyau Linux, les pages d'une \texttt{vm\_area\_struct} marquée de ce drapeau ne seront pas associées à une \texttt{struct page} \citelinux{mm/memory.c}{2094} :
	\begin{quote}
		VM\_PFNMAP tells the core MM that the base pages are just raw PFN mappings, and do not have a "struct page" associated with them.
	\end{quote}\\
	\texttt{VM\_IO} & & Ce drapeau est beaucoup utilisé dans le code des pilotes de périphérique du noyau Linux et le commentaire suivant va dans ce sens \citelinux{mm/memory.c}{2092} :
	\begin{quote}
		VM\_IO tells people not to look at these pages (accesses can have side effects).
	\end{quote}\\
	\texttt{VM\_DONTEXPAND} & & Une \texttt{vm\_area\_struct} marquée de ce drapeau ne pourra pas être fusionnée avec une autre ou sa taille ne pourra pas être modifiée à la hausse \citelinux{mm/memory.c}{2097} :
	\begin{quote}
		VM\_DONTEXPAND Disable vma merging and expanding with mremap().
	\end{quote}
	Comme le drapeau précédent, celui-ci est aussi largement utilisé dans les pilotes de périphérique.\\
	\texttt{VM\_LOCKONFAULT} & \texttt{MLOCK\_ONFAULT} & Une \texttt{vm\_area\_struct} sera marquée avec ce drapeau lors d'un appel à l'appel système \texttt{mlock2} avec l'argument \texttt{flags} valant \texttt{MLOCK\_ONFAULT} \cite{noauthor_mlock2_nodate}.
	Ce drapeau a un effet similaire au drapeau \texttt{MAP\_LOCKED} dans le sens où les pages de la \texttt{vm\_area\_struct} ne pourront pas être réclamées lorsqu'il y a pression mémoire.
	Par contre, et contrairement à ce dernier, les pages physiques ne seront pas allouées directement \cite[\texttt{mm/gup.c}, ligne 1201]{noauthor_linux_2018}.
	Lors d'un défaut de page, la page sera immédiatement verrouillée et ne pourra donc pas être réclamée.\\
	\texttt{VM\_ACCOUNT} & & Ce drapeau a l'effet inverse du drapeau \texttt{VM\_NORESERVE}.
	Ainsi, lors de l'ajout d'une \texttt{vm\_area\_struct} avec ce drapeau, le noyau vérifiera qu'il a assez de mémoire physique disponible \citelinux{mm/mmap.c}{1712}.
	Une \texttt{vm\_area\_struct} qui a ce drapeau est forcément une \texttt{vm\_area\_struct} privée dont les pages peuvent être écrites \cite[\texttt{mm/mmap.c}, ligne 1662]{noauthor_linux_2018}.\\
	\texttt{VM\_ARCH\_1} & \texttt{VM\_PAT} & Ce drapeau est laissé libre pour implémenter une fonctionnalité de l'architecture.
	Pour l'architecture x86, ce drapeau est redéfini comme étant le drapeau \texttt{VM\_PAT} \citelinux{include/linux/mm.h}{248}.\\
	\texttt{VM\_SOFTDIRTY} & \texttt{\_PAGE\_BIT\_SOFT\_DIRTY} & Comme indiqué par le commentaire suivant, une \texttt{vm\_area\_struct} nouvellement créé voit toujours son drapeau \texttt{VM\_SOFTDIRTY} positionné \citelinux{mm/mmap.c}{1811} :
	\begin{quote}
		New (or expanded) vma always get soft dirty status
	\end{quote}
	Ce drapeau a une correspondance avec le drapeau \texttt{\_PAGE\_BIT\_SOFT\_DIRTY} d'une entrée de table des pages.
	Il est donc utilisé pour traquer les tâches qui ont écrit la page marquée \cite{emelyanov_soft_dirty_2013}.\\
	\texttt{VM\_MIXEDMAP} & & Ce drapeau indique que la \texttt{vm\_area\_struct} contient à la fois des pages qui sont associées à une \texttt{struct page} et des pages qui n'ont pas de telle structure.
	Ce drapeau est majoritairement placé par des pilotes de périphériques.\\
	\hline
\end{longtable}