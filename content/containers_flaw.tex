\chapter{De la gestion mémoire problématique des conteneurs : l'isolation sans la consolidation}
\label{chapter:container_flaw}
\section{Introduction}
\label{sec:container_flaw_intro}
Dans le Chapitre \ref{chapter:memory}, nous avons montré que les conteneurs s'appuient sur les \texttt{namespaces} et les \texttt{cgroups}.
Les premiers offrent une isolation orientée sécurité, par exemple, une tâche extérieure à un \texttt{namespace} ne peut envoyer de signaux à une tâche appartenant à ce \texttt{namespace}.
Les \texttt{cgroups} sont, quant à eux, utilisés pour limiter l'utilisation des ressources d'un conteneur, assurant ainsi une isolation de performance.
Ces deux fonctionnalités sont offertes par le système d'exploitation, qui dispose d'une vision globale des ressources, la question de la garantie de la consolidation se pose alors.

Nous présentons donc, dans ce chapitre, comment le \texttt{cgroup} mémoire effectue cette limitation.
Nous montrons ensuite, par l'expérience, que ces mécanismes ne permettent pas de garantir en même temps l'isolation et la consolidation mémoire \cite{laniel_highlighting_2019, thesis_exps}.

\section{Les mécanismes de limites du \texttt{cgroup} mémoire}
\label{sec:limits}
Ce \texttt{cgroup} permet de contrôler l'empreinte mémoire d'un conteneur, c'est-à-dire la somme de la mémoire anonyme allouée et de l'ensemble des pages contenues dans le \textit{page cache}.
En pratique, ce contrôle opère sur les pages de la seconde catégorie.
En effet, et comme nous l'avons discuté dans la Section \ref{subsec:memory_reclaim}, la réclamation de la mémoire anonyme s'appuie sur un espace d'échange situé sur le disque.
Transférer une page depuis l'espace d'échange vers la mémoire est coûteux en temps et a donc un impact sur les performances des applications \cite{zaitsev_impact_2017}.

Pour contrôler la taille de l'empreinte mémoire des tâches qu'il contient, le \texttt{cgroup} mémoire s'appuie sur plusieurs limites.
Ces limites peuvent être renseignées au lancement d'un conteneur ou lors de son exécution.
Si elles ne sont pas renseignées, leurs valeurs sont équivalentes à $+\infty$, ce qui revient à ne pas activer le mécanisme \citelinux{mm/memcontrol.c}{4485}.

Lorsqu'une page est chargée dans un \texttt{cgroup}, Linux vérifie que l'allocation respecte les différentes limites mémoires fixées.
Dans le cas contraire, les tâches du \texttt{cgroup} voient leurs mémoires réclamées pour satisfaire cette allocation.

Dans les sous-sections qui viennent, nous détaillons les \texttt{max} et \texttt{soft} limites.
Puis nous expliquons brièvement les nouvelles limites introduites par \texttt{cgroup v2}\footnote{Dans la suite de ce manuscrit, nous n'utilisions pas ces nouvelles limites puisque nous lançons nos conteneurs via \texttt{docker} qui, lors de l'écriture de ces lignes, n'est compatible qu'avec \texttt{cgroup v1} \cite{suda_current_2019}.}.

\subsection{La \texttt{max} limite}
\label{subsec:max_limit}
La \texttt{max} limite indique le nombre maximal d'octets pouvant être dédié aux tâches d'un \texttt{cgroup}.
Cette limite peut être dépassée mais la mémoire du \texttt{cgroup} sera immédiatement réclamée \cite[Section 2.2]{noauthor_memory_nodate}.
Si la réclamation n'a pas abouti, l'\textit{OOM killer} est invoqué pour tuer la tâche ayant la plus grosse empreinte mémoire du \texttt{cgroup} \cite[Section 2.5]{noauthor_memory_nodate}\cite[Section "The Out of Memory Killer"]{bovet_understanding_2006}.

Le fait de pouvoir dépasser cette limite peut sembler étrange.
Néanmoins, cette souplesse permet de réclamer des pages fichiers pour satisfaire une demande en pages anonymes.
Sans celle-ci, un \texttt{cgroup} allouant et touchant beaucoup de mémoire anonyme serait tué tandis qu'ici le noyau le force à se débarrasser de ses pages fichiers pour les utiliser comme des pages anonymes.

\subsection{La \texttt{soft} limite}
\label{subsec:soft_limit}
Cette limite n'empêche pas le conteneur d'utiliser une quantité quelconque de mémoire, tant que les deux conditions suivantes sont remplies \cite[Section 7]{noauthor_memory_nodate} :
\begin{enumerate}
	\item La mémoire du conteneur n'excède pas sa \texttt{max} limite.
	\item Et il n'y a pas de pression mémoire.
\end{enumerate}
C'est seulement quand il y a pression mémoire, c'est-à-dire quand la mémoire disponible devient rare, que la \texttt{soft} limite prend effet.
Si les valeurs des \texttt{soft} limites sont trop hautes, par rapport à la mémoire dont dispose la machine, elles sont rognées.
Ainsi, un conteneur ne peut affamer les autres.
Par exemple, si la machine dispose de \SI{3}{\giga\byte} de mémoire et que deux conteneurs sont créés avec chacun une \texttt{soft} limite fixée à \SI{2}{\giga\byte} Linux ne pourra pas garantir ces valeurs.

La valeur idéale de cette limite est le WS de l'application \cite{denning_working_1967}.
Dans ce cas, l'application conserve de bonnes performances puisque son WS est en mémoire.

\subsection{Les limites de \texttt{cgroup v2}}
\label{subsec:cgroup_v2}
Comme nous l'avons vu dans la Section \ref{subsubsec:cgroup}, la version 2 des \texttt{cgroups} amène son lot de nouveautés.
Pour le \texttt{cgroup} mémoire, les limites suivantes remplacent les limites susnommées \cite{heo_control_2015} :
\begin{description}
	\item[min] : Un \texttt{cgroup} ayant une empreinte mémoire inférieure à cette valeur ne sera pas réclamé.
	Néanmoins, si Linux n'arrive pas à réclamer de la mémoire ailleurs il peut invoquer l'\textit{OOM killer} sur ce \texttt{cgroup} afin de libérer de la mémoire.
	\item[low] : Cette limite correspond à la \texttt{soft} limite de \texttt{cgroup v1}.
	\item[high] : Les tâches appartenant à un \texttt{cgroup} dont l'empreinte mémoire dépasse cette valeur subiront une forte réclamation mémoire.
	\item[max] : Si l'empreinte mémoire du \texttt{cgroup} dépasse cette valeur et ne peut être réduite, l'\textit{OOM killer} est invoqué.
\end{description}

Dans les faits, cette nouvelle mouture propose un renommage des deux limites existantes et introduit une nouvelle limite garantissant une empreinte mémoire minimale.
De plus, les bases théoriques de ces limites sont conservées tout comme leur caractère statique.
Nous allons, dans la suite de ce chapitre, démonter que ce dernier point est un problème.

\section{Environnement d'expérimentation}
\label{sec:experiment_environment}
Pour étudier l'efficacité des mécanismes de contrôle qu'offre le \texttt{cgroup} mémoire nous avons réalisé une expérience figurant une base de données \texttt{mysql} accédée par le \textit{benchmark} \texttt{sysbench} \cite{noauthor_mysql_nodate, alexey_kopytov_sysbench_nodate}.
Nous renseignons d'abord les métriques auxquelles nous nous intéressons pour juger les mécanismes utilisés puis les caractéristiques matérielles et logicielles de l'environnement d'expérimentation.

\subsection{Métriques}
\label{subsec:metrics}
Dans nos expériences, nous souhaitons mesurer l'isolation ainsi que son impact sur les performances.
Nous voulons aussi observer si la consolidation a lieu et établir une corrélation entre l'empreinte mémoire et les performances d'une application.
En effet, si un conteneur a peu de mémoire ceci n'implique pas qu'il n'ait pas de bonnes performances si son WS est petit \cite{denning_working_1967}.

Ainsi, pour mesurer l'isolation et la consolidation nous nous intéressons aux métriques système suivantes :
\begin{description}
	\item[l'empreinte mémoire :] L'empreinte mémoire d'un conteneur correspond à la quantité de mémoire qu'il utilise pour un instant donné.
	Les données de la base de données qui auront déjà été accédées seront stockées dans la mémoire du conteneur, plus particulièrement dans le \textit{page cache}.
	\item[le nombre de lectures depuis le disque :] Ce nombre représente les lectures effectuées par un conteneur pour un instant donné.
	Idéalement, ce nombre doit être le plus petit possible puisqu’une lecture vers le disque est lente et a un fort impact sur les performances du conteneur.
\end{description}

Pour mesurer les performances de l'application, nous observons son débit.
Il s'agit du nombre de transactions que le \textit{benchmark} est capable de traiter en une seconde.
Une transaction est un ensemble de requêtes adressées à une base de données.
Plus le débit est élevé, plus le conteneur a de bonnes performances.

Le débit est obtenu depuis la sortie standard du \textit{benchmark} tandis que les empreintes mémoires et lectures depuis le disque sont collectées par \texttt{docker}.

\subsection{Environnement matériel}
\label{subsec:hardware_environment}
Nos premières expériences ont été lancées sur une station de travail Dell T3600 \cite{dell_dell_2012}.
Cette machine possède un processeur Intel\textregistered Xeon\textregistered E5-1603 épaulé par \SI{8}{\giga\byte} de mémoire DDR3 et un SSD \cite{intel_intel_2012}.

Dans un second temps, nous avons mené nos expériences sur des machines appartenant au banc de test Grid'5000 \cite{grid5000, thesis_g5kscripts}.
Elles sont équipées de deux processeurs Intel\textregistered Xeon\textregistered Gold 6130 cadencés à \SI{2.1}{\giga\hertz} \cite{intel_intel_2017}.
Les processeurs sont accompagnés de \SI{192}{\giga\byte} de mémoire DDR4 et un SSD officie pour le stockage.

Les conclusions de ces deux séries d'expériences étant les mêmes, nous avons choisi de ne détailler ici que les chiffres obtenus suite au lancement de nos expériences sur la machine appartenant à Grid'5000.
De plus, ces machines ont une architecture typée serveur convenant à une utilisation dans le \textit{cloud}.

\subsection{Environnement logiciel}
\label{subsec:software_environment}
Pour créer, artificiellement, de la pression mémoire afin que certaines limites s'activent, nous limitons la taille de la mémoire.
Notre expérience est, pour cela, lancée dans une VM à laquelle 4 cœurs et \SI{3}{\giga\byte} de mémoire ont été dédiés.
Par ce fait, nous simulons la présence d'autres conteneurs générant de la pression mémoire.
L'interprétation des résultats est aussi plus aisée.
Le comportement de plusieurs conteneurs est toutefois étudié dans la Section \ref{sec:memoplight_eight_containers}.

Notre VM est gérée par l'hyperviseur \texttt{qemu 3.1.0} tandis que la gestion des conteneurs revient à \texttt{docker 19.03.2} et sa bibliothèque \texttt{python} \texttt{docker-py 4.0.2} \cite{qemu_qemu_2019, docker_docker_nodate, noauthor_docker_py_nodate, thesis_scripts}.
Le \textit{benchmark} utilisé est \texttt{sysbench oltp} \cite{alexey_kopytov_sysbench_nodate}.
Nous avons modifié le code de ce dernier afin de faire varier le débit au cours du temps \cite{thesis_sysbench, thesis_images}.
Le \textit{benchmark} interagit avec une base de donnée \texttt{mysql 5.7} \cite{noauthor_mysql_nodate}.
La version du noyau Linux utilisée est la 4.19 \cite{kroah_hartman_linux_2018}.

\section{Expérience de référence}
\label{sec:problem_reference}
Afin de comparer efficacement les différents mécanismes, nous avons besoin de chiffres de référence.
Ces chiffres vont nous permettre de comparer les mécanismes par rapport à des résultats idéaux, c'est-à-dire lorsque l'application s'exécute seule et sans être limitée.
Pour les obtenir, nous avons mené une expérience de référence, ne figurant qu'un seul conteneur exécutant le scénario du conteneur B tel que décrit dans le Tableau \ref{table:two_load}.
La Figure \ref{fig:reference} montre les résultats obtenus pour cette expérience.

\begin{figure}
	% This environnement permits putting figure into the margin.
	% The argument will shrink the margin from the value.
	% This environnement is built-in with KOMA on which classicthesis is based.
	% See:
	% https://tex.stackexchange.com/a/208422
	% https://mirrors.chevalier.io/CTAN/macros/latex/contrib/koma-script/doc/scrguien.pdf page 124
	\begin{addmargin}{-3cm}
		\centering

		\subfloat[Débit]{ % Default behavior.
			\includegraphics[scale = .42]{alone_default_transactions.pdf}

			\label{fig:reference_transactions}
		}
		\subfloat[Empreinte mémoire]{
			\includegraphics[scale = .42]{alone_default_memory.pdf}

			\label{fig:reference_memory}
		}
		\subfloat[Lecture depuis le disque]{
			\includegraphics[scale = .42]{alone_default_reads.pdf}

			\label{fig:reference_disk}
		}

		\caption{Expérience de référence}
		\label{fig:reference}
	\end{addmargin}
\end{figure}

Ainsi, la Figure \ref{fig:reference_transactions} montre que le conteneur gère jusqu'à 2000 transactions par seconde (t/s), nous choisissons donc cette valeur comme référence.
Comme le dépeint la Figure \ref{fig:reference_memory}, l'empreinte mémoire du conteneur est de \SI{2.8}{\giga\byte} et ne varie pas beaucoup au cours du temps.
Le nombre de lectures depuis le disque, visible sur la Figure \ref{fig:reference_disk}, est d'abord élevé au début de l'expérience, puisque le conteneur ramène des informations du disque vers la mémoire.
Il diminue ensuite, puisque le conteneur a ramené les informations qui lui sont utiles en mémoire, pour atteindre une valeur d'environ 100 lectures par seconde.

\section{Scénario d'expérience avec deux conteneurs}
\label{sec:two_containers_experiment}
Après avoir obtenu les chiffres de référence, nous pouvons désormais lancer les expériences visant à étudier les différents mécanismes offerts par le \texttt{cgroup} mémoire.
Nous pourrons, en effet, comparer les résultats de chaque expérience à la référence.
Notre expérience comporte deux conteneurs : A et B.
Chacun exécute une charge de travail, dite \textit{OnLine Transaction Processing} (OLTP), tout en expérimentant, au cours du temps, des variations dans celle-ci.
Chacun de nos conteneurs, A et B, s'exécute avec deux cœurs et accède en lecture à une base de données de \SI{4}{\giga\byte}.

Comme le ferait un administrateur fixant les limites d'un \texttt{cgroup}, nous fixons les SLO des deux conteneurs.
Ainsi, le conteneur A possède un SLO fixé à 1700 t/s tandis que celui de B est défini à 1000 t/s, le conteneur A est donc plus prioritaire que le conteneur B.
Un conteneur est dit satisfait, et ses performances sont dites satisfaisantes, si son SLO est respecté.
Ces nombres sont représentés dans la Figure \ref{fig:transactions} à la Figure \ref{fig:transactions_soft} par des lignes pointillées colorées.
Les figures \ref{fig:default} à \ref{fig:soft} comportent aussi les valeurs de référence, obtenues suite à l'expérience de la Section \ref{sec:problem_reference}, présentées par des lignes noires horizontales discontinues.

Quand les deux conteneurs sont soumis à une forte charge de travail, nous nous attendons à observer l'isolation mémoire.
C'est-à-dire qu'ils auront chacun de la mémoire et qu'aucun n'affamera l'autre.
Dans le cas où un conteneur doit faire face à une forte activité pendant que son voisin reçoit une activité moindre, nous espérons observer de la consolidation mémoire.
Le premier conteneur devrait donc récupérer la mémoire inutilisée du second afin d'obtenir les performances de référence.

Notre expérience est composée de 6 phases différentes, décrites dans le Tableau \ref{table:two_load}.

\begin{table}
	\centering

	\caption{Charge de travail au cours des 6 phases de l'expérience pour les conteneurs A et B}
	\label{table:two_load}

	\begin{tabular}{ccc}
		\hline
		Phases & Container A & Container B\\
		\hline
		$\varphi_1(h, h)$\label{step_1} & forte charge & forte charge\\
		$\varphi_2(h, l)$\label{step_2} & forte charge & faible charge\\
		$\varphi_3(h, i)$\label{step_3} & forte charge & charge intermédiaire\\
		$\varphi_4(l, l)$\label{step_4} & faible charge & faible charge\\
		$\varphi_5(i, h)$\label{step_5} & charge intermédiaire & forte charge\\
		$\varphi_6(s, h)$\label{step_6} & conteneur arrêté & forte charge\\
		\hline
	\end{tabular}
\end{table}

Avant de détailler chacune de ces phases, nous définissons la notation donnée dans la Formule \ref{formula:step} pour faciliter leurs descriptions.
\begin{equation}
	\varphi_{n}(a, b)
	\label{formula:step}
\end{equation}
Dans celle-ci, $n$ correspond au numéro de la phase, allant de 1 à 6, $a$ correspond à la charge reçue par le conteneur A et $b$ à celle reçue par B.
Les valeurs possibles pour $a$ et $b$ sont les suivantes :
\begin{itemize}
	\item $h$ : La charge de travail reçue est forte et correspond au débit de référence.
	\item $l$ : Le conteneur est soumis à une faible activité.
	Nous avons fixé, arbitrairement, cette valeur comme valant 10\% du débit de référence soit 200 t/s.
	Les lignes noires horizontales pointillées présentes dans les figures \ref{fig:transactions} à \ref{fig:transactions_soft} dépeignent cette faible charge.
	\item $i$ : Une charge de travail intermédiaire est envoyée au conteneur.
	Comme pour la faible charge, nous avons décidé de fixer la charge intermédiaire à 1500 t/s, soit 75\% du débit de référence.
	\item $s$ : Le conteneur est arrêté et ne reçoit aucune requête.
\end{itemize}
Chaque phase du scénario dure \SI{180}{\second}.

Dans un système idéal, les comportements suivants sont attendus pour chaque phase.
Pendant la phase \step{1}{h}{h}, la mémoire physique disponible est inférieure à la somme des tailles des bases de données, nous sommes donc dans une situation de pression mémoire.
Le \textit{page cache} est forcé à rétrécir causant, ainsi, une augmentation du nombre de lectures depuis le disque et donc une diminution du débit.
Dans \step{2}{h}{l}, si le \texttt{cgroup} mémoire permet la consolidation, A devrait être à même de prendre de la mémoire à B pour augmenter ses performances.
D'une manière similaire à \step{1}{h}{h}, les performances devraient être faibles dans \step{3}{h}{i}.
Dans \step{4}{l}{l}, les deux conteneurs devraient être capables de répondre à toutes les transactions qui leur sont adressées.
\step{5}{i}{h} est symétrique à \step{3}{h}{i}, comme dans celle-ci, les performances seront faibles.
Enfin, pendant \step{6}{s}{h}, puisque A est arrêté B devrait obtenir les performances de référence.

Nous avons exécuté ce scénario 10 fois et calculé la moyenne et l'écart-type pour chaque métrique, ce pour chaque seconde.
Nos résultats sont présentés dans les Figures \ref{fig:default} à \ref{fig:soft}.

\begin{figure}
	\begin{addmargin}{-3cm}
		\centering

		\subfloat[Débit]{ % Default behavior.
			\includegraphics[scale = .42]{pair_default_transactions.pdf}

			\label{fig:transactions}
		}
		\subfloat[Empreinte mémoire]{
			\includegraphics[scale = .42]{pair_default_memory.pdf}

			\label{fig:memory}
		}
		\subfloat[Lecture depuis le disque]{
			\includegraphics[scale = .42]{pair_default_reads.pdf}

			\label{fig:disk}
		}

		\caption[Aucune limite fixée]{\footnotesize Aucune limite fixée}
		\label{fig:default}

		\subfloat[Débit]{ % Max limit behavior.
			\includegraphics[scale = .42]{pair_max_transactions.pdf}

			\label{fig:transactions_max}
		}
		\subfloat[Empreinte mémoire]{
			\includegraphics[scale = .42]{pair_max_memory.pdf}

			\label{fig:memory_max}
		}
		\subfloat[Lecture depuis le disque]{
			\includegraphics[scale = .42]{pair_max_reads.pdf}

			\label{fig:disk_max}
		}

		\caption[\texttt{Max} limites fixées à \SI{1.8}{\giga\byte} (A) et \SI{1}{\giga\byte} (B)]{\footnotesize \texttt{Max} limites fixées à \SI{1.8}{\giga\byte} (A) et \SI{1}{\giga\byte} (B)}
		\label{fig:max}

		\subfloat[Débit]{ % Soft limit behavior.
			\includegraphics[scale = .42]{pair_soft_transactions.pdf}

			\label{fig:transactions_soft}
		}
		\subfloat[Empreinte mémoire]{
			\includegraphics[scale = .42]{pair_soft_memory.pdf}

			\label{fig:memory_soft}
		}
		\subfloat[Lecture depuis le disque]{
			\includegraphics[scale = .42]{pair_soft_reads.pdf}

			\label{fig:disk_soft}
		}

		\caption[\texttt{Soft} limites fixées à \SI{1.8}{\giga\byte} (A) et \SI{1}{\giga\byte} (B)]{\footnotesize \texttt{Soft} limites fixées à \SI{1.8}{\giga\byte} (A) et \SI{1}{\giga\byte} (B)}
		\label{fig:soft}
	\end{addmargin}
\end{figure}

\section{Deux conteneurs sans limite fixée}
\label{sec:problem_without}
Avec cette expérience nous étudions le comportement des conteneurs sans fixer de limite dans l'optique de respecter les SLO fixés ci-dessus.

La Figure \ref{fig:default} représente les résultats de cette expérience.
Le débit au cours du temps est représenté sur la Figure \ref{fig:transactions} tandis que la Figure \ref{fig:memory} et la Figure \ref{fig:disk} montrent, respectivement, l'évolution des empreintes mémoires et les lectures depuis le disque des conteneurs au cours du temps.

Dans \step{1}{h}{h}, les deux conteneurs reçoivent une forte charge.
La Figure \ref{fig:transactions} montre que les deux conteneurs ont les mêmes performances.
Ceci peut être expliqué par la Figure \ref{fig:memory}, en effet, dans \step{1}{h}{h}, les deux conteneurs ont la même empreinte mémoire.
Puisqu'il n'y a pas de mécanisme indiquant qu'un conteneur doit être plus réclamé que l'autre, ils ont donc les mêmes performances.

Dans \step{2}{h}{l}, le débit du conteneur recevant une forte charge de travail augmente aux dépens de celui soumis à une faible activité.
Cette même phase, dans la Figure \ref{fig:memory}, montre que l'empreinte mémoire du conteneur A augmente tandis que celle de B diminue.
L'augmentation mémoire permet à A d'augmenter la taille de son \textit{page cache} comme dépeint dans la Figure \ref{fig:disk}.
Néanmoins, même si B est soumis à une faible charge, il freine les performances de A, car son empreinte mémoire est toujours conséquente.
Cette phase montre bien qu'il y a consolidation mais que celle-ci est imparfaite.
En effet, le conteneur très actif n'atteint ni les performances de référence ni des performances satisfaisantes.

Dans \step{3}{h}{i} et \step{5}{i}{h}, un conteneur reçoit une charge intermédiaire tandis que son voisin est soumis à une forte charge.
Ces phases sont symétriques et présentent donc les mêmes résultats.
Comme dans \step{1}{h}{h}, il n'y a pas de mécanisme indiquant qu'un conteneur doit être plus réclamé qu'un autre, les deux conteneurs ont donc des performances similaires.
Il nous faut toutefois noter que le conteneur soumis à une charge intermédiaire présente de meilleures performances que l'autre conteneur.
En effet, comme montré dans la Figure \ref{fig:memory}, l'empreinte mémoire du premier est supérieure à celle du second.

Pendant \step{6}{s}{h}, A est complètement arrêté, l'empreinte mémoire de B augmente donc et, par conséquent, il effectue moins de lectures depuis le disque.
Il atteint donc les performances de référence.
L'absence de limite permet, dans ce cas, d'atteindre les performances idéales, car B récupère la mémoire inutilisée par A.

Le lecteur averti aura remarqué que le débit du conteneur B subit un pic entre les phases \step{3}{h}{i} et \step{4}{l}{l}.
Puisque B est soumis à une forte charge dans \step{3}{h}{i}, les transactions qu'il n'a pas pu traiter dans cette phase le sont dans \step{4}{l}{l} puisque B dispose de plus de ressources.
Ce comportement est visible dans les Figures \ref{fig:transactions} à \ref{fig:transactions_soft}.

Ainsi, sans renseigner de limite, il n'y a pas d'isolation mémoire, il n'est donc pas possible d'assurer des performances satisfaisantes aux conteneurs.
Néanmoins, il y a consolidation mémoire, mais celle-ci est imparfaite, car aucun conteneur n'atteint les performances de référence.

\section{Deux conteneurs avec \texttt{max} limites}
\label{sec:problem_max}
Dans la section précédente, nous avons vu que sans limite il n'y a pas d'isolation.
Nous étudions donc la \texttt{max} limite pour régler ce problème.
L'expérience est identique à celle lancée dans la section \ref{sec:problem_without} mais nous avons, cette fois-ci, fixé les \texttt{max} limites des conteneurs à \SI{1.8}{\giga\byte} et \SI{1}{\giga\byte} pour A et B.
Nous avons choisi ces valeurs de sorte que leur somme soit égale à l'empreinte mémoire de référence.

De plus, avec ces valeurs, le conteneur B ne freine pas les performances du conteneur A.
En effet, le conteneur A a de meilleures performances dans les phases \step{1}{h}{h}, \step{3}{h}{i} et \step{5}{i}{h} dans la Figure \ref{fig:transactions_max}, comparée à la Figure \ref{fig:transactions}.
Cette amélioration peut être expliquée en regardant la Figure \ref{fig:memory_max}, celle-ci montre que les empreintes mémoires des conteneurs suivent leurs \texttt{max} limites.
Puisque A a plus de mémoire que son voisin, il effectue moins de lectures depuis le disque, comme montré dans la Figure \ref{fig:disk_max}.

Mais la \texttt{max} limite bloque la consolidation mémoire comme montrée dans la phase \step{6}{s}{h} de la Figure \ref{fig:memory_max}.
Dans cette figure, l'empreinte mémoire de B est freinée par sa \texttt{max} limite.
Il ne peut donc pas atteindre les performances de référence, alors qu'il n'y a qu'un seul conteneur actif, comme le montre la comparaison de la Figure \ref{fig:transactions_max} avec la Figure \ref{fig:transactions}.

De plus, dans \step{2}{h}{l}, la \texttt{max} limite présente le même problème que l'exécution sans limite.
Le conteneur A, recevant une forte charge, ne peut pas réclamer la mémoire du conteneur B qui reçoit une faible charge.
Ce comportement illustre le caractère statique de la \texttt{max} limite.
Celle-ci ne prend pas en compte les variations dans la charge du travail des conteneurs.

La \texttt{max} limite permet l'isolation mémoire, puisque A possède plus de mémoire que B, mais bloque complètement la consolidation mémoire.

\section{Deux conteneurs avec \texttt{soft} limites}
\label{sec:problem_soft}
Comme la \texttt{max} limite, la \texttt{soft} limite bloque la consolidation mémoire.
Pour montrer ce fait, nous lançons une troisième expérience.
Celle-ci est similaire à celle de la section \ref{sec:problem_max} mais nous avons renseigné les \texttt{soft} limites en lieu et place des \texttt{max} limites.

Les résultats de la Figure \ref{fig:soft} sont très similaires à ceux dépeints dans la Figure \ref{fig:max}.
En effet, de \step{1}{h}{h} à \step{5}{i}{h}, notre système est sous pression mémoire.
La mémoire des conteneurs est donc réclamée et leurs empreintes mémoires suivent leurs \texttt{soft} limites, qui ont les mêmes valeurs que les \texttt{max} limites renseignées précédemment.
La \texttt{soft} limite garantit donc l'isolation sous pression mémoire.

Comme les deux autres mécanismes, dans \step{2}{h}{l}, la \texttt{soft} limite ne permet pas à A d'obtenir les performances de référence.

La \texttt{soft} limite diffère de la \texttt{max} limite dans la phase \step{6}{s}{h} de la Figure \ref{fig:memory_soft}.
Contrairement à cette même phase dans la Figure \ref{fig:memory_max}, l'empreinte mémoire de B augmente.
Puisque A est arrêté, il n'y a plus de pression mémoire et la \texttt{soft} limite n'est donc pas activée.
L'empreinte mémoire du conteneur B peut donc augmenter, tout comme ses performances qui atteignent celles de référence.

La \texttt{soft} limite permet donc l'isolation mémoire, le conteneur B a moins de mémoire que son voisin, tant qu'il y a de la pression mémoire.
Dans cette configuration, elle bloque la consolidation mémoire, mais celle-ci devient possible quand il n'y a plus de pression mémoire.

\section{Conclusion}
\label{sec:container_flaw_conclusion}
Dans ce chapitre, nous avons mené des expériences visant à mettre en évidence les problèmes du \texttt{cgroup} mémoire.
En effet, les mécanismes existants du \texttt{cgroup} mémoire ne permettent pas de combiner de façon satisfaisante l'isolation et la consolidation mémoire.

Sans renseigner de limite, la consolidation mémoire prend place, imparfaitement, et il n'y a pas d'isolation mémoire.
La \texttt{max} limite permet l'isolation mais bloque la consolidation.
La \texttt{soft} limite est similaire lors d'une pression mémoire, mais la consolidation devient possible quand la mémoire est en quantité suffisante.
Ces résultats sont résumés dans le Tableau \ref{table:summary}.

\begin{table}
	\centering

	\caption{L'isolation et la consolidation mémoire des différents mécanismes existants sous Linux}
	\label{table:summary}

	\begin{tabular}{lcc}
		\hline
% 		\diagbox{}{Characteristics}
		Mécanisme utilisé & Consolidation mémoire & Isolation mémoire\\
		\hline
		limite non renseignée & faible & non\\
		\texttt{Max} limite & non & oui\\
		\texttt{Soft} limite & non (sous pression mémoire) & oui\\
	\end{tabular}
\end{table}

De plus, les \texttt{max} et \texttt{soft} limites sont des mécanismes statiques.
Elles ne peuvent donc pas s'adapter aux variations de charge de travail, particulièrement quand les applications font face à une faible charge de travail ou sont totalement arrêtées \cite{barroso_datacenter_2013, barroso_case_2007, meisner_powernap_2009, liu_measurement_2011}.

Un mécanisme dynamique nécessiterait de corréler les performances de l'application à l'utilisation de ses ressources.
Baser cette corrélation sur des métriques noyaux, comme la fréquence des défauts de page, les entrées-sorties, le nombre de cycles CPU consommés ou les empreintes mémoires, n'est pas chose aisée.

Dans le chapitre suivant, nous présentons notre contribution, MemOpLight, qui permet une meilleure répartition de la mémoire entre les conteneurs en corrigeant le problème du \texttt{cgroup} mémoire pour offrir à la fois l'isolation et la consolidation.