\chapter{Articles de l'état de l'art}
\label{chapter:state_of_the_art_articles}
Dans ce chapitre sont résumés les différents articles évoqués dans le Chapitre \ref{chapter:state_of_the_art}.

\section{Dupont \textit{et al.}}
\label{sec:dupont}
Dupont et ses collaborateurs constatent qu'à cause de la multitude des paramètres, il est compliqué, pour un humain, de gérer une application dans le \textit{cloud}  \cite{dupont_experimental_2015}.

Les auteurs définissent l'élasticité logicielle comme le remplacement à la volée de briques logicielles.
Par exemple, une brique gourmande en ressources pourra être remplacée par une brique plus légère pour faire face à un pic de charge.
Dans leurs travaux, ils donnent la priorité à cette élasticité plutôt qu'à celle du \textit{cloud}.
En effet, ajouter des ressources coûte de l'argent et démarrer celles-ci n'est pas immédiat.

Les auteurs utilisent une \textit{MAPE-K loop}.
Cette boucle peut engendrer des actions simples comme ajouter/retirer une VM/brique logicielle.
Elle peut aussi mener à des actions plus atypiques comme dégrader le logiciel pendant le démarrage d'une VM, qui vient d'être ajoutée, pour faire face à un pic de charge.
Le logiciel sera remis dans son état d'origine lorsque la VM sera pleinement initialisée.
Par exemple, la qualité d'une vidéo sera réduite puis re-augmentée dès que la VM aura démarré.
Bien entendu, ces actions pourront être apprises pour être réutilisées.
D'après leurs expériences, cette solution permet d'augmenter la réactivité, mais augmente aussi le coût, puisqu'une nouvelle VM est lancée.

L'administrateur du \textit{cloud} pourra définir de telle action.
Il pourra aussi définir ce que les auteurs appellent des tactiques, <<\textit{tactics}>> dans l'article.
Une tactique consister à effectuer une, ou plusieurs, action si un événement particulier se produit.

Pour choisir au mieux la tactique à utiliser, les auteurs proposent des filtres de contraintes et des filtres de préférences, respectivement <<\textit{constraints filter}>> et <<\textit{preferences filter}>> dans le texte original.
Un filtre de contrainte est lié au contexte d'exécution, une contrainte pouvant être, par exemple, la quantité de mémoire physique dont dispose la machine physique.
Tandis qu'un filtre de préférence est purement subjectif, par exemple exécuter la tactique minimisant la latence au détriment du coût.

\section{Kouki \textit{et al.}}
\label{sec:kouki_sla_driven_2012}
Kouki et Ledoux s'intéressent au SaaS, \textit{Software as a Service} comme Office 365 \cite{kouki_sla_driven_2012}.
Les auteurs expliquent ensuite qu'un utilisateur d'un SaaS peut choisir entre plusieurs offres correspondant à différents niveaux de SLA.
Pour eux, un SaaS repose sur un IaaS, \textit{Infrastructure as a Service}\sidenote{La définition du \textit{cloud}, telle que rapportée dans le Chapitre \ref{chapter:introduction}, correspond à celle d'un IaaS.}.
Il y a donc deux niveaux de SLA :
\begin{enumerate}
	\item Un entre l'utilisateur et le SaaS.
	\item Et un autre entre le SaaS et le IaaS.
\end{enumerate}
Les auteurs proposent donc une gestion de la capacité de l'architecture reposant sur deux éléments.
Le premier est une fonction d'utilité, si une configuration maximise cette fonction alors elle est optimale.
Le second est un algorithme de gestion de capacité fonctionnant en deux parties.
La première partie de cet algorithme va, dans un premier temps, calculer une configuration préliminaire qui respecte la SLA et, dans un second temps, minimiser le coût du service.
La seconde partie s'efforcera de réduire le nombre de serveurs pour réduire les coûts.
Les expériences des auteurs montent que leur méthode de gestion de la capacité s'avère correcte puisque la capacité prédite suit la capacité demandée.

Le but des auteurs est de faire en sorte que cette gestion de la capacité soit automatique.
Ils s'appuient, pour cela, sur une \textit{MAPE-K loop}.
Dans la phase M, des informations sur l'infrastructure, la SLA et la charge de travail sont collectées.
Ces informations sont transmises, périodiquement, à la phase A qui décidera s'il faut re-planifier la capacité grâce à l'algorithme décrit précédemment.
La phase P définira un plan qui sera mis en exécution dans la phase E.
Le nombre de VM actuellement présente dans l'infrastructure sera mémorisé dans la phase K.

\section{Maurer \textit{et al.}}
\label{sec:maurer}
Maurer \textit{et al.} appliquent l'autonomique au \textit{cloud} pour augmenter l'utilisation des ressources et le respect de la \textit{Service Level Agreement} (SLA) \cite{maurer_revealing_2011}.
Ils proposent donc un cadriciel, FoSII, s'appuyant sur une variante de la \textit{MAPE-K loop} à laquelle est ajoutée une étape antérieure d'<<Adaptation>> (A).
Cette nouvelle phase intervient avant le déploiement de l'application dans le \textit{cloud} et son exécution.
Elle consiste à vérifier que la SLA promise par le fournisseur et celle attendue par le client concordent.

Dans la phase M, les auteurs collectent des informations bas niveaux, par exemple, le temps de fonctionnement de la machine.
Ces mesures, bas niveaux, seront alors abstraites en des métriques haut niveau, comme la disponibilité.
Il devient donc possible de vérifier si la SLA a été respectée, par exemple, est-ce que la disponibilité mesurée est inférieure à celle vendue par le fournisseur ?

La phase K des auteurs fonctionne en résonnant avec des cas de bases, \textit{Case Base Reasoning} (CBR).
Cette technique consiste à résoudre un cas en s'appuyant sur le passé : si le cas s'est déjà produit alors la solution est simplement réutilisée.
Les auteurs abstraient les cas comme étant un couple (valeur mesurée, valeur fournis) et ce pour chaque ressource.
Ils utilisent une équation de la littérature pour comparer deux cas et en déduire leur similarité \cite{hefke_framework_2004}.

Les auteurs comparent un cas initial et un cas résultant grâce à une fonction d'utilité.
Cette fonction peut donner plus d'importance au respect de la SLA ou à l'utilisation des ressources.
En mettant plus de poids sur le respect de la SLA, les auteurs ont comparé leur solution au comportement par défaut et ont réussi à réduire de 33\% les violations de SLA au bout de 20 violations.

\section{Malrait \textit{et al.}}
\label{sec:malrait}
Malrait et ses associés s'intéressent à l'impact de la configuration d'un serveur sur le compromis entre performance et disponibilité \cite{malrait_fluid_2009}.
D'après eux, un serveur limite son \textit{Multi-Programming Level} (MPL) pour ne pas être submergé de requêtes.
Les auteurs ont lancé une expérience avec un serveur recevant des requêtes pour différents MPL.
Avec un MPL faible, la latence des requêtes est faible mais le taux d'abandon de celles-ci est élevé.
Tandis qu'avec un MPL haut, le contraire se produit.

Les auteurs ont modélisé le comportement d'un serveur sous forme d'un fluide.
Ils définissent le nombre d'utilisateur du système à un moment $t$ via la Formule \ref{formula:nb_users}, où $\alpha(t)$ correspond au taux d'abandon de requête, $T_i(t)$ au nombre de requêtes reçues et $T_o(t)$ à la capacité de traitement du serveur :
\begin{equation}
	N(t) = (1 - \alpha(t)) \cdot T_i(t) - T_o(t)
	\label{formula:nb_users}
\end{equation}
À la fin de leur raisonnement, les auteurs définissent la Formule \ref{formula:latency} modélisant la latence pour un nombre de client $N$, un mélange de charge de travail $M$ à un temps donné $t$ :
\begin{equation}
	L(N, M, t) = a(M, t)N² + b(M, t)N + c(M,t)
	\label{formula:latency}
\end{equation}
Dans la Formule \ref{formula:latency}, $a$ et $b$ correspondent au temps de traitement des requêtes et $c$ à la latence quand il n'y a aucune charge.
D'après leurs expériences, le modèle des auteurs est proche de la réalité.

Les auteurs rattachent leurs travaux à la théorie du contrôle en définissant deux lois de contrôle.
La première vise à maximiser la disponibilité pour une latence donnée en ajustant le MPL.
Ils utilisent la Formule \ref{formula:mpl1} pour ajuster dynamiquement le MPL.
Dans cette formule, $N$ est le nombre de client actuel, $g$ un paramètre fixé, $L$ la latence actuelle et $L_{max}$ la latence à ne pas dépasser.
\begin{equation}
	MPL = \frac{N}{1 + g \cdot (L - L_{max})}
	\label{formula:mpl1}
\end{equation}
Les auteurs ont lancé une expérience avec le benchmark pour tester leur lois.
Celle-ci permet, à chaque fois, de respecter la latence fixée, tandis que le système non contrôlé voit sa latence être 30\% supérieure à la latence cible.
Par contre, le respect de la latence se fait au détriment du taux d'abandon qui augmente par rapport au système non contrôlé.

La deuxième loi tente de minimiser la latence pour un taux d'abandon donné, là aussi, en réglant la valeur du MPL.
La Formule \ref{formula:mpl2} sera utilisée pour modifier dynamiquement le MPLM en accord avec cette loi.
Elle est l'équivalent de la Formule \ref{formula:mpl1} pour le taux d'abandon.
Ainsi, dans cette formule, $\alpha$ correspond au taux actuel d'abandon de requête, $N$ est le nombre de client actuel, $k$ un paramètre fixé et $\alpha_{max}$ le taux d'abandon à ne pas dépasser.
\begin{equation}
	MPL = \frac{\alpha \cdot N}{\alpha + k \cdot (\alpha - \alpha_{max})}
	\label{formula:mpl2}
\end{equation}
Cette solution permet de réduire la latence d'en moyenne 20\% tout en respectant, dans l'ensemble, le taux d'abandon fixé.

Dans des travaux plus récents, Maltrait \textit{et al.} ont ajouté deux nouvelles lois de contrôle aux deux existantes \cite{malrait_experience_2011}.
La troisième loi étend la deuxième loi en suivant ces propriétés :
\begin{enumerate}
	\item Le taux d'abandon doit rester sous une valeur fixée.
	\item Tant que la latence reste sous une valeur fixée, le taux d'abandon est minimisé.
	\item Tant que le taux d'abandon est inférieur ou égal à la valeur fixée, la latence est minimisée.
\end{enumerate}
Dans leurs expériences, cette loi réduit le taux d'abandon par rapport à la deuxième loi.
Mais, quand la charge de travail est trop conséquente, alors son taux d'abandon égale celui de la deuxième loi.

Symétriquement, la quatrième loi est une extension de la première, elle répond aux propriétés suivantes :
\begin{enumerate}
	\item La latence doit rester sous une valeur fixée.
	\item Tant que le taux d'abandon est inférieur à une valeur fixée, la latence est minimisée.
	\item Tant que la latence est inférieure ou égale à la valeur maximale tolérée, le taux d'abandon est minimisé.
\end{enumerate}
Leurs expériences ont montré que cette loi réduit la latence comparée à la première loi.
De plus, et d'après ces mêmes expériences, leur solution n'a pas de surcoût.

\section{Berekmeri \textit{et al.}}
\label{sec:berekmeri}
Berekmeri et ses co-auteurs s'intéressent aussi au MapReduce \cite{berekmeri_feedback_2018}.
Pour eux, le traitement des données, avec cette technique, n'a aucune garantie de performance.
De plus, ils indiquent que si des mécanismes de dimensionnement existent, ceux-ci ne sont pas pleinement autonomes puisque la définition des seuils est laissée à l'utilisateur.

Les auteurs ont identifié deux cas d'usage.
Le premier, où les performances sont relâchées et l'utilisation des ressources minimisée.
Dans ce cas, la SLA pourra ne pas être respectée pendant un court laps et de temps.
De plus, la minimisation de l'usage des ressources entraîne une diminution du coût.
Dans le second, le respect de la SLA est strict et les performances maximisées.
Ils proposent donc, pour le premier cas, un contrôleur répondant à des événéments et, pour le second, un contrôleur prédictif.

Les auteurs ont lancé une expérience où ils ont ajouté, au cours du temps, des nœuds de calcul pour un nombre de clients donné.
Dans une autre expérience, les auteurs ont fixé le nombre de nœuds et augmenter le nombre de clients.
D'après ces deux expériences, ils concluent que le temps nécessaire pour répondre à une requête n'est pas linéaire vis-à-vis du nombre de nœuds et de clients.

Les auteurs proposent un modèle qu'ils utiliseront dans leurs contrôleurs.
Ce modèle permet de calculer le temps de traitement d'une requête en fonction du nombre de nœuds et de clients.
Les expériences des auteurs montrent qu'il prédit bien le temps de traitement selon si le nombre de clients, respectivement nœuds, augmente ou diminue.

Par la suite, ce modèle est étendu par deux composants que les auteurs ont éprouvé par l'expérience.
Le premier composant, répondant au cas où les performances sont relâchées, réduit le nombre de nœud ajouté tout en garantissant un temps de service acceptable.
Le second, garantit le respect de la SLA dans toutes les situations, si le nombre de client augmente, le nombre de nœud suivra.

\section{De Cicco \textit{et al.}}
\label{sec:decicco}
De Cicco \textit{et al.} se concentrent sur les \textit{Content Delivery Network} (CDN) \cite{de_cicco_resource_2013}.
Selon eux, ces derniers sont dimensionnés pour faire face à un pic de charge.
Ces pics de charge arrivent à certains moments de la journée.
Ainsi, il y aura des moments où l'infrastructure du CDN sera sous-utilisée\sidenote{Il est aisément devinable qu'un service de vidéo à la demande est plus sollicité le soir qu'en début de matinée.}.

Ils proposent donc de se baser sur le \textit{cloud} pour profiter de son élasticité.
Lorsqu'un client souhaite visionner une vidéo, un équilibreur de charge route sa requête vers une VM qui sera en charge d'envoyer le flux au client.
Les auteurs proposent donc de faire varier le nombre de VM de manière à minimiser celui-ci tout en garantissant la meilleure qualité vidéo pour les clients.

Pour tester leur solution, les auteurs ont utilisé un simulateur de CDN.
Leurs expériences ont montré que leur solution permet d'adapter le nombre de VM pour suivre la charge sans sur-approvisionner.

\section{Rao \textit{et al.}}
\label{sec:rao}
Rao et ses coauteurs constatent que l'élasticité du \textit{cloud} introduit une complexité pour l'administrateur système\cite{rao_distributed_2011}.
En effet, il n'est pas trivial, pour celui-ci, de sélectionner la meilleure offre de configuration d'une VM parmi celles proposées par le fournisseur.
De plus, d'après des travaux cités par les auteurs, les pics d'utilisation de différentes applications ne se produisent pas en même temps \cite[Section 6.1, § 5]{armbrust_above_2009}.
Il est donc possible de maximiser l'utilisation du \textit{cloud}.
Pour cela, les auteurs proposent un mécanisme auto-apprenant facilitant l'adaptation des machines virtuelles pour le provisionnement des ressources.

Les auteurs s'accordent sur la complexité à établir un lien entre l'utilisation des ressources et la performance des applications, plus particulièrement pour la mémoire.
En effet, si la mémoire totale utilisée par une application est inférieure à la taille de son \textit{Working Set} (WS) alors ses performances seront moindres \cite{denning_working_1967}.
De plus, estimer la taille du WS n'est pas aisé \cite{gregg_working_2018}.

Les auteurs ont aussi montré qu'ajouter ou retirer des ressources à une VM n'a pas forcément d'effet immédiat.
Ils ont, pour cela, lancé une expérience dans laquelle ils ont retiré des cœurs à une VM et en ont ajouté un à différents moments de l'expérience, tout en mesurant les performances de l'application.
Ils ont alors constaté que l'ajout du cœur n'a pas eu de répercussion immédiate sur les performances mais seulement 5 minutes après.

Les auteurs présentent donc leur solution, iBalloon, qui s'appuie sur 3 composants logiciels.
Le premier est l'agent hôte, il est en charge d'allouer les ressources matérielles de l'hôte aux VM.
Il y a un tel agent par machine physique.
Le deuxième est l'agent applicatif, celui-ci rapporte, à la volée, les performances de l'application.
Chaque VM possède un agent de ce type.
Le dernier est celui qui prend les décisions, comme ajouter ou retirer des ressources à une VM.
Une décision sera prise à partir du statut d'exécution d'une VM.
Ce statut est défini, pour chaque VM, par son utilisation CPU, mémoire, disque et swap.
Avec iBalloon, une décision donne lieu à une réaction qui permettra de calculer une récompense.
Cette récompense punira un paramètre qui ne respecte pas la SLA tout en essayant de maximiser l'usage des ressources.
La décision sera aussi apprise grâce à cette réaction au moyen d'un apprentissage basé sur l'apprentissage renforcé.

Les auteurs ont réalisé plusieurs expériences dans lesquelles ils ont montré que leur solution permet d'adapter correctement les ressources à la charge reçue.
De plus, leur réalisation est celle qui se rapproche le plus des performances optimales, comparées à d'autres solutions issues de l'état de l'art.

\section{Amazon Auto Scaling}
\label{sec:amazon_autoscaling}
Amazon propose, via son \textit{cloud} \textit{Amazon Web Service} (AWS), une solution de dimensionnement, dénommé \textit{AWS Auto Scaling} \cite{barr_new_2018, amazon_aws_2018}.
Celle-ci vise à optimiser la disponibilité ou le coût d'une application, ou encore, un équilibre des deux.
Ce service est disponible à la fois pour les VM et les conteneurs.

Pour le dimensionnement, il s'appuie sur des plans de dimensionnement.
Un plan peut reposer sur un dimensionnement dynamique ou prédictif pour ajuster les instances de l'application.

Le dimensionnement dynamique ajoute, ou supprime, des instances pour maintenir leur utilisation au niveau fixé.
Le dimensionnement prédictif analyse jusqu'à 14 jours d'historique d'une métrique pour prédire sa valeur pour les 2 jours à venir.
Ce dimensionnement est basé sur l'apprentissage automatique, le grain d'une prédiction est l'heure.
\textit{AWS Auto Scaling} ajoutera, ou supprimera, des instances selon la prédiction.
Le dimensionnement dynamique prendra la main si elle s'est révélée trop rassurante.

Amazon conseille aux clients de connaître leur application pour créer un plan de dimensionnement le plus efficace possible.
Un plan personnalisé repose sur une métrique de dimensionnement ou une métrique de charge, uniquement disponible pour le dimensionnement prédictif, et une valeur cible.

Dans un document, Amazon montre que la prédiction de l'utilisation CPU suit la réalité, mais cette prédiction se révèle moins vraie pour le nombre d'instances \cite[page 15]{amazon_aws_2018}.
Mais le contexte de cette prédiction n'est pas donné.

\section{Moore \textit{et al.}}
\label{sec:moore}
Moore et ses collaborateurs souhaitent tirer parti de l'élasticité offerte par le \textit{cloud} \cite{moore_transforming_2013}.
Pour cela, ils proposent un cadriciel, \textit{Platform Insights}, répondant à plusieurs cas.
Celui-ci engendre, à partir de règle statique pour l'ajout de VM, des modèles de dimensionnement prédictif.
Il est aussi capable d'apprendre à la volée et d'employer des modèles basés sur les fenêtres temporelles, ce pour ajouter du contexte à la décision de dimensionnement.

Le modèle prédictif des auteurs comprend 3 modèles, le premier est basé sur des séries temporelles tandis que les deux autres s'appuient sur des modèles bayésiens naïfs.
Le premier modèle prend en entrée la charge de travail et, à partir de celle-ci, donnera une prédiction de la charge de travail future.
Cette prédiction sera comparée à la moyenne de la charge de travail sur les 4 dernières heures pour catégoriser la tendance de la charge de travail comme croissante, décroissante ou stable.
L'intervalle de confiance de cette prédiction est mis à l'échelle et donné au deuxième modèle.

Ce deuxième modèle prend en entrée la charge de travail, la \textit{Quality of Service} (QoS) et le nombre d'instances actuel.
La valeur de la QoS est utilisée pour savoir si cette QoS est respectée ou non.
Ainsi, ce modèle apprend le lien entre la charge de travail et la QoS pour prédire le nombre d'instances garantissant le respect de la QoS dans 95\% des cas.

Le contrôleur prédictif comporte trois modèles : un basé sur des séries temporelles et deux modèles bayésiens. Le couple de règles agrège pour la première règle les valeurs de la QoS (Quality of Service) et de la charge de travail tandis que la seconde règle n'agrège que la charge de travail.
Le modèle basé sur les séries temporelles ne s'occupe que de la valeur de la seconde règle. La valeur projetée est ensuite comparée à la moyenne obtenue sur une fenêtre de 4 heures afin d'estimer si la charge de travail a augmenté, diminué ou est restée stable.
L'intervalle de confiance de la valeur projetée est mis à l'échelle puis donné au troisième modèle afin de déterminer les limites du nombre d'instances du serveur attendu pour la prochaine période.

Le deuxième modèle prend en entrée les valeurs (QoS et charge de travail) de la première règle ainsi que le nombre d'instances actuelles. La valeur de QoS est utilisée pour savoir si la QoS est ou non violée. Le deuxième modèle va donc apprendre la relation entre la charge de travail et la QoS afin de prédire le nombre de serveurs pour que la QoS ne soit pas violée dans 95\% des cas.

Le troisième et dernier modèle est similaire au deuxième mais il en prend en entrée l'intervalle de confiance de la prédiction calculée par le premier modèle.
Il renvoie une estimation du nombre d'instances devant être opérationnelles pour respecter la QoS dans les 30 minutes.

Les auteurs ont lancé plusieurs expériences pour tester leur modèle.
Malheureusement, leur premier modèle n'a pas été capable de prédire un pic de requête.
De plus, la probabilité d'irrespect de la QoS pour le deuxième et troisième modèle atteint 70\% à partir de 500 requêtes.

\section{Li \textit{et al.}}
\label{sec:li}
Li et Xia se placent dans le cadre d'un \textit{cloud} hybride.
Un tel \textit{cloud} repose à la fois sur un \textit{cloud} public, où un particulier peut louer une instance, et un \textit{cloud} privé, ne pouvant être accédé par le \textit{quidam}, comme un \textit{cloud} interne à une entreprise \cite{li_auto_scaling_2016}.
Le \textit{cloud} public sera utilisé pour étendre les capacités de son homologue privé en cas de pic de charge.

Les auteurs s'intéressent aux conteneurs \texttt{docker}.
Pour eux, avec les conteneurs, le dimensionnement horizontal est plus efficace que le vertical.
De plus, lancer un conteneur est bien plus rapide qu'une VM.

La solution des auteurs s'appuie sur un équilibreur de charge, celui-ci va, à partir de l'IP, router la requête vers le \textit{cloud} correspondant.
Si la géolocalisation de l'IP indique qu'elle est proche du \textit{cloud} privé, alors la requête lui sera envoyée, sinon c'est le \textit{cloud} public qui sera en charge de celle-ci.

Leur solution comporte aussi un serveur de gestion.
Celui-ci va collecter le nombre de requêtes pour chaque période de temps donnée et stocker ce nombre dans un historique.
Cet historique sera analysé afin de prédire, au moyen du modèle ARMA, le nombre de requêtes pour la prochaine période.
Le nombre de conteneurs nécessaires pour répondre aux requêtes sera calculé à partir de cette prédiction.
Celle-ci a été validée par l'expérience.

Les auteurs augmentent leur solution avec un modèle réactif à base de seuil.
Un nouveau conteneur sera lancé si l'utilisation des ressources de ceux-ci est supérieure à un seuil donné.
La suppression d'un conteneur n'est effectuée qu'à condition que le modèle prédictif ait annoncé avoir besoin de moins de conteneurs qu'actuellement, et ce pour $n$ périodes de temps consécutives.
Les auteurs préfèrent ajouter un conteneur sur le \textit{cloud} privé et en supprimer sur le public afin de minimiser le coût.
De plus, des conteneurs s'exécutant de concert seront exécutés sur le même \textit{cloud}.

Les expériences des auteurs ont montré que leur solution adapte correctement le nombre de conteneurs à la charge tout en privilégiant le \textit{cloud} privé.

\section{Jin-Gang \textit{et al.}}
\label{sec:jingang}
Jin-Gang et ses co-auteurs s'intéressent aux applications de communication unifiée \cite{jin_gang_research_2017}.
Ils les définissent comme des logiciels regroupant la messagerie instantanée, les conférences multimédias ainsi que les appels audios et vidéos.
De telles applications sont accédées depuis de nombreux périphériques tels que des ordinateurs personnels ou des \textit{smartphones}.
Les différents services d'une application de communication unifiée sont indépendants les uns des autres mais communiquent via un même bus.

Pour les auteurs, les conteneurs \texttt{docker} sont plus légers que les VM.
Ils estiment aussi qu'il y a plus de travaux visant à dimensionner celles-ci.
Les auteurs utilisent \texttt{kubernetes} comme orchestrateur.

L'algorithme des auteurs scanne les pods \texttt{kubernetes} pour calculer leur utilisation moyenne.
L'utilisation des nœuds, où les pods s'exécutent, sera aussi surveillée.
Si l'utilisation des pods est supérieure à un seuil alors des pods seront ajoutés.
\textit{A contrario}, si cette utilisation est inférieure à ce seuil, des pods seront supprimés.

Le modèle ARIMA est utilisé pour prédire le nombre de requêtes de la prochaine période.
Si la différence entre la prédiction et la réalité est conséquente, des pods seront ajoutés pour faire face au pic de requêtes.

Enfin, si l'utilisation d'un nœud est supérieure à une valeur donnée, un coefficient de corrélation sera calculé pour chaque pod du nœud.
Tant que l'utilisation du nœud concerné sera supérieure à la valeur fixée, ses pods seront migrés par ordre décroissant de leurs coefficients de corrélation.

Les auteurs se sont comparés au mécanisme de dimensionnement de \texttt{kubernetes}.
Avec leur solution, le temps de réponse est inférieur car ils ajoutent des pods pour faire face aux pics de charge.
De plus, quand l'activité est faible, les pods sont libérés plus vite, la solution des auteurs minimise donc le coût.

\section{Kubernetes}
\label{sec:k8s}
Kubernetes est un logiciel, \textit{open source}, permettant l'automatisation, le passage à l'échelle et la gestion d'applications conteneurisées \cite{kubernetes_kubernetes_nodate}.
L'élément de base de Kubernetes est un \textit{pod} qui est un ensemble de conteneurs.
Les \textit{pods} s'exécutent sur des nœuds qui sont des machines virtuelles ou physiques.

Kubernetes intègre un mécanisme de dimensionnement dynamique \cite{noauthor_kubernetes_autoscaler_2019}.
Lorsqu'un \textit{pod} ne pourra être ordonnancé sur un nœud, par exemple, si le pod nécessite plus de mémoire que ce dont dispose le nœud sous-jacent.
Ce \textit{pod} sera donc marqué comme ne pouvant être ordonnancé et un nouveau nœud sera donc ajouté.

À l'inverse, si un dimensionnement à la hausse n'est pas demandé, un nœud non nécessaire pourra être supprimé.
Un nœud est considéré ainsi si toutes les conditions suivantes sont satisfaites :
\begin{enumerate}
	\item La somme des requêtes d'utilisation du processeur et de la mémoire du nœud est inférieure à la moitié de la capacité pouvant être allouée par ce nœud.
	\item Les \textit{pods} s'exécutant sur ce nœud peuvent être exécutés sur d'autres nœuds.
	\item Le nœud n'est pas marqué comme ne pouvant être supprimé.
\end{enumerate}
Si le nœud est marqué comme non nécessaire pendant \SI{10}{\minute}, il pourra être supprimé.
Les \textit{pods} du nœud supprimé seront bien entendu migrés vers d'autres nœuds.

\section{Google Cloud Scaling}
\label{sec:google_scaling}
Google Cloud Scaling permet le dimensionnement horizontal de VM en fonction de la charge de travail \cite{noauthor_google_2018}.
Ce dimensionnement se base sur l'utilisation du processeur, le nombre de requêtes HTTP reçues ou bien des métriques personnalisées par l'utilisateur.
Par exemple, si l'utilisateur a défini le taux d'utilisation du processeur comme étant 80\%, le dimensionnement automatique ajoutera ou enlèvera des VM pour que cette métrique respecte le taux défini.

Pour revoir le nombre de VM à la baisse, Google Cloud Scaling calcule le nombre de VM cible en fonction de la charge maximale reçue sur les 10 dernières minutes.
Si le nombre cible est inférieur au nombre courant de VM alors des VM seront supprimées.

\section{Microsoft Azure Scaling}
\label{sec:microsoft_scaling}
Selon Microsoft, le dimensionnement vertical n'est pas utilisé avec les VM car, pendant cette opération, elles seront indisponibles.
Le \textit{cloud} Azure propose donc du dimensionnement horizontal basé sur des seuils statiques, par exemple, ajouter une VM si l'utilisation moyenne du processeur est supérieure à 70\% \cite{noauthor_microsoft_2017}.
Les métriques utilisées peuvent être des métriques personnalisées.
Ce \textit{cloud} offre la possibilité de planifier les dimensionnements, il devient donc possible, par exemple, d'ajouter une VM les week-ends ou à certains horaires de la journée.

\section{Kriushanth \textit{et al.}}
\label{sec:kriushanth}
D'après Kriushanth et Arockiam, le dimensionnement dynamique se fait principalement à base de seuils statiques \cite{kriushanth_load_2015}.
Pour eux, une règle simple, comme ajouter ou retirer des VM selon l'utilisation du processeur, n'est pas sans problème.
En effet, les seuils d'utilisation doivent être bien calibrés au risque de générer des oscillations dans le nombre d'instances.

Ils présentent donc LoBBI, un équilibreur de charge capable de générer des règles de dimensionnement pour fournir dynamiquement les ressources.
Cet équilibreur de charge est défini dans la Formule \ref{formula:lobbi}\sidenote{La formule des auteurs a été simplifiée, <<\textit{diviser par une fraction, c'est multiplier par son inverse}>>.} :
\begin{equation}
	\int_{a}^{b} \frac{a_1}{b_1} \cdot 50
	\label{formula:lobbi}
\end{equation}
Dans la Formule \ref{formula:lobbi}, $a$ correspond au seuil minimal d'utilisation d'une ressource, $b$ à son seuil maximal, $a_1$ aux ressources disponibles et $b_1$ au nombre de requêtes reçues.

L'équilibreur de charge enverra des informations à un générateur de règles qui prendra une décision en fonction de seuils et de la valeur renvoyée par la Formule \ref{formula:lobbi}.
Les auteurs donnent plusieurs cas en exemple, où $I$ est la valeur renvoyée par leur modèle :
\begin{itemize}
	\item Si $I < 20\%$, la charge est faible, le nombre de VM est donc surdimensionné par rapport à celle-ci.
	Des VM seront donc enlevées.
	\item Si $I > 80\%$, la charge est trop importante pour le nombre de VM actuel.
	L'équilibreur de charge ajoutera donc des VM.
	\item Si $I > 70\%$, la valeur de seuil, $b$ dans la Formule \ref{formula:lobbi}, est augmentée.
	La charge de travail est gérée avec les VM disponibles.
\end{itemize}
Dans tous les autres cas, les auteurs estiment que les VM déjà instanciées peuvent supporter la charge de travail.

Les auteurs ont lancé une expérience montrant que leur solution minimise l'utilisation des ressources.

\section{Ge \textit{et al.}}
\label{sec:ge}
Ge et ses associés s'intéressent au MapReduce avec des échéances \cite{ge_resource_2019}.
Ainsi, une tâche manquant de ressources pourrait rater celle-ci.
Pour pallier à cela, il faut donc accorder des ressources à ces tâches, ce peut être fait dynamiquement ou statiquement.

Pour Ge et ses co-auteurs, le travail est divisé en <<slot>> dans les cadriciels MapReduce tels que Hadoop.
Certains slots sont réservés pour les opérations Map et d'autres pour les Reduce.
Dans cette configuration, les slots Reduce ne seront pas utilisés tant que les slots Maps n'auront pas terminé leurs tâches.

Les auteurs proposent donc une architecture sans slot à base de dimensionnement horizontal.
Des conteneurs seront ajoutés pour fournir la puissance de calcul nécessaire à la phase Reduce.
Dans l'hypothèse où il y aurait trop de conteneurs et que ceux-ci seraient inactifs, ils pourront être enlevés.
La Formule \ref{formula:map} renvoie le nombre de conteneurs nécessaires pour la phase Map tandis que la Formule \ref{formula:reduce} ceux pour la phase Reduce.
\begin{equation}
	P_m = \frac{\sqrt{R_mD_m}(\sqrt{R_mD_m} + \sqrt{R_r(D_r + D_s)})}{T}
	\label{formula:map}
\end{equation}
\begin{equation}
	P_r = \frac{\sqrt{R_r(D_r + D_s)}(\sqrt{R_mD_m} + \sqrt{R_r(D_r + D_s)})}{T}
	\label{formula:reduce}
\end{equation}
Dans ces formules, $R_m$/$R_r$ indique le nombre de tâches Map/Reduce restantes, $D_m$/$D_r$/$D_s$ la durée estimée pour la phase Map/Reduce/Shuffle et T la durée du MapReduce complet.

Les auteurs ont modifié Kubernetes et ont développé deux ordonnanceurs s'appuyant sur les formules décrites précédemment.
Le premier, semi-statique, dimensionne le nombre de conteneurs associés aux opérations de Map et Reduce au début des calculs.
Tandis que le second, dynamique, s'active lors d'un événement, par exemple, si une tâche se termine ou loupe son échéance.

D'après leurs expériences, l'ordonnanceur semi-statique est meilleur que son alter ego dynamique.
En effet, avec le premier, les tâches ne ratent pas leurs échéances.
L'allocation dynamique est la cause des moindres performances du second.
En effet, celle-ci perturbe l'exécution des conteneurs et donc les tâches associées ratent leurs échéances.

\section{\textit{Automatic ballooning}}
\label{sec:auto_ballooning}
Capitulino présente un projet visant à rendre le \textit{memory ballooning} automatique \cite{qemu_qemu_2019, kvm_automatic_2013, capitulino_automatic_2013}.
Celui-ci consiste à récupérer de la mémoire à une VM pour la donner à une autre.
Pour cela, la mémoire inutilisée d'une VM est réclamée, il est dit que la VM <<gonfle le ballon>>.
Une autre VM pourra alors récupérer cette mémoire, cette VM va, pour cela, <<dégonfler le ballon>>.
La mémoire ainsi récupérée lui sera allouée.

L'automatisation du <<gonflement du ballon>> repose sur des événements de pression mémoire basés sur des seuils statiques.
Le <<dégonflement>> se fait avec des fonctions interchangeables.

Le présentateur indique que ce projet est encore assez expérimental et qu'il doit être plus testé.

\section{Kim \textit{et al.}}
\label{sec:kim}
Kim et les autres auteurs de l'article constatent que les solutions actuelles de \textit{memory ballooning} présentent un surcoût trop important \cite{kim_dynamic_2015}.
Pour cela, ils proposent donc un \textit{memory ballooning} conscient de la pression mémoire.

Ils définissent alors trois seuils de pression mémoire.
La pression mémoire est faible si la somme de la mémoire anonyme\sidenote{
	La mémoire anonyme correspond à de la mémoire qui ne peut être lue depuis le disque, comme celle obtenue après un appel \texttt{malloc}.
	La mémoire fichier peut, elle, être lue depuis le disque.
	Nous reviendrons sur cette différence dans la Section \ref{subsec:page}.
}, de toutes les VM, additionnée à la mémoire fichier, de toutes les VM, est inférieure à la mémoire dont dispose l'hôte.
Elle est considérée comme moyenne quand cette somme est supérieure ou égale à la mémoire totale que possède l'hôte.
Enfin, elle est élevée lorsque la somme de la mémoire anonyme de toutes les VM est supérieure ou égale à la mémoire possédée par l'hôte.
Les auteurs proposent aussi que chaque VM dispose d'une zone <<coussin>> dans son ballon, ce pour faire face à de forte demande mémoire.

Les auteurs ont comparé leur solution à Tmem \cite{magenheimer2009transcendent}.
Celle-ci réduit drastiquement le nombre de défaut de page et d'hypercall tout en se rapprochant de la solution optimale.

\section{Carver \textit{et al.}}
\label{sec:carver}
Pour Carver et ses coéquipiers, la virtualisation permet de découper une machine physique en plusieurs <<sous-machines>> \cite{carver_acdc_2017}.
Celles-ci sont plus faciles à vendre, néanmoins, il est plus compliqué de récupérer des ressources qui leur ont été allouées.
Les auteurs s'intéressent aux conteneurs \texttt{docker}.

Ils catégorisent les utilisateurs comme actifs, ils utilisent toutes les ressources qui leur ont été allouées, ou inactifs, ils n'ont pas besoin de leurs ressources.
La somme des ressources consommées par les utilisateurs actifs ne dépasse jamais le total des ressources dont dispose la machine sous-jacente.

Les auteurs ont remarqué une chute de performance des conteneurs actifs quand un conteneur devient actif et qu'un autre devient inactif.
Ils expliquent ce comportement par l'implémentation des conteneurs dans le noyau Linux\sidenote{Nous y reviendrons dans le Chapitre \ref{chapter:containers}.}.

Pour pallier à cela, ils proposent d'utiliser une horloge globale et des horloges locales.
L'horloge globale sera incrémentée à chaque fois qu'une ressource mémoire est allouée à un conteneur.
Les horloges locales sont propres à chaque conteneur.
L'horloge d'un conteneur sera modifiée quand une nouvelle ressource mémoire lui sera attribuée.
Les auteurs ont modifié l'algorithme de réclamation mémoire de Linux pour que les conteneurs soient réclamés du plus ancien au plus jeune en se basant sur l'horloge globale et les horloges locales.
La réclamation mémoire prend fin quand suffisamment de mémoire a été réclamée.

Les expériences des auteurs ont montré que le problème décrit précédemment n'est plus présent, ce grâce à leur solution.