\chapter{MemOpLight : Principe de fonctionnement}
\label{chapter:memoplight}
\section{Introduction}
\label{sec:memoplight_intro}
Dans le Chapitre \ref{chapter:container_flaw}, nous avons montré qu'il est difficile de garantir à la fois l'isolation et la consolidation mémoire en utilisant les fonctionnalités offertes par le \texttt{cgroup} mémoire.

Il en résulte donc un problème pour le client et le fournisseur de \textit{cloud}.
En effet, l'absence d'isolation, dans une plateforme \textit{cloud} signifie que le conteneur d'un client pourrait affamer ceux des autres.
Ce comportement n'est pas souhaitable pour le client ni pour le fournisseur.
En effet, il en résulterait une violation du SLA et ce dernier devrait donc payer des pénalités au premier \cite{ovh_conditions_nodate, amazon_sla_nodate}.
De plus, l'absence de consolidation empêche une utilisation optimale des ressources de la plateforme et donc un manque à gagner pour le fournisseur.
De l'autre côté, si le conteneur d'un client n'est pas limité dans son utilisation des ressources, le coût de celui-ci sera onéreux, puisqu'un client est facturé pour ce qu'il utilise \cite{amazon_amazon_nodate}.

D'autre part, les mécanismes actuels offerts par le noyau Linux pour garantir l'isolation sont purement statiques et ne s'adaptent donc pas à des charges de travail dynamiques.
Ainsi, fixer une \texttt{max} ou \texttt{soft} limite pour les requêtes reçues à \SI{10}{\hour} du matin ne garantit absolument pas que le serveur conteneurisé pourra faire face au pic de requêtes arrivant à \SI{18}{\hour}.
Symétriquement, renseigner la valeur de ces limites par rapport aux besoins du précédent pic implique un surdimensionnement des ressources le reste du temps et donc une surfacturation pour le client.

De surcroît, les articles détaillés dans le Chapitre \ref{chapter:state_of_the_art} ont montré qu'un dimensionnement basé sur les performances telles que perçues par une application permet d'être plus proche de ses besoins.
Quoi de mieux que l'application elle-même pour juger de ses performances ?
En effet, les métriques disponibles au niveau du système d'exploitation sont totalement déconnectées de la satisfaction comme la voit l'application.

Néanmoins, une approche basée uniquement sur l'application perd la vision globale des ressources qu'offre une vue bas niveau telle que celle du système ou de l'hyperviseur.
En effet, avec le \textit{page cache} une application a toujours besoin de mémoire, il faut savoir limiter la taille de celui-ci quand la ressource mémoire se fait rare.

C'est pourquoi, dans cette thèse, nous proposons MemOpLight\sidenote{Notre solution utilise les couleurs des feux de circulation pour indiquer l'état de performance d'un conteneur.}.
Notre contribution peut être vue comme une double boucle MAPE.
La première collecte des informations sur l'état de performance des applications vis-à-vis d'un SLO et en informe le noyau.
Quant à la seconde, elle réclame la mémoire de certains conteneurs en se basant sur les informations fournies par la première.

MemOpLight vise à s'exécuter dans le \textit{cloud}, mais cet environnement est régi par des accords fixant la qualité de service, notre mécanisme doit donc prendre en compte ceux-ci.
Ainsi, dans la suite de ce chapitre, nous commençons par définir les différents termes liés à la qualité de service pour terminer par une présentation de notre mécanisme.

\section{Qualité de service}
\label{sec:qos}
Dans l'introduction, nous avons indiqué que la location de ressources par un client à un fournisseur de cloud faisait l'objet d'une SLA.
Nous revenons donc sur les différents termes utilisés dans ces ententes puis nous en donnons des exemples, fictifs ou réels.

\subsection{Termes liés à la qualité de service}
\label{subsec:qos_terms}
Les SLA se basent sur plusieurs termes, dont les sigles, se rapprochant l'un l'autre, ont des significations divergeant fortement.
Dans les faits, une \textit{Service Level Agreement} (SLA) se base sur des \textit{Service Level Objectives} (SLO) \cite[Chapter 4]{beyer_site_2016}.
Ceux-ci sont définis comme une valeur cible mesurée par un \textit{Service Level Indicator} (SLI).
Un SLI est une mesure quantitative d'un aspect du service fourni comme la latence des requêtes ou la disponibilité\sidenote{Comme nous l'avons indiqué dans l'introduction, la disponibilité est le SLI le plus utilisé par les fournisseurs de \textit{cloud}.}.

Ainsi, un SLO garantit que la valeur mesurée, par exemple, de la disponibilité sera supérieure ou égale au SLI associé.
Si cette valeur devient inférieure, le SLA n'est pas respecté.

Dans le cas de MemOpLight, un SLO\sidenote{Pour MemOpLight, les termes SLO et <<performance satisfaisante>> sont interchangeables.} est passé en paramètre d'une boucle MAPE.
Notre mécanisme fera tout son possible pour garantir le respect de celui-ci en informant le système d'exploitation qui fera alors varier l'allocation mémoire.

\subsection{Exemples de SLA}
\label{subsec:qos_example}
Les SLA varient en fonction du service proposé.
Ainsi, pour un IaaS, la SLA la plus utilisée se base sur la disponibilité.
Par exemple, un fournisseur de \textit{cloud} s'engage à ce que son service ait une disponibilité mensuelle d'environ 99.9\% \cite{microsoft_sla_nodate, ovh_conditions_nodate, amazon_sla_nodate}.
Tandis que pour un \textit{Software as a Service} (SaaS) ou \textit{Platform as a Service} (PaaS), il existe des SLA plus variés.

Le service de base de données Azure Cosmos DB est, à notre connaissance, le seul PaaS à garantir, en plus de la disponibilité, un temps de réponse borné.
Celui-ci est de \SI{10}{\milli\second}, respectivement \SI{15}{\milli\second}, pour des opérations de lecture, respectivement d'écriture, ayant réussi en renvoyant moins de \SI{1}{\kilo\byte} de données \cite{microsoft_sla_2018}.

En s'inspirant de ce que propose Azure Cosmos DB, il devient possible d'imaginer un SLA pour un serveur web où le SLO serait la latence des requêtes.
Cet engagement pourrait spécifier que les requêtes ayant renvoyé un code 200 et moins de $s$ octets aient une latence inférieure à \SI[parse-numbers = false]{t}{\milli\second}.

Pour un SaaS, il est possible d'imaginer une application de \textit{streaming} faisant l'objet d'un SLA concernant la qualité de son flux et son débit d'informations par seconde.
Le fournisseur d'un tel service orienté vidéo pourrait s'engager sur l'un de ces critères, par exemple, un flux d'image définie en $1920 * 1080$ pixels, ou les deux, ce même flux avec un débit de 60 images par seconde.
Ainsi, le client pourrait donc choisir le SLA correspondant le plus à son besoin.

\section{MemOpLight}
\label{sec:memoplight}
Afin d'allier les avantages de l'exécution en espace utilisateur à ceux procurés par l'espace noyau, plusieurs solutions sont possibles.
En effet, les éléments de la boucle MAPE peuvent être répartis dans ces deux espaces.

Dans les sous-sections à venir, nous étudions les faiblesses d'une boucle prenant place principalement en espace utilisateur, respectivement noyau.
Nous concluons par l'étude d'une double boucle permettant de pallier aux inconvénients des deux précédentes.

\subsection{Boucle principalement en espace utilisateur}
\label{subsec:loop_user}
Comme représenté par la Figure \ref{fig:loop_user}, il est possible d'imaginer une boucle dont les éléments siègent principalement en espace utilisateur.
Ainsi, la décision est prise dans cet espace en se basant à la fois sur les performances de l'application et des informations transmises par le noyau.

\begin{figure}
	\begin{addmargin}{-5cm}
		\centering

		\subfloat[Boucle principalement en espace utilisateur]{
			\includegraphics[scale = .21]{loop_user.pdf}

			\label{fig:loop_user}
		}
		\subfloat[Boucle principalement en espace noyau]{
			\includegraphics[scale = .21]{loop_kernel.pdf}

			\label{fig:loop_kernel}
		}
		\subfloat[Double boucle]{
			\includegraphics[scale = .21]{memoplight.pdf}

			\label{fig:memoplight}
		}

		\caption{Les différentes boucles possibles}
		\label{fig:solutions}
	\end{addmargin}
\end{figure}

Néanmoins, cette approche a deux principaux défauts.
Premièrement, le noyau doit transmettre des informations aux applications ce qui peut résulter en des indiscrétions.
Par exemple, l'application d'un client A pourrait inférer que l'application du client B est très active à partir de ces informations.
Secondement, puisque chaque application prend une décision, celles-ci pourraient être en contradiction.
Il est possible que ces décisions convergent, mais cela ne se ferait qu'au bout d'un certain temps conduisant possiblement à un non-respect de la SLA.

Pour pallier ces problèmes, une boucle dont les éléments prennent principalement place en espace noyau semble être une réponse convenable.

\subsection{Boucle principalement en espace noyau}
\label{subsec:loop_kernel}
Symétriquement à la boucle présentée dans la Figure \ref{fig:loop_user}, celle montrée par la Figure \ref{fig:loop_kernel} voit ses éléments occuper l'espace noyau.
De cette manière, la décision est prise en espace noyau en se basant sur des informations transmises par les applications.

Néanmoins, cette approche doit être capable d'inférer l'état de performance de chaque application à partir de données hétérogènes.
En effet, une application peut être un serveur web et une autre un logiciel de \textit{streaming} vidéo.
Il faut donc que l'algorithme de la phase \textit{Analysis} soit capable d'inférer l'état de performance d'une application pour des métriques aussi variées qu'une latence ou un nombre d'images par seconde.
Nous doutons qu'un algorithme aussi générique soit réalisable en espace noyau compte tenu des limitations de celui-ci évoquées dans la Section \ref{subsec:solution_complexity}.

Cette approche a donc, comme l'approche précédente, des inconvénients.
Une solution s'appuyant sur une double boucle, prenant place à la fois en espace utilisateur et noyau, nous semble être une solution de choix.

\subsection{MemOpLigt : une double boucle}
\label{subsec:double_loop}
MemOpLight, la contribution principale de cette thèse, permet de remédier aux problèmes des deux approches susmentionnées.
Ce mécanisme peut être représenté comme deux boucles MAPE chaînées entre elles.
La Figure \ref{fig:memoplight} montre cette représentation.

L'une de ces boucles correspond au contrôleur applicatif, et prend donc place en espace \textit{user}, tandis que l'autre correspond au mécanisme noyau, et siège donc en espace \textit{supervisor}.
De cette manière, MemOpLight profite des avantages d'une double vue tels qu'énoncés dans le Tableau \ref{table:views_sumup}.

Le contrôleur applicatif, de la première boucle MAPE, permet de collecter les performances de l'application conteneurisées.
Ces performances sont ensuite analysées et un compte-rendu de celles-ci est transmis au noyau Linux via un moyen de communication \textit{user-supervisor}.
Enfin, et à partir de ces données, un mécanisme d'arbitrage, résidant dans le noyau et représenté par la seconde boucle MAPE, permet de réclamer de la mémoire aux conteneurs présentant de bonnes performances.
La mémoire réclamée pourra être utilisée par les conteneurs ayant des performances moindres dans l'optique d'augmenter celles-ci.

Dans le reste de chapitre, nous étudions, dans le détail, les composants de chacune de ces boucles.

\section{Le contrôleur applicatif}
\label{sec:applicative_probe}
Le contrôleur applicatif correspond à la boucle MAPE de l'espace \textit{user} dans la Figure \ref{fig:memoplight}.
Il a pour but de vérifier que la SLA est respectée.
Pour cela, il sonde les performances de l'application.

Comme tout instrument de mesure, il ne doit pas perturber les mesures effectuées \cite{noauthor_probe_nodate}.
De plus, ce mécanisme s'intègre à des applications existantes.
Il doit donc, pour être couramment utilisé, accéder à des informations déjà disponibles pour minimiser les modifications à apporter aux applications .
Dans les faits, un contrôleur ne fait que lire des métriques qui sont déjà calculées, il suffit alors de les exposer pour qu'il puisse rendre son verdict.

Dans cette section, nous décrivons, dans un premier temps, le but d'un contrôleur applicatif.
Nous détaillons, dans un deuxième temps, son fonctionnement en détaillant chacune des étapes de la boucle MAPE.
Dans un troisième et dernier temps, nous donnons des exemples de sondes pour différents types d'application.

\subsection{But d'un contrôleur applicatif}
\label{subsec:applicative_probe_goal}
Un contrôleur applicatif doit informer le noyau du respect de la SLA.
Il utilise, pour ce faire, les métriques de l'application conteneurisée plutôt que des métriques noyaux qui sont trop éloignées des performances telles que vues par les applications.
De plus, le contrôleur contextualise ces performances.
En effet, celles-ci peuvent être incomplètes.
En pratique, le contrôleur scrute plusieurs données :
\begin{description}
	\item[l'entrée :] C'est la charge du système en entrée.
	Par exemple, un serveur web a reçu 1000 requêtes cette seconde.
	\item[la sortie :] C'est ce qu'a pu traiter le système.
	Le serveur web a répondu à 900 requêtes cette seconde.
	\item[la performance de référence :] Les performances du conteneur sont jugées satisfaisantes quand sa sortie est supérieure ou égale à cette donnée, fournie par l'utilisateur.
	Dans notre exemple, si l'utilisateur a fixé cette valeur à 850 requêtes par seconde, le serveur web conteneurisé a des performances satisfaisantes.
\end{description}
Le contrôleur doit absolument contextualiser les métriques de l'application.
En effet, si dans notre exemple le serveur n'avait reçu, en entrée que 100 requêtes, et qu'il les avait toutes traitées, ses performances doivent alors être jugées comme satisfaisantes bien qu'elles soient inférieures au SLO donné par l'utilisateur.

\subsection{Étape \textit{Monitor}}
\label{subsec:probe_monitor}
Le contrôleur applicatif prend place dans un conteneur aux côtés de l'application.
Il collecte les métriques mises à disposition par celle-ci.
Ainsi, son intrusivité est limitée.

Dans l'exemple donné dans la Figure \ref{fig:memoplight}, le contrôleur récupère les requêtes traitées par le serveur web depuis son fichier \texttt{access.log}.

\subsection{Étape \textit{Analysis}}
\label{subsec:probe_analysis}
Dans cette étape, les métriques collectées dans la phase \textit{Monitor} sont transformées pour pouvoir être comparées au SLO fourni par l'utilisateur.
Le calcul d'une moyenne ou d'un 95e centile sont de bons exemples des transformations que peut effectuer un contrôleur.

Dans la Figure \ref{fig:memoplight}, le contrôleur calcule une moyenne du nombre de requêtes pour une période donnée, par exemple chaque seconde.

\subsection{Étape \textit{Plan}}
\label{subsec:probe_plan}
Le contrôleur compare ici les performances de l'application au SLO.
Selon le résultat de ces comparaisons, l'état de performance du conteneur est qualifié par 3 couleurs :
\begin{description}
	\item[ROUGE :] Les performances du conteneur sont inférieures aux performances de référence.
	\item[JAUNE :] Le conteneur atteint les performances de référence, mais pourrait faire mieux si des ressources lui étaient allouées\sidenote{
		L'article R312-29 du code de la route stipule ceci \cite{noauthor_code_2001} :
		\begin{quote}
			 Les feux de signalisation lumineux réglant la circulation des véhicules sont verts, jaunes ou rouges.
		\end{quote}
	}.
	\item[VERT :] Les performances du conteneur sont les meilleures possibles.
\end{description}

Dans notre exemple dépeint par la Figure \ref{fig:memoplight}, la couleur du conteneur est décidée suite à la comparaison de la moyenne du nombre de requêtes avec le SLO.
L'exemple donné ici est volontairement simplifié puisqu'il ne prend pas en compte l'entrée du système contrôlé.

\subsection{Étape \textit{Execute}}
\label{subsec:probe_execute}
La couleur du conteneur, calculée dans l'étape antérieure, est ensuite communiquée au noyau via un moyen de communication \textit{user-supervisor}.

Nous étudions d'abord les différents moyens de communication que nous aurions pu utiliser et pourquoi notre choix s'est porté sur le \texttt{sysfs}.
Puis, nous montrons, dans les faits, comment cette communication s'effectue.

\subsubsection{Interface \textit{user-supervisor}}
\label{subsubsec:interface}
De multiples moyens de communication entre l'espace utilisateur et l'espace noyau existent comme les appels système, les \textit{sockets} ou le \texttt{sysfs}.
Un appel système peut sembler être la voie royale pour communiquer avec le noyau.
Néanmoins, l'ajout d'un nouvel appel nécessite de modifier des domaines du noyau non relatifs à la mémoire.
Son utilisation se fait via son invocation ce qui implique d'utiliser un langage bas niveau ou une interface depuis un langage haut niveau.

Les \texttt{ioctls} forment une alternative aux appels système \cite{the_kernel_development_community_ioctl_nodate}.
En effet, l'ajout d'un nouvel \texttt{ioctl} est plus aisé que celui d'un appel système.
Néanmoins, l'utilisation se fait via l'invocation de la fonction \texttt{ioctl} avec comme argument des drapeaux particuliers.
Ainsi, le problème de l'appel système n'est pas solutionné.

Le \texttt{sysfs} est un système de fichier virtuel, les fichiers n'existent pas sur le disque, permettant d'interagir avec le noyau.
Comparé aux deux autres mécanismes, il offre une interaction simplifiée basée sur des opérations de lecture/écriture dans des fichiers.
Ce mécanisme est, entre autres, utilisé par les \texttt{cgroups} pour communiquer avec l'espace utilisateur.

Nous pensons que MemOpLight doit rester simple d'utilisation, surtout que le contrôleur est la seule partie devant être écrite par l'utilisateur.
Une communication basée sur des entrées/sorties, comme l'offre le \texttt{sysfs}, nous semble donc être la meilleure solution.

\subsubsection{Utilisation du \texttt{sysfs}}
\label{subsubsec:sysfs}
Dans notre cas, nous avons rajouté, pour chaque instance du \texttt{cgroup} mémoire, un fichier \texttt{memory.color}.
Ce fichier est écrit par le contrôleur du conteneur pour y indiquer l'état de performance de ce dernier.
Pour ce faire, et comme le montre le code suivant, un simple \texttt{echo}, en tant que superutilisateur, suffit :
\begin{verbatim}
	GREEN=3
	echo $GREEN > /sys/fs/cgroup/memory/docker/${container_id}/memory.color
\end{verbatim}
La couleur est ensuite lue par notre mécanisme de réclamation qui prend place dans le noyau Linux.

\subsection{Exemples de sonde}
\label{subsec:applicative_probe_example}
Il est assez facile d'imaginer des contrôleurs applicatifs pour différentes applications.

Les performances d'un conteneur exécutant un serveur web seraient jugées par rapport à la latence de ses requêtes.
Ces latences sont récupérées depuis le fichier \texttt{access.log} du serveur.
À partir de celles-ci, le contrôleur peut calculer le 99e centile pour les requêtes reçues la dernière seconde.
Cette valeur est ensuite comparée à une latence de référence fournie par l'utilisateur.
Si la première est inférieure à la seconde, le conteneur est rouge, si elle est égale, il est jaune, sinon il est vert.
Cette couleur est ensuite transmise au noyau par le biais d'un moyen de communication, décrit dans la Section \ref{subsubsec:interface}.

Pour une base de données, la latence des requêtes est aussi examinée.
Celle-ci peut être lue en détournant le \texttt{slow\_query\_log} de \texttt{mysql} pour qu'il affiche les latences de toutes les requêtes \cite{noauthor_slowquerylog_nodate}.
Le reste du procédé est identique au contrôleur décrit ci-dessus.

Dans le cas d'une application de \textit{streaming}, la définition de l'image et/ou le nombre d'images par seconde peuvent être mesurés pour statuer sur les performances du conteneur.
Pour \texttt{OBS Studio}, le débit en termes d'images par seconde est disponible dans le code source \cite{jp9000_obs_2018}.
Il faudrait donc afficher cette valeur chaque seconde pour qu'elle puisse être lue par le contrôleur.
Similairement au serveur web, cette valeur est ensuite comparée à un débit de référence fourni par l'utilisateur; la couleur du conteneur découle alors de cette comparaison.

\section{Modifications apportées au noyau Linux}
\label{sec:kernel_modifications}
Le noyau permet d'avoir une vue globale de l'utilisation des ressources.
De plus, c'est lui qui est en charge de la gestion de la mémoire notamment quand celle-ci se fait rare.
Il est donc tout naturel que MemOpLight siège dans le noyau \cite{thesis_linux}.

Néanmoins, notre mécanisme doit avoir un temps de calcul limité, puisqu'il s'exécute en interrompant du code utilisateur, et avec une empreinte mémoire minimale, car il est invoqué lors d'une pression mémoire.
De plus, et pour simplifier la maintenabilité de notre code, nous avons limité le nombre de sous-systèmes modifiés.

Dans la suite de cette section, nous analysons, les différentes étapes de la boucle MAPE, montrées dans la Figure \ref{fig:memoplight}, siégeant dans le noyau.
Nous finissons par détailler les modifications apportées au noyau pour introduire notre mécanisme.

\subsection{Étape \textit{Monitor}}
\label{subsec:kernel_monitor}
Cette étape est double.
Premièrement, notre mécanisme n'est appelé que lorsque le système est en pression mémoire.
En effet, nous avons implémenté celui-ci dans la fonction \texttt{mem\_cgroup\_soft\_limit\_reclaim} invoquée par la \texttt{soft} limite \citelinux{mm/memcontrol.c}{2954}.
Secondement, cette étape va récupérer la couleur des \texttt{cgroups}, qui a été communiquée par le contrôleur applicatif, via le champ \texttt{color}.

\subsection{Étape \textit{Analysis}}
\label{subsec:kernel_analysis}
Cette phase procède à une filtration des conteneurs en fonction de leurs couleurs.
Comme le montre la boucle ligne 5 de l'Algorithme \ref{algo:memoplight}, les conteneurs verts et jaunes sont regroupés dans deux listes distinctes, \texttt{greens} et \texttt{yellows}, tandis que les conteneurs rouges sont dénombrés.

\begin{algorithm} % Algorithm.
	\SetAlgoLined

	\SetKwFunction{memoplight}{mem\_cgroup\_soft\_limit\_special\_reclaim}
	\SetKwProg{func}{Function}{}{}

	\func{\memoplight{}}{
		$reds = 0$\;
		$yellows = []$\;
		$greens = []$\;

		\tcc{Cette boucle compte le nombre de \texttt{cgroups} rouges et ajoute les autres aux listes correspondantes.}
		\For(\tcc*[h]{Pour chaque \texttt{cgroup} dans le système.}){$iter \in mem\_cgroups$}{
			\uIf(\tcc*[h]{Si les performances du \texttt{cgroup} sont mauvaises.}){$iter.color == RED$}{
				$reds++$\;
			}
			\uElseIf(\tcc*[h]{Si le \texttt{cgroup} a des performances satisfaisantes, mais pourrait faire mieux.}){$iter.color == YELLOW$}{
				$yellows.append(iter)$\;
			}
			\uElseIf(\tcc*[h]{Si les performances du \texttt{cgroup} sont les meilleures possibles.}){$iter.color == GREEN$}{
				$greens.append(iter)$\;
			}
		}

		$reclaimed = 0$\;
		$expected = 0$\;

		\tcc{La mémoire va être réclamée.}
		\If(\tcc*[h]{S'il y a au moins un \texttt{cgroup} rouge ou un jaune, les \texttt{cgroups} verts sont réclamés.}){$reds \lor len(yellows)$}{
			$green\_reclaimed, green\_expected = reclaim\_cgroup\_list(greens)$\;
			$reclaimed \mathrel{+}= green\_reclaimed$\;
			$expected \mathrel{+}= green\_expected$\;
		}

		\If(\tcc*[h]{S'il y a au moins un \texttt{cgroup} rouge, les jaunes sont aussi réclamés.}){$reds$}{
			$yellow\_reclaimed, yellow\_expected = reclaim\_cgroup\_list(yellows)$\;
			$reclaimed \mathrel{+}= yellow\_reclaimed$\;
			$expected \mathrel{+}= yellow\_expected$\;
		}

		\If(\tcc*[h]{Notre mécanisme s'arrête si suffisamment de mémoire a été réclamée.}){$expected \land reclaimed \geq expected$}{
			\Return{$reclaimed$}
		}
		\tcc{Sinon, il s'arrête et indique l'échec de la réclamation.}
		\Return{$0$}
	}

	\caption{L'algorithme de MemOpLight}
	\label{algo:memoplight}
\end{algorithm}

Noter que, puisque cet algorithme effectue un parcours de la liste de tous les \texttt{cgroups} existants, sa complexité est donc en $\mathcal{O}(n)$.

\subsection{Étape \textit{Plan}}
\label{subsec:kernel_plan}
En se basant sur le travail effectué dans la phase précédente, cette étape va décider quels conteneurs doivent être réclamés.
Plusieurs cas de réclamations sont possibles, ceux-ci sont compilés dans le Tableau \ref{table:reclaim_cases}.

\begin{table}
	\centering

	\begin{tabular}{ccp{.5\textwidth}}
		\hline
		\texttt{Cgroups} rouges & \texttt{Cgroups} jaunes & Conséquence\\
		\hline
		$0$ & $0$ & Aucun \texttt{cgroup} n'est réclamé.
		Soit le système n'a pas de \texttt{cgroup}, soit <<tout est au mieux>>.\\
		$0$ & $\ge 1$ & Les \texttt{cgroups} verts sont réclamés.\\
		$\ge 1$ & $0$ & Les \texttt{cgroups} verts sont réclamés.\\
		$\ge 1$ & $\ge 1$ & Les \texttt{cgroups} verts sont d'abord réclamés puis les jaunes.\\
		\hline
	\end{tabular}

	\caption{Les différents cas de MemOpLight}
	\label{table:reclaim_cases}
\end{table}

Pour résumer, notre mécanisme vise d'abord à éliminer les \texttt{cgroups} rouges, c'est-à-dire ceux n'ayant pas des performances satisfaisantes.
Puis, il améliore les performances des conteneurs jaunes, qui ont des performances satisfaisantes, mais pourraient faire mieux.

\subsection{Étape \textit{Execute}}
\label{subsec:kernel_execute}
C'est dans cette étape qu'a lieu la réclamation, si la phase \textit{Plan} a décidé qu'elle devait avoir lieu.
L'Algorithme \ref{algo:reclaim_cgroup_list} donne les étapes de la fonction \texttt{reclaim\_cgroup\_list}, en charge de récupérer de la mémoire aux \texttt{cgroups} d'une liste.

\begin{algorithm}
	\SetAlgoLined

	\SetKwFunction{reclaimcgrouplist}{reclaim\_cgroup\_list}
	\SetKwProg{func}{Function}{}{}

	\func{\reclaimcgrouplist{cgroups}}{
		$reclaimed = 0$\;
		$expected = 0$\;

		\For{$iter \in cgroups$}{
			\If(\tcc*[h]{Si le \texttt{cgroup} n'a pas été réclamé lors de la période courante.}){$\neg iter.reclaimed$}{
				\tcc{La quantité de mémoire à réclamer est calculée.}
				$to\_take = \frac{mem\_cgroup\_usage(iter, false)}{iter.divider}$\;
				\tcc{Puis le noyau essaye de libérer celle-ci.}
				$local\_reclaimed = try\_to\_free\_mem\_cgroup\_pages(iter, to\_take)$\;
				$reclaimed \mathrel{+}= local\_reclaimed$\;
				$expected \mathrel{+}= to\_take$\;
				\tcc{Le \texttt{cgroup} est marqué comme ayant été réclamé pendant cette période. La sonde se chargera de mettre à \texttt{false} ce drapeau lors de la prochaine période.}
				$iter.reclaimed = true$\;
			}
		}
		\Return $(reclaimed, expected)$
	}

	\caption{La fonction en charge de réclamer les \texttt{cgroups} d'une liste}
	\label{algo:reclaim_cgroup_list}
\end{algorithm}

Cette fonction réclame, pour chaque \texttt{cgroup} de la liste, une fraction de son empreinte mémoire, fraction pouvant être changée à la volée pour chaque \texttt{cgroup}.
La réclamation s'effectue réellement dans la fonction \texttt{try\_to\_free\_mem\_cgroup\_pages} \citelinux{mm/vmscan.c, 3298}.
Cette fonction, offerte par le noyau Linux, va agir sur le \textit{page cache} du \texttt{cgroup}, comme résumé dans la Section \ref{subsec:memory_reclaim}, pour libérer des pages.
L'utilisation de cette fonction nous permet de limiter l'intrusivité de notre mécanisme tout en rendant notre code résistant aux changements de l'implémentation du \textit{page cache}.

Une fois que le \texttt{cgroup} est réclamé, il est marqué comme tel.
Ainsi, il ne pourra pas être à nouveau victime du mécanisme pendant la période en cours.
Nous revenons sur ce fait dans la sous-section suivante.

\subsection{Modifications apportées au noyau Linux}
\label{subsec:kernel_modifications}
Notre mécanisme prend place dans la fonction \texttt{mem\_cgroup\_soft\_limit\_reclaim} invoquée par la \texttt{soft} limite \citelinux{mm/memcontrol.c}{2954}.
L'Algorithme \ref{algo:mem_cgroup_soft_limit} montre les modifications que nous avons apportées à celle-ci.

\begin{algorithm}
	\SetAlgoLined

	\SetKwFunction{timer}{timer}
	\SetKwFunction{softlimitreclaim}{mem\_cgroup\_soft\_limit\_reclaim}
	\SetKwProg{func}{Function}{}{}
	\SetKwProg{proc}{Procedure}{}{}

	$mem\_op\_light\_lock = init\_lock()$\;

	\proc{\timer{}}{
		$unlock(mem\_op\_light\_lock)$\;
	}

	\func{\softlimitreclaim{}}{
		\uIf(\tcc*[h]{Si le verrou est libre, soit MemOpLight n'est pas en cours d'exécution, soit sa dernière exécution remonte à la période précédente.}){$lock(mem\_op\_light\_lock)$}{
			$reclaimed = mem\_cgroup\_soft\_limit\_special\_reclaim()$\;
			\If{$reclaimed$}{
				\tcc{Un compte à rebours invoquera la fonction \texttt{timer} lors de la prochaine période.}
				$set\_timer(timer, 1)$
				\tcc{Si suffisamment de mémoire a été réclamée, la fonction est quittée. MemOpLight et la \texttt{soft} limite ne s'activeront pas jusqu'à la fin de cette période.}
				\Return $reclaimed$
			}
			$unlock(mem\_op\_light\_lock)$\;
		}
		\uElse{
			\tcc{Soit MemOpLight est en cours d'exécution soit il a déjà été invoqué cette période-ci.}
			\Return $0$
		}
		\tcc{La fonction continue en appelant la \texttt{soft} limite.}
	}

	\caption{Les modifications apportées à \texttt{mem\_cgroup\_soft\_limit\_reclaim}}
	\label{algo:mem_cgroup_soft_limit}
\end{algorithm}

Nous avons modifié cette fonction de sorte que MemOpLight soit appelé au plus une fois par période\sidenote{Par défaut, une période dure une seconde.}.
Ainsi, notre surcoût a, au pire, une complexité en $\mathcal{O}(n)$ chaque période.
Si MemOpLight a réclamé suffisamment de mémoire, ni la \texttt{soft} limite ni lui ne seront rappelés pendant cette période.
De cette façon, la \texttt{soft} limite ne vient pas détruire ce que MemOpLight a accompli.
Si MemOpLight n'a, malheureusement, pu réclamer suffisamment de mémoire, la \texttt{soft} limite prend le relais.

\section{Conclusion}
\label{sec:memoplight_conclusion}
Nous venons, dans ce chapitre, de présenter MemOpLight.
Comparée aux mécanismes existants, notre solution apporte au noyau Linux une réclamation basée sur les besoins des applications et non sur des limites statiques.
Ainsi, MemOpLight garantit l'isolation et la consolidation mémoire.

De plus, nous avons restreint le caractère intrusif de notre mécanisme, car les modifications apportées au noyau Linux ne touchent que 2 fichiers dans un même sous-système.
Nous avons aussi limité le surcoût de MemOpLight puisque sa complexité est en $\mathcal{O}(n)$ et qu'il s'exécute au plus une fois par période.
Concernant les contrôleurs applicatifs, puisque ceux-ci accèdent à des informations déjà disponibles et informent le noyau au plus une fois par seconde nous avons la certitude que leur surcoût est faible.

Dans le prochain chapitre, nous mettons à rude épreuve MemOpLight d'abord en le comparant aux mécanismes existants.
Puis nous faisons varier ses propres paramètres pour trouver une configuration optimale de ceux-ci.