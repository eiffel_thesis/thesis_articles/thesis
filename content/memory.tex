\chapter{La mémoire}
\label{chapter:memory}
\section{Introduction}
\label{sec:memory_intro}
Dans ce chapitre, nous allons étudier la mémoire telle qu'utilisée en informatique.
La mémoire est définie ainsi par le wiktionnaire \cite{wiki_memory} :
\begin{quote}
	Capacité à retenir, conserver et rappeler de nombreuses informations antérieures.
\end{quote}
Un ordinateur a notamment besoin d'une telle capacité afin de se rappeler l'état courant des données manipulées ainsi que les instructions à exécuter.

Les différents composants d'un ordinateur possèdent presque tous un dispositif permettant la mémorisation.
Ceux-ci divergent de par leurs réalisations et utilisations.
Nous verrons donc, dans un premier temps, comment les dispositifs de mémorisation d'un ordinateur s'articulent et quels sont les compromis entre ceux-ci.

Les systèmes d'exploitation permettent à plusieurs applications de s'exécuter simultanément.
Pour ce faire, ils garantissent un certain niveau d'isolation entre ces dernières.
Nous regardons alors en détail comment cette isolation est mise en place par Linux \cite{kroah_hartman_linux_2018}.

Malgré l'isolation des applications, les systèmes d'exploitation offrent des mécanismes permettant de consolider la mémoire afin d'offrir de meilleures performances aux applications et une équité de celles-ci dans la réclamation de leurs mémoires.
Nous étudions donc sous quelle forme la consolidation est offerte par Linux.

\section{La mémoire d'un point de vue physique}
\label{sec:physical memory}
Dans cette section, nous étudions les différents dispositifs sur lesquels s'appuient les composants d'un ordinateur pour mémoriser des informations.
Nous présentons d'abord l'architecture d'un ordinateur moderne avant de nous intéresser plus particulièrement à la mémoire dynamique.

\subsection{Architecture d'un ordinateur moderne}
\label{subsec:modern_architecture}
L'architecture des ordinateurs modernes, désormais quasiment commune aux mobiles multifonctions, hérite de l'architecture dite de von Neumann \cite{von_neumann_first_1993}.
Une représentation de cette architecture est donnée dans la Figure \ref{fig:von_neumman}.

\begin{figure}
	\centering

	\includegraphics{von_neumann.pdf}

	\caption[Architecture dite de von Neumann]{Architecture dite de von Neumann \cite{wiki_von_neumann}}
	\label{fig:von_neumman}
\end{figure}

Cette architecture repose sur les éléments suivants :
\begin{description}
	\item[Unité arithmétique et logique :] Cette unité est en charge des opérations de calcul comme les additions ou multiplications.
	\item[Unité de contrôle :] Après l'exécution d'une instruction, c'est cette unité qui est chargée de faire passer le programme à l'instruction suivante.
	\item[Unité de mémoire :] Les instructions et les données manipulées par l'unité arithmétique et logique sont stockées dans cette mémoire.
	L'architecture dite de von Neumann tient son originalité du fait que les données et les instructions sont stockées dans la même mémoire; contrairement à l'architecture Harvard où ces informations sont conservées dans des mémoires séparées \cite{arm_general_2009}.
	\item[Périphérique d'entrée :] Ce type de périphérique envoie des informations à l'unité arithmétique et logique.
	Une souris ou un clavier sont de parfaits exemples de tels périphériques.
	\item[Périphérique de sortie :] Ces périphériques permettent à l'unité arithmétique et logique de présenter des informations.
	Un écran est un périphérique de sortie.
\end{description}
Notez toutefois que certains composants, tels que les disques durs et les cartes réseau, sont à la fois des périphériques d'entrée et de sortie.

Dans un ordinateur, l'unité de contrôle et l'unité arithmétique et logique font partie du même composant : le processeur \cite[Section 2.2.2]{intel_basic_2019}.
C'est donc ce composant qui exécute les instructions permettant la réalisation des applications.
Pour ce faire, le processeur s'appuie sur des registres lui permettant de stocker le résultat des instructions et de les réutiliser \cite[Section 3.4.1]{intel_basic_2019}.
Ces registres sont néanmoins petits, \SI{64}{\bit} pour une architecture moderne, et en nombre limité.

L'unité de mémoire est organisée de manière hiérarchique.
Au plus proches du processeur, les mémoires sont rapides, mais petites.
Plus ces mémoires sont distantes du processeur, plus elles sont volumineuses, mais lentes.
Ce compromis, connu comme la pyramide mémoire, est dépeint dans la Figure \ref{fig:pyramid}

\begin{figure}
		\centering

		\includegraphics[scale = .42]{pyramide.pdf}

		\caption{La pyramide mémoire}
		\label{fig:pyramid}
\end{figure}

Les processeurs embarquent, en plus des registres, plusieurs niveaux de cache contenant des copies des dernières données et instructions utilisées \cite[Figure 2-1]{intel_basic_2019}\cite[Section 5.2.1]{jacob_memory_2007}.
Ces caches ont une taille finie, de l'ordre du \si{\mega\byte} \cite{amd_amd_2019}.
Au-dessus de ces caches se trouve la mémoire dynamique, extérieure au processeur, se présentant sous forme de barrettes.
Ces barrettes de mémoire comportent des puces composées de plusieurs milliards de cellules permettant, chacune, de stocker un \si{\bit}.
L'organisation interne de ces puces est très différente de celle des registres et des caches du processeur \cite[Section 8.2]{jacob_memory_2007}.

Toutes les mémoires que nous avons présentées jusqu'ici sont des mémoires vives.
C'est-à-dire que les données stockées sont perdues lorsque ces mémoires ne sont plus sous tension.
Ainsi, et pour stocker des données sur le long terme, l'ordinateur peut s'appuyer sur des disques, que ce soit des disques durs ou des disques statiques à semi-conducteurs.
Dans les premiers, les données sont stockées sous la forme d'un champ magnétique \cite[Section 17.1.1]{jacob_memory_2007}.
Tandis que les seconds stockent les bits dans la grille flottante des transistors \cite[Section 2.1.2]{boukhobza_flash_2017}.

Toutes ces mémoires s'articulent entre elles selon deux axes : la capacité de stockage et la vitesse d'accès.
Comme nous l'avons évoqué, les registres et caches du processeur sont très rapides, un accès ne prend que quelques \si{\nano\second}, mais aussi très petits, quelques \si{\mega\byte} \cite{amd_amd_2019}.
De l'autre côté, les disques peuvent atteindre plusieurs \si{\tera\byte} mais sont beaucoup plus lents, l'accès se fait dans l'ordre de la \si{\milli\second} pour les plus lents \cite{fisher_what_2019}.

\subsection{La mémoire dynamique}
\label{subsec:dram}
Dans cette thèse, nous nous intéressons plus particulièrement à la mémoire dynamique.
Dans les différents composants que nous avons présentés dans la Section \ref{subsec:modern_architecture}, la mémoire dynamique se trouve dans les puces situées sur les barrettes de mémoire.
Aujourd'hui, il est plus courant de parler de mémoire dynamique à accès aléatoire, ou \textit{Dynamic Random Access Memory} (DRAM).

Cette mémoire est dite dynamique, car son contenu nécessite d'être périodiquement rafraîchi sous peine d'être perdu \cite[Section 8.2.1]{jacob_memory_2007}.
En effet, une cellule, l'élément permettant de stocker un bit, s'appuie sur un condensateur dont la charge fuit au cours du temps \cite[Figure 8.2]{jacob_memory_2007}.
Contrairement aux cellules des caches et registres processeurs qui s'appuient uniquement sur des transistors et qui sont donc dites statiques \cite[Figure 5.2]{jacob_memory_2007}.
La Figure \ref{fig:dram_cell} montre une cellule de mémoire dynamique composée d'un condensateur et d'un transistor d'accès.

\begin{figure}
	\centering

	\includegraphics[scale = .42]{dram_cell.pdf}

	\caption[Une cellule de DRAM]{Une cellule de DRAM \cite[Figure 8.2]{jacob_memory_2007}}
	\label{fig:dram_cell}
\end{figure}

Ces cellules sont organisées en matrices qui sont ensuite regroupées en plusieurs blocs.
Dans ce manuscrit, nous n'entrerons pas dans le détail de l'organisation interne d'une barrette de DRAM.
Néanmoins, la Figure \ref{fig:ddr4} présente l'agencement des différents composants tel que standardisé par le \textit{Joint Electron Device Engineering Council} (JEDEC) pour la DDR4 \cite{jedec_ddr4_2017}.

\begin{figure}
	\centering

	\includegraphics[scale = .28]{DDR4.pdf}

	\caption[L'organisation de la DDR4 telle que standardisée par le JEDEC]{L'organisation de la DDR4 telle que standardisée par le JEDEC \cite{jedec_ddr4_2017}}
	\label{fig:ddr4}
\end{figure}

De plus, la DRAM n'a pas été sujette aux mêmes progrès que les processeurs \cite{wulf_hitting_1995}.
Elle peut donc être un frein aux performances des applications, même si des progrès ont été faits pour contrer sa faible fréquence et augmenter sa bande passante \cite{allan_ddr4_nodate}.
Ainsi, une gestion efficace de la mémoire par le système d'exploitation s'avère cruciale pour les performances des applications.
Nous allons donc voir comment les systèmes d'exploitation modernes, Linux plus particulièrement, gèrent la mémoire.

\section{L'isolation garantie par le système d'exploitation}
\label{sec:isolation}
Aujourd'hui, les systèmes d'exploitation permettent l'exécution simultanée de plusieurs applications.
Néanmoins, les données manipulées par celles-ci doivent être strictement privées pour éviter bogues et indiscrétions.
Le mécanisme de mémoire virtuelle, basé sur des possibilités offertes par le matériel, permet d'isoler les applications entre elles.

Dans cette section, nous commençons par présenter le concept de mémoire virtuelle ainsi que celui de mémoire paginée.
Puis, nous examinons, sans entrer dans le détail, les structures de données utilisées par Linux pour représenter différents concepts propres à la mémoire virtuelle paginée.
Ensuite, nous étudions brièvement les capacités d'isolation offertes par les hyperviseurs.
Pour terminer, nous nous attardons sur les conteneurs en les étudiant au niveau utilisateur et noyau.

\subsection{La mémoire virtuelle paginée}
\label{subsec:paged_virtual_memory}
Linux est un système d'exploitation basé sur le concept de mémoire virtuelle \cite{denning_virtual_1970}.
Dans ce concept, les processus ne manipulent pas des adresses physiques, mais des adresses virtuelles.
Un processus a donc l'illusion qu'il dispose de toute la mémoire, en plus d'être isolé de ses congénères.

La mémoire virtuelle peut être divisée en unités insécables appelées <<pages>> \cite[Section 3.3.2]{intel_basic_2019}.
Dans Linux, la mémoire virtuelle est paginée, une page est donc la plus petite unité mémoire qu'il puisse allouer à une tâche\sidenote{
	Linux ne fait pas la différence entre un processus et un \textit{thread} \cite[Section "The Linux Implementation of Threads"]{love_linux_2010}.
	Pour lui, des \textit{threads} sont des tâches partageant des données.
}.
Une page virtuelle est associée à une page physique.
Les droits d'accès d'une page physique, par exemple, lecture seule ou lecture/écriture, sont gérés via des drapeaux propres au matériel \cite[Table 4-19]{intel_sys_prog_2019}\cite[Chapter 3]{gorman_understanding_2004}.

Une association entre une page virtuelle et physique est rangée dans une structure, propre à chaque tâche, nommée la table des pages.
Le format de cette table est imposé par le matériel puisque c'est la \textit{Memory Management Unit} (MMU) qui est chargée d'effectuer la traduction d'une adresse virtuelle en une adresse physique.
Cette table n'est pas contiguë et est découpée en plusieurs niveaux, ce pour limiter sa taille.
Chacun des niveaux intermédiaires est une page.
Lors d'une traduction, l'adresse virtuelle est découpée en plusieurs parties.
Les bits de chacune de ces parties sont utilisés pour accéder au niveau correspondant dans la table des pages.

La Figure \ref{fig:page_table} montre l'organisation d'une table des pages à 5 niveaux pour l'architecture x86\_64 \cite[Figure 2-1]{intel_5_level_2017}.
L'adresse du plus haut niveau de la table des pages, PML5, de la tâche courante est stockée dans le registre \texttt{cr3}.
L'adresse virtuelle\sidenote{
	Dans la Figure \ref{fig:page_table}, le terme <<\textit{Linear Address}>> correspond à <<adresse virtuelle>>.
} est découpée en 5 parties de 9 bits.
Par exemple, si les bits de la partie PML5 ont pour valeur décimale 484, l'entrée 484 du PML5 sera accédée pour passer au PML4 correspondant.
Les bits de poids faible de l'adresse virtuelle ne sont pas utilisés pour la traduction et correspondent au déplacement à effectuer dans la page physique pour obtenir la donnée demandée.
Ces bits sont donc communs à une adresse virtuelle et une adresse physique.

\begin{figure}
	\centering

	\includegraphics[scale = .42]{5_level_page_table.pdf}

	\caption[Table des pages à 5 niveaux]{Table des pages à 5 niveaux \cite[Figure 2-1]{intel_5_level_2017}}
	\label{fig:page_table}
\end{figure}

Linux, dans son implémentation, distingue deux types de pages :
\begin{description}
	\item[pages fichiers :] Ces pages contiennent des données qui ont été récupérées depuis le disque.
	En effet, quand une donnée sur le disque est demandée, Linux récupère plusieurs données à la fois et les stocke en mémoire physique à l'aide d'une page.
	Ainsi, si la donnée demandée est à nouveau accédée, ou si une donnée proche l'est, elle pourra être lue depuis la mémoire physique.
	Ce qui permet alors un gain de temps considérable par rapport à une lecture depuis le disque \cite{scott_latency_2020}.
	\item[pages anonymes :] Ces pages contiennent des données qui ont été créées lors de l'exécution d'un programme.
	Par exemple, les pages utilisées pour la pile ou le tas sont des pages anonymes.
\end{description}
Malgré cette distinction, Linux s'efforce d'appliquer des algorithmes similaires à toutes les pages.

\subsection{Espace d'adressage virtuel d'une tâche}
\label{subsec:virtual_space}
Dans l'espace d'adressage virtuel d'une tâche, les pages sont regroupées en région.
Une région est une partie contiguë de l'espace d'adressage virtuel.
Elle regroupe des pages de même type ayant des droits d'accès communs.
Par exemple, il y aura une région pour la pile, une pour le tas et une autre pour un fichier projeté en mémoire suite à l'invocation de l'appel système \texttt{mmap}.

Une région peut être créée suite à un appel à \texttt{mmap}\sidenote{
	\texttt{mmap} est appelé par \texttt{malloc} \cite{sploitfun_syscalls_2015}.
}.
Suite à la création d'une nouvelle tâche, via un appel à \texttt{fork}, la tâche enfant hérite des régions de son parent \cite[Section "Copy On Write"]{gorman_understanding_2004}.

Les différentes régions d'une même tâche sont regroupées dans l'espace d'adressage virtuel de celle-ci.
Il existe un espace d'adressage virtuel pour chaque tâche.
Cet espace est associé à la table des pages de la tâche.

\begin{figure}
	\centering

	\subfloat[La mémoire des tâches n'est pas limitée]{
		\includegraphics[scale = .42]{processus_memory.pdf}

		\label{figs:processus_memory}
	}

	\subfloat[La quantité maximale de mémoire qu'une machine virtuelle peut utiliser est fixée]{
		\includegraphics[scale = .42]{vm_memory.pdf}

		\label{figs:vm_memory}
	}

	\subfloat[L'empreinte mémoire d'un conteneur ne peut pas dépasser une certaine valeur]{
		\includegraphics[scale = .42]{container_memory.pdf}

		\label{figs:container_memory}
	}

	\caption{Organisation de la mémoire pour différentes solutions}
	\label{fig:memory_organization}
\end{figure}

Comme le montre la Figure \ref{figs:processus_memory}, les différentes tâches partagent la mémoire physique de la machine et ne sont limitées que par celle-ci.

\subsection{L'isolation poussée à l'extrême : les hyperviseurs}
\label{subsec:hypervisor}
Un hyperviseur est un logiciel permettant d'exécuter des VM, il en existe deux types \cite{popek_formal_1974}\cite[Section 1.4]{bugnion_hardware_2017} :
\begin{description}
	\item[Hyperviseur de type 1 :] Il s'exécute à même le matériel, il remplace donc le système d'exploitation.
	\texttt{xen} est un bon exemple d'hyperviseur de ce type \cite{noauthor_xen_nodate}.
	\item[Hyperviseur de type 2 :] Ce type d'hyperviseur s'exécute par-dessus un système d'exploitation, c'est donc un processus appartenant à un système d'exploitation.
	\texttt{qemu} peut être cité comme hyperviseur de type 2 \cite{noauthor_qemu_nodate}.
\end{description}
La Figure \ref{fig:hypervisors} dépeint la différence entre les deux types d'hyperviseurs.

\begin{figure}
	\centering

	\subfloat[Hyperviseur de type 1]{
		\includegraphics[scale = .42]{hypervisor_type1.pdf}

		\label{fig:hypervisor_type1}
	}
	\subfloat[Hyperviseur de type 2]{
		\includegraphics[scale = .42]{hypervisor_type2.pdf}

		\label{fig:hypervisor_type2}
	}

	\caption{Schémas de l'organisation des deux types d'hyperviseurs}
	\label{fig:hypervisors}
\end{figure}

Un hyperviseur permet, en plus d'exécuter un système de manière isolée, de restreindre les ressources qui lui sont allouées.
Ainsi, au démarrage du système invité, il est possible de l'exécuter sur tel nombre de cœurs ou avec telle quantité de mémoire.
Cette fonctionnalité est donc à la base du concept d'instance \cite{amazon_types_nodate}.
La Figure \ref{figs:vm_memory} montre bien que, contrairement à la Figure \ref{figs:processus_memory}, la mémoire de chaque VM est bornée.
Ainsi, une VM ne peut s’accaparer toute la mémoire.

Un hyperviseur doit aussi gérer la mémoire du système invité.
En effet, le système invité possède ses propres tables des pages, or celui-ci n'a pas accès directement à la mémoire.
Ses tables des pages associent donc une adresse virtuelle invitée à une adresse physique invitée \cite[Section 8.3.1]{smith_virtual_2005}.
La traduction d'une adresse virtuelle invitée en une adresse physique de l'hôte est alors réalisée par l'hyperviseur au moyen des \textit{shadow pages tables} \cite[Section 8.3.2]{smith_virtual_2005}.

\subsection{Sous les conteneurs : les \texttt{cgroups}}
\label{subsec:container}
Comme nous l'avons vu dans l'introduction, les conteneurs présentent une alternative plus légère aux VM.
Comme le montre la Figure \ref{fig:docker_vs_vm}, les conteneurs, contrairement aux VM, ne s'appuient pas sur un système d'exploitation invité : c'est l'application uniquement qui est conteneurisée \cite{docker_what_nodate, zhang_comparative_2018}.

Les conteneurs proposent principalement trois services :
\begin{description}
	\item[Sécurité :] Les données d'un conteneur ne peuvent être accédées par un autre conteneur sauf partage explicite.
	\item[Contrôle des ressources :] Il est possible d'allouer finement des ressources à un conteneur, comme un cœur de processeur ou un octet de la mémoire physique.
	Contrairement aux tâches conventionnelles, l'empreinte mémoire d'un conteneur pourra être bornée par une valeur fixée par l'utilisateur.
	\item[Facilité de déploiement :] Lancer un conteneur est aussi simple qu'exécuter une commande \texttt{shell}.
\end{description}

Dans les sous-sections qui suivent, nous présentons, dans un premier temps, l'organisation logicielle des conteneurs \texttt{docker} \cite{docker_docker_nodate}.
Il n'existe pas, dans Linux, de structure de données correspondant à un conteneur.
Ainsi, les conteneurs forment une abstraction basée sur plusieurs fonctionnalités offertes par les systèmes d'exploitation.
Nous détaillons donc, dans un deuxième temps, deux de ces fonctionnalités : les \texttt{namespaces} et les \texttt{cgroups}.

\subsubsection{Les conteneurs \texttt{docker}}
\label{subsubsec:docker}
\texttt{Docker} est, comme \texttt{lxc}, \texttt{kata} et \texttt{podman}, un des nombreux logiciels existants permettant d'exécuter et gérer des conteneurs \cite{docker_docker_nodate, noauthor_lxc_2008, noauthor_kata_2017, noauthor_podman_nodate}.

Un conteneur est une application exécutée avec toutes ses dépendances et ses données.
Celles-ci sont stockées dans une image de l'application.
Ainsi, au démarrage du conteneur, \texttt{docker} va extraire ces données de l'image, notamment le code de l'application afin de l'exécuter.

Ces images peuvent être récupérées en ligne \cite{docker_hub}.
Il existe des images de base, comme \texttt{debian}, pouvant être utilisées pour construire de nouvelles images.
En effet, l'utilisateur peut installer de nouveaux logiciels dans un conteneur et le sauvegarder comme une nouvelle image.
Celle-ci peut ensuite être utilisée par l'utilisateur pour exécuter son service personnalisé.
Une image peut être créée sans avoir à exécuter de conteneur auparavant en utilisant un \texttt{dockerfile} \cite[Section "Introducing the Dockerfile"]{mckendrick_mastering_2017}.

Originellement \texttt{docker} était un système monolithique.
Il est désormais, dans un système GNU/Linux, composé des briques logicielles suivantes \cite{porterie_docker_2016} :
\begin{description}
	\item[docker-cli :] Cette brique contient les outils en ligne de commande permettant, entre autres, la création, l'exécution, l'arrêt ou la sauvegarde d'un conteneur \cite{docker_docker_nodate-1} .
	\item[moby :] \texttt{moby} gère les différents volumes et réseaux utilisés par les conteneurs.
	Un volume est un système de fichier pouvant être attaché à un conteneur.
	Ainsi, deux conteneurs différents peuvent partager des fichiers si un même volume leur est attaché.

	Ce démon, un processus s'exécutant en arrière-plan, est aussi en charge de l'orchestration des conteneurs.
	L'orchestration consiste à exécuter des conteneurs sur différentes machines tout en leur permettant de communiquer \cite{noauthor_moby_nodate, crosby_what_2017}.
	\item[containerd :] Ce démon propose l'abstraction des conteneurs.
	Il gère aussi les images que ce soit leurs créations ou leurs sauvegardes \cite{containerd_containerd_nodate, crosby_what_2017} .
	\item[runc :] Cette brique dialogue avec le système d'exploitation pour créer, lancer ou détruire des conteneurs.
	Elle contient des outils en ligne de commande permettant de manipuler des conteneurs.
	Pour ce faire, \texttt{runc} s'appuie sur la \texttt{libcontainer} qui exécute ce qui a été demandé par la ligne de commande.
	C'est au travers de cette bibliothèque que \texttt{containerd} communique avec \texttt{runc} \cite{opencontainers_runc_nodate, hykes_introducing_2015}.
	Lors du lancement d'un nouveau conteneur, \texttt{runc} instancie un nouveau \texttt{cgroup}, pour chaque sous-système, et un nouveau \texttt{namespace}, pour chaque ressource à abstraire.
\end{description}

Ces différentes briques sont organisées comme décrit dans la Figure \ref{fig_docker}.

\begin{figure}
	\centering

	\includegraphics[scale = .42]{docker_org.pdf}

	\caption{Organisation logicielle des différentes briques composant \texttt{docker}}
	\label{fig_docker}
\end{figure}

\subsubsection{Les \texttt{namespaces}}
\label{subsubsec:namespace}
Pour la sécurité, les conteneurs s'appuient sur les \texttt{namespaces} \cite{rami_rosen_namespace_2016, kerrisk_lce_2012, kerrisk_namespaces_2013}.
Un \texttt{namespace} abstrait une ressource système globale.
La finalité étant de faire croire aux tâches appartenant à une instance de ce \texttt{namespace} qu'elles possèdent leur propre exemplaire, isolé, d'une ressource donnée.

Il existe de nombreux \texttt{namespaces} dont le \texttt{mount namespace} et le \texttt{PID namespace}.
Le premier permet qu'un système de fichier ne soit monté que pour les tâches appartenant au \texttt{mount namespace} donné.
Il devient donc possible d'imaginer deux instances du \texttt{mount namespace} avec des tâches inscrites dans chacun et accédant à deux systèmes de fichiers complètement différents.
Les processus du premier \texttt{mount namespace} ne voyant pas le second système de fichier, ils ne pourront donc pas accéder à ses fichiers.

Quant au second \textit{namespace}, il offre la possibilité d'avoir des tâches ayant le même <<Process ID>> (PID), l'identifiant unique d'un processus, dans plusieurs \texttt{PID namespaces}.
Avec ce \texttt{namespace}, il peut y avoir plusieurs tâches de PID 1, c'est-à-dire plusieurs \texttt{init} \cite{corbet_notes_2007}.

\subsubsection{Les \texttt{cgroups}}
\label{subsubsec:cgroup}
Le contrôle fin des ressources des conteneurs est rendu possible par les \textit{control groups} (\texttt{cgroups}) \cite{rami_rosen_namespace_2016, hiroyu_cgroup_2008}.
Comme le montre la Figure \ref{figs:container_memory}, les \texttt{cgroups} peuvent, entre autres, restreindre la quantité de mémoire qu'un conteneur peut utiliser.
Le mécanisme des \texttt{cgroups} a fait ses premiers pas dans le noyau en 2006 \cite{corbet_another_2006}.
Néanmoins, ce n'est qu'en 2007, suite à son inclusion dans la version 2.26.24, qu'il gagne son nom de \texttt{cgroup}, le nom \textit{containers} ayant été jugé trop générique \cite{corbet_process_2007, corbet_notes_2007}.

Néanmoins, l'implémentation des \texttt{cgroups} a été la cible de plusieurs critiques.
En 2012, Jonathan Corbet écrivait ceci à leurs propos \cite{corbet_fixing_2012} :
\begin{quote}
	Control groups are one of those features that kernel developers love to hate.
\end{quote}
L'un des principaux reproches adressés aux \texttt{cgroups} visait alors l'organisation hiérarchique de ceux-ci.
L'implémentation des \texttt{cgroups} a donc été modifiée pour donner naissance à \texttt{cgroup v2} \cite{rosen_understanding_2016, heo_control_2015}.
Avec cette nouvelle version, l'organisation des différents \texttt{cgroups} est simplifiée, il n'est, entre autres, plus possible d'avoir des tâches appartenant à un \texttt{cgroup} nœud.

Cette nouvelle version n'est par contre pas encore utilisée par tous les logiciels permettant de lancer des conteneurs \cite{suda_current_2019, sweeney_first_2019}.
En effet, \texttt{docker}, que nous utilisons dans cette thèse, n'est pas encore compatible avec \texttt{cgroup v2} tandis que \texttt{podman} l'est.

Les \texttt{cgroups} permettent de grouper plusieurs tâches avec un ensemble de paramètres pour un ou plusieurs sous-systèmes \cite{noauthor_control_nodate}.
Un sous-système étant défini comme un module souhaitant appliquer un certain traitement à un ensemble de tâches.
Dans la pratique, un sous-système est un contrôleur de ressource permettant d'ordonnancer une ressource ou d'appliquer des limites à l'utilisation de celle-ci à un \texttt{cgroup} donné.
Les \texttt{cgroups} permettent aussi de comptabiliser l'utilisation des ressources faites par un ensemble de tâches.

De base, une tâche appartient à un \texttt{cgroup} particulier, le \texttt{cgroup root}, pour chaque sous-système.
Le \texttt{cgroup root} a la particularité de ne pas être limité dans son usage des ressources.
Pour limiter l'usage d'une tâche existante, il faut inscrire son identifiant dans le \texttt{cgroup} correspondant.

Tout comme les \texttt{namespaces}, plusieurs sous-systèmes existent pour les \texttt{cgroups}.
Le \texttt{cgroup} \texttt{cpuset} permet de restreindre l'exécution d'une instance d'un \texttt{cgroup} sur un nombre de cœurs donné \cite{cpusets_cgroup}.
Le \texttt{cgroup} \texttt{process number} offre la possibilité de limiter le nombre de tâches d'un \texttt{cgroup} \cite{process_number_cgroup}.
Ainsi, il est possible d'obtenir à tout moment le nombre de tâches d'un \texttt{cgroup} et de limiter l'explosion d'une \texttt{fork bomb} \cite{wiki_fork_bomb}.

L'existence d'un sous-système pour chaque ressource n'implique pas que toutes ces ressources sont aussi faciles à gérer les unes que les autres.
En effet, la part de temps CPU est assez simple à gérer, soit un \texttt{cgroup} utilise sa part soit il ne l'utilise pas.
La gestion de la mémoire est par contre plus complexe.
En effet, un \texttt{cgroup} peut utiliser, ou non, la totalité de sa mémoire, mais il n'est pas possible de savoir si les données stockées dans celle-ci lui sont utiles ou non.

\section{Les mécanismes de consolidation offerts par les systèmes d'exploitation}
\label{sec:consolidation}
Malgré l'existence de mécanisme d'isolation que nous venons de présenter, Linux a fait le choix d'une gestion commune de la mémoire dans trois situations :
\begin{enumerate}
	\item Lors de l'allocation mémoire, Linux utilise un ensemble de pages libres commun à toutes les applications.
	Ainsi, aucune application n'est prioritaire pour obtenir de nouvelles pages.
	\item La seconde est celle où des blocs lus depuis le disque sont ramenés dans la mémoire principale.
	Ce cache est partagé par toutes les applications.
	\item Lors d'une réclamation mémoire, quand celle-ci se fait rare, les applications sont toutes réclamées de la même manière.
	Linux garantit donc une équité totale entre ces dernières.
\end{enumerate}

Dans la suite de cette section, nous étudions comment Linux alloue la mémoire aux différentes applications.
Puis, nous analysons le fonctionnement du \textit{page cache} de Linux ainsi que l'algorithme exécuté lors d'une phase de réclamation mémoire.
Nous continuons par une brève étude du mécanisme de \textit{memory ballooning} offert par les hyperviseurs pour consolider la mémoire entre plusieurs VM.
Nous finissons par détailler les spécificités apportées par le \texttt{cgroup} mémoire dans la gestion mémoire.

\subsection{Allocation mémoire}
\label{subsec:memory_allocation}
Lors d'un appel à \texttt{malloc}, aucune page physique n'est associée à une page virtuelle, mais elles sont seulement marquées comme pouvant être utilisées.
En effet, Linux alloue les pages physiques de manière paresseuse, c'est-à-dire qu'une page physique est allouée uniquement quand elle est touchée par la tâche qui souhaite l'utiliser \cite{ott_virtual_2016}.

Le premier accès à une page non allouée déclenche une exception, plus communément appelée <<défaut de page>> \cite[Section 4.7]{intel_sys_prog_2019}.
Si l'accès est licite, entre autres quand l'adresse virtuelle ayant déclenché le défaut de page appartient bien à une des régions de l'espace d'adressage virtuel de la tâche, une page est allouée à la tâche.
Sa table des pages est aussi modifiée afin que l'adresse virtuelle soit désormais associée à une adresse physique.
Le prochain accès à cette même adresse virtuelle ne causera donc plus de défaut de page.

Pour allouer une page, Linux s'appuie sur un allocateur nommé le \textit{buddy allocator} \cite[Figure 6.1]{gorman_understanding_2004}.
Cet allocateur permet de répondre à des allocations de plusieurs pages contiguës tout en réduisant la fragmentation externe \cite[Section 6.6]{gorman_understanding_2004}.
Il repose sur un tableau où chaque case est une liste de blocs de pages.
D'une manière générale, la case $n$ pointe sur une liste de blocs de $2^n$ pages contiguës.
Par exemple, la liste stockée dans la case 5 contient des blocs de $2^5 = 32$ pages.
La Figure \ref{fig:buddy_allocator} montre l'organisation du \textit{buddy allocator}.

\begin{figure}
	\centering

	\includegraphics[scale = .42]{buddy_allocator.pdf}

	\caption[Organisation du \textit{buddy allocator}]{Organisation du \textit{buddy allocator} \cite[Figure 6.1]{gorman_understanding_2004}}
	\label{fig:buddy_allocator}
\end{figure}

Pour allouer un bloc de, par exemple, 7 pages, Linux va devoir allouer un bloc de 8 pages.
Si, malheureusement, aucun bloc de 8 pages contiguës n'existe actuellement, il sélectionne un bloc de 16 pages qu'il casse en 2 blocs de 8 pages.
Le premier est rangé dans la liste de la case 3.
Tandis que le second est utilisé pour satisfaire l'allocation.
La page restante est bien entendu ajoutée à la liste pointée par la case 0.

Une fois alloué, ce bloc est ajouté au \textit{page cache} que nous détaillons dans la Section \ref{subsec:page_cache}.

\subsection{Le \textit{page cache}}
\label{subsec:page_cache}
Comme nous l'avons vu dans la Section \ref{subsec:modern_architecture}, le stockage sur disque est beaucoup plus lent que la DRAM ou les caches processeurs.
Ainsi, plutôt que de lire une donnée sur le disque à chaque accès, les systèmes d'exploitation ramènent plusieurs données, formant un bloc, depuis le disque qu'ils stockent dans la mémoire physique.

Pour Linux, ce mécanisme s'appelle le \textit{page cache} \cite[Figure 10.1]{gorman_understanding_2004}.
Celui-ci peut occuper toute la mémoire qui n'a pas encore été allouée \cite[Chapter 16]{love_linux_2010}.

Dans la section \ref{subsec:paged_virtual_memory}, nous avons indiqué que Linux distingue les pages fichiers des pages anonymes.
Ainsi, pour ces deux types de pages, le cache contient deux listes de pages, il y a donc quatre listes au total :
\begin{description}
	\item[Liste active :] Une page qui a été accédée plusieurs fois se situe en liste active.
	\item[Liste inactive :] Une page qui n'a été accédée qu'une fois est d'abord placée en liste inactive.
	Si elle est à nouveau accédée, elle est promue en liste active.
\end{description}

\subsection{Réclamation mémoire}
\label{subsec:memory_reclaim}
Malheureusement, la mémoire est une ressource finie.
Quand les pages physiques commencent à manquer, il est courant de dire que le système est en pression mémoire.
Dans ce cas, le noyau doit réclamer des pages avant de pouvoir satisfaire une nouvelle allocation.

Pour ce faire, Linux s'appuie sur deux mécanismes \cite[Figure 17-3]{bovet_understanding_2006} :
\begin{description}
	\item[kswapd :] Ce démon est réveillé quand le nombre de pages libres passe en dessous d'un certain seuil.
	Il a pour but de réclamer des pages afin d'éviter, de façon préventive, que la pression mémoire n'empire \cite[Section 10.6]{gorman_understanding_2004}.
	\item[direct reclaim :] Lors d'une allocation de page, si le nombre de pages libres est inférieur à un seuil, lui-même inférieur au seuil de réveil de \texttt{kswapd}, Linux procède à un \textit{direct reclaim}.
	Ce mécanisme consiste simplement à réclamer des pages avant de répondre à la demande d'allocation \cite[Section "Low On Memory Reclaiming"]{bovet_understanding_2006}.
\end{description}

Ces mécanismes agissent sur le \textit{page cache} pour récupérer de la mémoire \citelinux{mm/vmscan.c}{2482}.
Linux essaye de gérer les deux catégories de pages de façon identique.
Une page fichier peut être réclamée sans effort, si son contenu n'a pas été modifié par rapport à celui sur le disque, car elle peut facilement être relue depuis le disque.
Cependant, la réclamation des pages anonymes est plus complexe, car les données contenues ne peuvent être lues depuis le disque.
Linux s'appuie donc sur un espace d'échange, une zone particulière de la mémoire de stockage où peuvent être conservées, temporairement, les pages anonymes.

Lorsqu'il y a pression mémoire, Linux va essayer de réclamer, à chaque liste, une certaine quantité de pages.
Une liste inactive est toujours réclamée tandis qu'une liste active l'est uniquement si la liste inactive correspondante comporte peu de pages.
Lorsqu'une liste active est réclamée, les pages qui ont été récupérées ne sont pas marquées immédiatement comme libres, mais sont d'abord transférées dans la liste inactive analogue.
Une page qui appartenait à une liste active se voit donc offrir une seconde chance.
\textit{A contrario}, les pages réclamées depuis une liste inactive peuvent être immédiatement ré-utilisées.

La Figure \ref{fig:memory_reclaim} donne une représentation de l'action de la réclamation mémoire sur le \textit{page cache}.

\begin{figure}
	\centering

	\includegraphics[scale = .42]{lru.pdf}

	\caption{Le déplacement des pages dans le \textit{page cache} lors d'une pression mémoire}
	\label{fig:memory_reclaim}
\end{figure}

\subsection{Le \textit{ballooning} des hyperviseurs}
\label{subsec:ballooning}
Pour transférer la mémoire entre plusieurs VM, les hyperviseurs s'appuient sur le \textit{memory ballooning} \cite{qemu_qemu_2019}.
Nous avons décrit ce mécanisme dans la Section \ref{sec:original_works} et conclu qu'il était difficile à utiliser.
Nous verrons, dans la prochaine sous-section, que le \texttt{cgroup} mémoire facilite le transfert de mémoire entre plusieurs conteneurs.

\subsection{La consolidation pour le \texttt{cgroup} mémoire}
\label{subsec:memory_cgroup}
Lorsqu'un conteneur est lancé via \texttt{docker}, un nouveau \texttt{cgroup} mémoire est instancié pour celui-ci.
Ce \texttt{cgroup} mémoire dispose de son propre \textit{page cache} \cite{corbet_integrating_2011, rik_van_riel_patch_2008}.
Ces différents \textit{page caches} sont exclusifs, c'est-à-dire qu'une page dans le \textit{page cache} d'un \texttt{cgroup} n'appartient pas au \textit{page cache} d'un autre \texttt{cgroup}, \texttt{root} par exemple.
Ce design permet donc de récupérer de la mémoire à un \texttt{cgroup} en particulier plutôt qu'à toutes les tâches du système.
La Figure \ref{fig:page_cache_cgroup} schématise cette organisation.

\begin{figure}
	\centering

	\includegraphics[scale = .42]{cgroups.pdf}

	\caption{Schéma de l'organisation du \textit{page cache} entre les \texttt{cgroups}}
	\label{fig:page_cache_cgroup}
\end{figure}

Quand une page est ajoutée au \textit{page cache} d'un \texttt{cgroup}, il est dit que cette page est chargée dans ce \texttt{cgroup}.
En effet, la fonction responsable de cela est \texttt{mem\_cgroup\_try\_charge} \citelinux{mm/memcontrol.c}{5874}.

Pour prendre certaines décisions, notamment liées à la réclamation mémoire, le \texttt{cgroup} mémoire se base sur des métriques système, exposées à l'utilisateur via le fichier \texttt{memory.stat} \cite[Section 5.2]{noauthor_memory_nodate}.
Le Tableau \ref{table:stats} compile celles-ci.

\begin{table}
	\centering

	\caption[Les différentes statistiques collectées par le \texttt{cgroup} mémoire]{Les différentes statistiques collectées par le \texttt{cgroup} mémoire \cite[Section 5.2]{noauthor_memory_nodate}}
	\label{table:stats}

	\begin{tabular}{cp{.7\textwidth}}
		\hline
		Nom & Description\\
		\hline
		\texttt{cache} & La taille du \textit{page cache} en octet.\\
		\texttt{rss} & La taille, en octet, de la mémoire utilisée pour des pages anonymes.\\
		\texttt{rss\_huge} & La taille, en octet, de la mémoire utilisée par des \textit{huge pages} anonymes.\\
		\texttt{mapped\_file} & Nombre d'octets utilisés pour des fichiers projetés en mémoire via \texttt{mmap}.\\
		\texttt{pgpgin} & Nombre de fois qu'une page a été chargée dans ce \texttt{cgroup}.\\
		\texttt{pgpgout} & Nombre d'occurrences de déchargement de page pour ce \texttt{cgroup}.\\
		\texttt{swap} & Taille actuelle, en octet, de l'espace d'échange.\\
		\texttt{dirty} & Nombre d'octets en attente d'écriture sur le disque.\\
		\texttt{writeback} & Nombre d'octets, en file d'attente pour être écrits sur le disque.\\
		\texttt{inactive\_anon} & Taille, en octet, de la liste anonyme inactive.\\
		\texttt{active\_anon} & Le nombre d'octets contenus dans la liste anonyme active.\\
		\texttt{inactive\_file} & Taille, en octet, de la liste fichier inactive.\\
		\texttt{active\_file} & Taille de la liste fichier active en octet.\\
		\texttt{unevictable} & Taille, en octet, de la liste des pages ne pouvant être évincées.\\
		\hline
	\end{tabular}
\end{table}

\section{Conclusion}
\label{sec:memory_conclusion}
Dans ce chapitre, nous avons vu que la mémoire est sujette à un compromis entre sa taille et sa vitesse d'accès.
Pour pallier à cela, les noyaux des systèmes d'exploitation proposent la mise en cache de blocs du disque dans la mémoire principale.
D'un autre côté, les hyperviseurs et les conteneurs apportent des modifications à la gestion de la mémoire, comme la présence d'un \textit{page cache} par instance du \texttt{cgroup} mémoire.
Dans le chapitre suivant, nous mettons en exergue l'incapacité du \texttt{cgroup} mémoire à offrir et l'isolation et la consolidation.