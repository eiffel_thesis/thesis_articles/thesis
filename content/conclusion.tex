\chapter{Conclusion}
\label{chapter:conclusion}
Le \textit{cloud} aura marqué un changement, bienvenu ou délétère, dans l'informatique.
Avant lui, la plupart des acteurs de ce domaine possédaient leurs propres centres de données.
Dorénavant, l'infrastructure a été abstraite et n'est possédée que par quelques géants du numérique menant ainsi à une situation d'oligopole.
Pour rendre possible son abstraction, le \textit{cloud} s'est appuyé sur la virtualisation.
Si, historiquement, les machines virtuelles sont les porte-étendards du concept du même nom, les conteneurs sont devenus, petit à petit, une alternative viable.

La virtualisation permet d'isoler les services de différents clients.
Ainsi, le conteneur d'un client donné ne peut pas s'accaparer toutes les ressources au détriment des autres.
Cette isolation permet donc de garantir la satisfaction liée aux SLA signées par le client et le fournisseur.
Néanmoins, un fournisseur de \textit{cloud} souhaite consolider les ressources de son système afin d'en faire un meilleur usage et de pouvoir accueillir plus de clients.

Linux propose des mécanismes garantissant l'isolation, mais, comme nous l'avons montré dans cette thèse, ils ne sont pas suffisants pour garantir la consolidation.
Pour mettre en exergue ce problème, nous avons développé une plateforme expérimentale reproduisant le comportement de conteneurs exécutant des bases de données utilisées par des sites de commerce en ligne.
Nos conteneurs étaient soumis à des charges de travail dynamiques représentant ainsi les différentes périodes d'activité d'une journée.
Les mécanismes que propose Linux pour contrôler l'empreinte mémoire des conteneurs ont montré leurs limites.
En effet, ils ne peuvent s'adapter à des charges de travail dynamiques à cause de leur caractère statique.
Ce problème prend tout son sens dans le \textit{cloud} où il mène à un surdimensionnement de la mémoire des clients et à une utilisation non optimale de celle-ci pour le fournisseur.

Les mécanismes de Linux sont certes souples, puisqu'il est permis, dans certaines conditions, à un conteneur de dépasser la valeur d'une limite mémoire.
Mais ils ne peuvent s'adapter aux variations dans la charge de travail des conteneurs.
De plus, le système d'exploitation a certes une vision globale de l'utilisation des ressources, mais les métriques dont il dispose sont déconnectées des performances des applications.
En s'inspirant de l'état de l'art, notamment de la boucle autonomique, nous avons conclu qu'une double vue pouvait être la solution à ce problème.
D'un côté, l'espace utilisateur permet la collecte de métriques proches des applications et donc représentatives de leurs performances.
De l'autre, l'espace noyau offre une vision globale du système permettant de savoir si celui-ci est en pression mémoire.

Ainsi, nous avons proposé MemOpLight.
Ce mécanisme s'appuie sur les informations haut niveaux offertes par l'espace utilisateur pour réclamer au mieux la mémoire en tant que mécanisme noyau.
Cette réclamation cible prioritairement les conteneurs ayant de bonnes performances dans l'espoir de trouver un équilibre d'utilisation de la mémoire où le maximum de conteneurs est satisfait.

Nous avons éprouvé notre solution en la comparant aux mécanismes existants dans une expérience calquant le comportement d'un site de commerce en ligne.
MemOpLight a ainsi montré de meilleures performances que les mécanismes offerts par Linux.
Contrairement à ceux-ci, notre solution est dynamique et s'adapte donc aux besoins des applications.
Nous avons ensuite consolidé nos résultats en lançant une expérience de plus grande envergure
Ainsi, MemOpLight offre des performances supérieures de 8.9\% tout en augmentant la satisfaction de 13\%, ce grâce à un meilleur usage de la mémoire disponible.

Nous verrons, dans la suite de cet ultime chapitre, tout d'abord les améliorations pouvant être apportées à MemOpLight pour pallier à ses faiblesses ou augmenter ses performances.
Puis, nous terminerons par une ouverture sur les horizons futurs brossés par cette thèse.

\section{Améliorations de MemOpLight}
\label{sec:ameliorations}
MemOpLight apporte la consolidation aux conteneurs tout en assurant une isolation entre ceux-ci.
Toutefois, notre mécanisme peut être amélioré.
Nous pouvons, premièrement, lancer des expériences plus conséquentes pour éprouver celui-ci.
Deuxièmement, il nous faudrait traiter particulièrement le cas d'un cycle de réclamation.
Enfin, le contrôleur applicatif est la seule partie qu'un utilisateur doit écrire pour utiliser MemOpLight, il faut donc faciliter au possible cette écriture.

Pour mettre à l'épreuve MemOpLight dans des expériences plus conséquentes, plusieurs solutions sont possibles.
Tout d'abord, nous pouvons reprendre notre expérience mais en allouant plus de ressources aux conteneurs et en augmentant, encore, le nombre de conteneurs.
Il est aussi possible de changer le \textit{benchmark} exécuté par les différents conteneurs voir de mixer les \textit{benchmarks} utilisés.
Notre solution peut aussi être testée avec une suite logicielle utilisée dans l'industrie.
Nous avons commencé à réaliser une telle expérience où des conteneurs sollicitent, via \texttt{gatling}, un site web hébergeant \texttt{woocommerce}, une extension transformant \texttt{wordpress} en site de commerce en ligne, et s'appuyant donc sur \texttt{apache} et \texttt{mysql} \cite{noauthor_gatling_nodate, noauthor_woocommerce_nodate, noauthor_wordpress_nodate, noauthor_apache_nodate, noauthor_mysql_nodate, thesis_gatling}.
Néanmoins, pour que cette expérience soit représentative de la réalité il nous faudrait posséder une copie d'un fichier \texttt{access.log} utilisé en production.
En effet, le fichier dont nous disposons correspond à un site de commerce en ligne iranien, mais le nombre de requêtes est limité \cite{zaker_online_2019}.
Nous pourrions extrapoler celui-ci, mais nous introduirions alors des biais statistiques qui fausseraient la réalité.
D'autres traces sont disponibles, comme celles de différents fournisseurs de \textit{cloud} ou du site web de la coupe du monde de football 98, mais elles ne sont pas exemptes de défauts \cite{wilkes_yet_2020, cortez_resource_2017, alibaba_alibaba_2017, noauthor_worldcup98_1998}.
En effet, les premières sont trop abstraites puisqu'elles ne contiennent que des informations temporelles sur l'utilisation de ressources \cite{wilkes_yet_2020, cortez_resource_2017, alibaba_alibaba_2017}.
Quant à la dernière, elle date de 1998 et n'est donc plus représentative du web d'aujourd'hui \cite{noauthor_worldcup98_1998}.

Une autre amélioration aurait pour but de prévenir le cas pathologique d'un cycle de réclamation où MemOpLight empêche un conteneur d'avoir des performances stables.
Nous pensons donc ajouter une connaissance à celui-ci.
Cette connaissance, au sens \textit{Knowledge} de la boucle MAPE-K, pourrait prendre la forme d'un historique d'états de satisfaction pour chaque conteneur.
Outre la connaissance, un cycle d'hystérésis forçant l'espacement des réclamations mémoire successives visant un conteneur devrait briser ce cercle vicieux.

Enfin, et pour faciliter l'utilisation de MemOpLight dans l'industrie, la formalisation des contrôleurs nous semble importante.
En effet, le contrôleur applicatif est la seule partie qu'un utilisateur doit écrire pour pouvoir se servir de notre mécanisme.
Nous pensons donc que le développement d'un cadriciel de contrôleurs applicatifs faciliterait l'écriture de ces derniers et démocratiserait l'utilisation de notre solution.

\section{Horizons futurs}
\label{sec:perspectives}
Dans cette thèse, nous nous sommes concentrés sur la mémoire présente dans les barrettes mémoires.
Néanmoins, les caches L3 des processeurs sont désormais de tailles conséquentes.
En effet, des processeurs ayant un cache L3 de \SI{256}{\mega\byte} sont disponibles à l'achat \cite{amd_amd_2019}.
Nous pourrions donc utiliser MemOpLight pour partager ce niveau de cache entre les différentes applications s'exécutant sur ce processeur.
En pratique, une application n'étant pas satisfaite se verrait allouer une plus grande part du cache qu'une application satisfaite.

À un autre niveau, les conteneurs ne prennent pas en compte, à l'écriture de ces lignes, la topologie mémoire.
Par exemple, il n'est pas possible d'allouer à un conteneur \SI[parse-numbers = false]{X}{\giga\byte} sur un nœud A et \SI[parse-numbers = false]{Y}{\giga\byte} sur un nœud B.
MemOpLight pourrait donc être utilisé pour effectuer un arbitrage de la mémoire selon les différents nœuds NUMA du système.

La mémoire est entourée d'autres ressources et elle n'est pas forcément limitante.
La bande passante disque est intimement liée à la mémoire puisque la seconde est utilisée comme un cache lorsque des données sont lues depuis le disque.
Ainsi, si la mémoire n'est pas suffisamment grande pour accueillir ces données, la bande passante disque sera saturée.
Contrairement à la bande passante disque, le CPU et la bande passante réseau ne sont pas forcément liés à la mémoire.
Néanmoins, si la bande passante réseau est limitée, la charge reçue sera faible.
MemOpLight indiquera donc de bonnes performances alors qu'il n'en est en fait rien.

Ce problème se résume donc à de l'optimisation multicritère qui est certes compliqué à résoudre mais qui, de l'autre côté, ne doit être négligé.
Dans le \textit{cloud}, les clients payent pour obtenir des ressources, trouver un équilibre entre celles-ci est donc important.
En effet, il est inutile d'avoir une trop grande quantité d'une ressource donnée si une autre fait défaut et mine les performances.