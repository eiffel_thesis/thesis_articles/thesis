\chapter{Introduction}
\label{chapter:introduction}
Aujourd'hui, le \textit{cloud} (l'informatique dans le nuage) est une réalité.
Selon l'un des plus gros acteurs du marché, celui-ci peut être défini comme suit \cite{amazon_quest_ce_nodate} :
\begin{quote}
	[...] la mise à disposition de ressources informatiques à la demande via Internet, avec une tarification en fonction de votre utilisation.
\end{quote}
Les exemples d'entreprises, comme Netflix ou Airbnb, s'étant tournées, avec succès, vers le \textit{cloud} ne manquent pas \cite{tafoya_10_2019, izrailevsky_fin_2016, amazon_success_stories_nodate}.
Si le \textit{cloud} attire autant, c'est parce qu'il présente de multiples avantages pour le client qui, d'après ce même acteur\sidenote{
	Il n'aura échappé à personne que cette entreprise capitaliste n'a aucun intérêt financier à être objective au sujet de son produit.
	Aucune technologie ne vient sans inconvénient et le \textit{cloud} n'y échappe pas \cite{larkin_disadvantages_2019}.
}, sont \cite{amazon_quest_ce_nodate} :
\begin{description}
	\item[L'agilité :] L'agilité permet d'innover plus rapidement grâce à la facilité d'accès aux ressources.
	\item[L'élasticité :] Elle permet d'allouer la juste quantité de ressources nécessaires à la volée.
	Ainsi, il n'est plus besoin de sur-approvisionner, en continu, une application pour faire face à d'hypothétiques pics de requêtes.
	\item[La réduction des coûts :] Le \textit{cloud} permet d'économiser l'achat de serveurs physiques puisque la puissance de calcul est celle du fournisseur.
	\item[Le déploiement mondial en quelques minutes :] Les géants du \textit{cloud} sont présents partout sur la planète \cite{amazon_tableau_2020}.
	Il devient donc plus facile pour une entreprise de conquérir de nouveau marché \cite{izrailevsky_fin_2016}.
\end{description}

Dans les années 2010, la plupart des géants du numérique ont revêtu la casquette de fournisseur \textit{cloud} \cite{alibaba_alibaba_2009, ovh_ovh_2010, microsoft_microsoft_2010, amazon_amazon_ecc, google_google_2011}.
Amazon a initié le mouvement, en 2008, en rendant disponible pour tous son service de \textit{cloud} : \textit{Elastic Cloud Compute} (EC2) \cite{amazon_amazon_2008}.

En théorie, le modèle économique du \textit{cloud} est très simple\sidenote{Dans les faits, certains frais peuvent apparaître comme cachés, la vigilance du client est donc de mise \cite{cloudcheckr_revealed_2017}...}.
Un client souhaitant exécuter son service dans le \textit{cloud} choisit d'abord l'option qui correspond le mieux à ses besoins par rapport aux fonds dont il dispose.
Une option correspond à un service logiciel offert par le fournisseur de \textit{cloud}, comme une base de données, ou bien à des ressources physiques, il est alors commun de parler d'<<instances>> \cite{dynamodb_amazon_nodate}.
Dans un \textit{cloud} dit \textit{Infrastucture as a Service} (IaaS), une instance est un système d'exploitation virtualisé, ou système invité, auquel est alloué de la puissance de calcul, c'est-à-dire des processeurs, de la mémoire vive et du stockage \cite{mell_nist_2011}.
Le client est ensuite facturé au temps passé, la tarification dépendant de l'instance choisie \cite{amazon_amazon_nodate}.
Par exemple, une instance disposant de peu de mémoire vive est bon marché comparée à une instance en ayant plus.
Le service du client est ensuite exécuté sur l'un des serveurs physiques du fournisseur de \textit{cloud} à côté de services appartenant à d'autres clients.
La mise à disposition de ressources aux clients par le fournisseur est garantie par <<une entente de niveau de service>>, \textit{Service-level Agreement} (SLA).
Dans les faits, la plupart des SLA des fournisseurs de \textit{cloud} garantissent un taux de disponibilité mensuelle d'environ 99,9\% \cite{microsoft_sla_nodate, ovh_conditions_nodate, amazon_sla_nodate}.

Le concept d'instance, qui est à la base du \textit{cloud}, s'appuie sur la virtualisation.
Celle-ci permet l'exécution de plusieurs systèmes d'exploitation, dits systèmes invités, sur un autre système d'exploitation, dit système hôte, tout en donnant l'impression aux systèmes invités qu'ils utilisent directement le matériel de la machine \cite{popek_formal_1974}.
On parle alors de machine virtuelle (VM pour \textit{virtual machine}).
Le système invité et le système hôte sont complètement isolés, il n'est pas possible, sauf accord explicite, pour un système de récupérer les données de l'autre système \cite{sancgarg_documentation9psetup_2010}.

Du point de vue du client, la virtualisation garantit que son service n'est ni dérangé, le service d'un autre client ne pourra pas s'accaparer ses ressources, ni espionné, un autre client n'a pas connaissance des VM des autres clients.
Cette isolation permet ainsi le respect de la SLA signée par le client et le fournisseur.
Pour le fournisseur, la virtualisation permet l'\textit{isolation} des différents services des clients tout en assurant la \textit{consolidation}.
Celle-ci consiste à exécuter plusieurs VM sur une même machine physique.
Le fournisseur a d'ailleurs tout intérêt à maximiser le nombre d'instances sur une même machine physique pour tirer profit de celle-ci\footnote{
	Amazon et Google proposent un type d'instance s'exécutant sur des machines peu chargées, par contre ces instances peuvent être arrêtées par le fournisseur à tout moment \cite{amazon_spot_nodate, google_preemptible_nodate}.
	Néanmoins, et Amazon l'assure, pour des instances normales, les ressources allouées à un client ne sont allouées qu'à ce client \cite[Slide 14]{spicer_deep_2017} :
	\begin{quote}
		All resources assigned to you are dedicated to your instance with no over commitment
	\end{quote}
}.
En effet, les VM des clients peuvent être assez légères tandis que les machines physiques utilisées dans le \textit{cloud} disposent de processeurs puissants\sidenote{Les instances EC2 T3a s'appuient sur des processeurs AMD EPYC 7000 \cite{amazon_types_nodate}.
Certains AMD EPYC disposent de 64 cœurs et 128 threads comme l'EPYC 7702P \cite{amd_amd_2019}.
} offrant un haut niveau de parallélisme.
Par exemple, si la machine physique dispose d'un processeur 4 cœurs et de \SI{4}{\giga\byte} de mémoire vive il est parfaitement possible d'y exécuter deux instances s'étant vues allouer 2 cœurs et \SI{2}{\giga\byte} de mémoire vive sans qu'elles ne se gênent mutuellement.

Plus récemment, les conteneurs se sont posés comme une alternative aux hyperviseurs pour la virtualisation \cite{docker_what_nodate}.
Ceux-ci permettent, comme les premiers, d'isoler les applications tout en permettant un contrôle fin des ressources allouées à un conteneur \cite{rami_rosen_namespace_2016}.
Néanmoins, ils sont plus légers que les hyperviseurs, notamment car ils suppriment le système d'exploitation invité \cite{zhang_comparative_2018}.
En effet, les applications <<conteneurisées>> s'exécutent directement au-dessus d'un système d'exploitation hôte comme le montre la Figure \ref{fig:docker_vs_vm}.
Plusieurs logiciels permettent de lancer des conteneurs et de les gérer, l'un des plus connus est \texttt{docker} \cite{docker_docker_nodate}.
Amazon a rendu public son service d'exécution de conteneur dans AWS en 2015 \cite{amazon_amazon_2015}.
La grande majorité des fournisseurs de \textit{cloud} s'est adaptée et propose donc des services permettant d'exécuter des conteneurs sur leurs plateformes \cite{amazon_amazon_ecs, alibaba_alibaba_nodate, microsoft_microsoft_nodate}.

\begin{figure}
	\centering

	\subfloat[Docker]{
		\includegraphics[scale = .42]{docker.pdf}

		\label{fig:docker}
	}
	\subfloat[Hyperviseur avec plusieurs VM]{
		\includegraphics[scale = .42]{virtual_machine.pdf}

		\label{fig:vm}
	}

	\caption{Docker comparé à un hyperviseur}
	\label{fig:docker_vs_vm}
\end{figure}

Pour gérer les ressources allouées, les conteneurs s'appuient sur des fonctionnalités offertes par le système d'exploitation hôte.
Pour Linux, cette fonctionnalité porte le nom de \texttt{cgroup} \cite{hiroyu_cgroup_2008}.
Il existe un \texttt{cgroup} pour chaque type de ressource (nombre de cœurs, tranche de temps CPU, mémoire physique, bande passante disque, etc.).
Il est donc possible, par exemple, de restreindre l'exécution d'une application à 2 cœurs et avec \SI{2}{\giga\byte} de mémoire.
Plus spécifiquement, le \texttt{cgroup} mémoire propose deux limites :
\begin{description}
	\item[Une limite dite <<dure>> :] Un conteneur ne peut pas allouer plus de mémoire que cette limite.
	S'il dépasse cette limite, le conteneur se voit réclamer sa mémoire aussitôt.
	\item[Une limite dite <<molle>> :] Quand la mémoire physique disponible se fait rare, on parle alors de pression mémoire, le système d'exploitation hôte essaye d'assurer cette quantité de mémoire au conteneur.
	Néanmoins, cette limite vient sans aucune garantie de fonctionnement.
\end{description}
Ces deux limites garantissent l'isolation, puisqu'un conteneur ne pourra pas s'accaparer toute la mémoire au détriment de ses congénères.
Elles se posent, par contre, en opposition totale avec la consolidation.
Nous avons mené des expériences qui montrent que si un conteneur n'utilise pas sa mémoire, il n'est pas possible pour un autre conteneur de la lui réclamer afin d'en faire un meilleur usage.
Avec une charge de travail dynamique, il y a donc nécessité d'une intervention humaine.
Mais le client a l'illusion que son service s'exécute seul sur la machine et il n'a donc pas une vue globale du système comme l'a le fournisseur.

Dans cette thèse, notre but est d'apporter la consolidation aux conteneurs tout en garantissant une isolation nécessaire au respect de la SLA.
Ceci pose plusieurs questions auxquelles nous allons répondre dans ce manuscrit :
\begin{enumerate}
	\item Tout d'abord, pourquoi est-ce que les limites proposées par Linux n'arrivent pas à garantir et l'isolation et la consolidation ?
	\item Ensuite, existe-t-il un mécanisme prenant en compte les fluctuations dans les charges de travail des applications tout en ne fixant pas de limite dure à leurs empreintes mémoires ?
	\item Enfin, est-il possible, dans le même temps, de prendre en compte l'utilisation globale des ressources du système ?
\end{enumerate}

Nous répondons à ces questions en proposant les contributions suivantes :
\begin{enumerate}
	\item Dans un premier temps, nous montrons par l'expérience que les mécanismes actuels de limitation du \texttt{cgroup} mémoire permettent l'isolation mais pas la consolidation.
	\item Dans un deuxième temps, il est possible d'utiliser une boucle autonomique en espace utilisateur indiquant au noyau le niveau de satisfaction des applications.
	\item Dans un dernier temps, une seconde boucle autonomique, prenant place dans le noyau, permet de réclamer la mémoire des applications tout en ayant une vue globale de l'utilisation de la mémoire par celles-ci.
\end{enumerate}

MemOpLight, notre contribution, permet de répondre aux deux derniers points.
Ce mécanisme garantit le respect de la SLA tout en améliorant les performances des applications.
Nous présentons notre mécanisme dans ce manuscrit dont le plan est le suivant :
\begin{description}
	\item[Chapitre \ref{chapter:state_of_the_art} :] Ce chapitre présente un état de l'art des techniques permettant la consolidation ou le suivi des performances des applications.
	Dans celui-ci, sont abordées les différentes théories permettant de dimensionner aux mieux l'environnement d'exécution d'une application.
	Pour chacune de ces théories, des articles de recherche y faisant référence sont étudiés.
	Des travaux de recherche n'utilisant pas les approches susnommées sont examinés \cite{amazon_aws_2018, carver_acdc:_2017}.
	\item[Chapitre \ref{chapter:memory} :] Ce chapitre porte sur la mémoire.
	Celle-ci est d'abord étudiée au niveau matériel en abordant la pyramide mémoire et le compromis entre taille et vitesse.
	Ensuite, la gestion de la mémoire par le système d'exploitation est décrite, notamment l'allocation et la réclamation de celle-ci.
	Puis, le concept de virtualisation est présenté en étudiant les hyperviseurs et les conteneurs.
	\item[Chapitre \ref{chapter:container_flaw} :] Dans la lignée du Chapitre \ref{chapter:memory}, ce chapitre met en exergue l'impossibilité des conteneurs à garantir et l'isolation et la consolidation mémoire en montrant expérimentalement ce problème.
	Avant cela, les spécificités, dans la gestion mémoire, propres à ces derniers, sont détaillées.
	\item[Chapitre \ref{chapter:memoplight} :] Ce chapitre présente MemOpLight.
	Notre mécanisme s'appuie sur deux composants :
	\begin{itemize}
		\item D'un côté, sur des contrôleurs applicatifs, propres à chaque conteneur, qui informent le noyau des performances de ces derniers
		\item et, d'un autre côté, sur un mécanisme de réclamation mémoire, intégré au noyau, se basant sur les informations collectées par les contrôleurs.
	\end{itemize}
	\item[Chapitre \ref{chapter:memoplight_evaluation} :] Les performances de MemOpLight sont comparées aux mécanismes existants dans ce chapitre.
	Pour ce faire, nous réalisons une expérience reprenant le scénario d'exécution d'un site de commerce en ligne mais avec plus de conteneurs.
	Ensuite, nous étudions l'impact des différents paramètres de MemOpLight sur ses performances.
	\item[Chapitre \ref{chapter:conclusion} :] Enfin, ce chapitre conclut nos travaux en les discutant et en apportant des pistes de travaux futurs.
\end{description}